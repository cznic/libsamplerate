# libsamplerate

Package libsamplerate is a ccgo/v4 version of libsamplerate.a (https://github.com/libsndfile/libsamplerate)

Documentation: https://pkg.go.dev/modernc.org/libsamplerate
