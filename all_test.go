// Copyright 2024 The libsamplerate-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libsamplerate // import "modernc.org/libsamplerate"

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	util "modernc.org/ccgo/v3/lib"
	_ "modernc.org/ccgo/v4/lib"
	_ "modernc.org/libc"
)

var (
	oXTags = flag.String("xtags", "", "passed as -tags to tests")
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	target = fmt.Sprintf("%s/%s", goos, goarch)
)

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

func Test(t *testing.T) {
	blacklist := map[string]struct{}{}
	switch target {
	case "darwin/amd64":
		blacklist["callback_hang"] = struct{}{}        //TODO
		blacklist["multichan_throughput"] = struct{}{} //TODO
		blacklist["throughput"] = struct{}{}           //TODO
	case "darwin/arm64":
		blacklist["callback_hang"] = struct{}{}        //TODO
		blacklist["multichan_throughput"] = struct{}{} //TODO
		blacklist["throughput"] = struct{}{}           //TODO
	case "freebsd/amd64":
		blacklist["callback_hang"] = struct{}{}        //TODO
		blacklist["multichan_throughput"] = struct{}{} //TODO
		blacklist["throughput"] = struct{}{}           //TODO
	case "linux/arm":
		blacklist["callback_hang"] = struct{}{} //TODO
	case "linux/arm64":
		blacklist["callback_hang"] = struct{}{} //TODO
	case "linux/loong64":
		blacklist["callback_hang"] = struct{}{} //TODO
	}
	m, err := filepath.Glob(filepath.Join("tests", "*"))
	if err != nil {
		t.Fatal(err)
	}

	var tests, pass, skip, fail int
	for _, td := range m {
		tests++
		bn := filepath.Base(td)
		if _, ok := blacklist[bn]; ok {
			skip++
			t.Logf("%v: SKIP", td)
			continue
		}

		out, err := util.Shell("go", "run", "-tags="+*oXTags, "./"+td)
		if err != nil {
			fail++
			t.Errorf("%v: %s\nFAIL: %v", td, out, err)
			continue
		}

		a := strings.Split(string(out), "\n")
		for _, v := range a {
			if strings.Contains(v, "ERROR") || strings.Contains(v, "FAIL") {
				fail++
				t.Errorf("%v: %s", td, v)
				continue
			}
		}

		pass++
	}
	t.Logf("tests=%v skip=%v fail=%v pass=%v", tests, skip, fail, pass)
}
