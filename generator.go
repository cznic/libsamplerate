// Copyright 2024 The libsamplerate-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"modernc.org/cc/v4"
	util "modernc.org/ccgo/v3/lib"
	ccgo "modernc.org/ccgo/v4/lib"
)

const (
	archivePath = "c81375f070f3c6764969a738eacded64f53a076e.zip"
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	target = fmt.Sprintf("%s/%s", goos, goarch)
	sed    = "sed"
	j      = fmt.Sprint(runtime.GOMAXPROCS(-1))
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintln(os.Stderr, strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	if ccgo.IsExecEnv() {
		if err := ccgo.NewTask(goos, goarch, os.Args, os.Stdout, os.Stderr, nil).Main(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		return
	}

	f, err := os.Open(archivePath)
	if err != nil {
		fail(1, "cannot open archive: %v\n", err)
	}

	f.Close()
	switch target {
	case "freebsd/amd64", "freebsd/arm64", "darwin/amd64", "darwin/arm64":
		sed = "gsed"
	}

	_, extractedArchivePath := filepath.Split(archivePath)
	extractedArchivePath = "libsamplerate-" + extractedArchivePath[:len(extractedArchivePath)-len(".zip")]
	tempDir := os.Getenv("GO_GENERATE_DIR")
	dev := os.Getenv("GO_GENERATE_DEV") != ""
	switch {
	case tempDir != "":
		util.MustShell(true, "sh", "-c", fmt.Sprintf("rm -rf %s", filepath.Join(tempDir, extractedArchivePath)))
	default:
		var err error
		if tempDir, err = os.MkdirTemp("", "libsamplerate-generate"); err != nil {
			fail(1, "creating temp dir: %v\n", err)
		}

		defer func() {
			switch os.Getenv("GO_GENERATE_KEEP") {
			case "":
				os.RemoveAll(tempDir)
			default:
				fmt.Printf("%s: temporary directory kept\n", tempDir)
			}
		}()
	}
	libRoot := filepath.Join(tempDir, extractedArchivePath)
	makeRoot := libRoot
	fmt.Fprintf(os.Stderr, "archivePath %s\n", archivePath)
	fmt.Fprintf(os.Stderr, "extractedArchivePath %s\n", extractedArchivePath)
	fmt.Fprintf(os.Stderr, "tempDir %s\n", tempDir)
	fmt.Fprintf(os.Stderr, "libRoot %s\n", libRoot)
	fmt.Fprintf(os.Stderr, "makeRoot %s\n", makeRoot)

	util.MustShell(true, "unzip", archivePath, "-d", tempDir)
	util.MustCopyFile(true, "LICENSE-LIBSAMPLERATE.md", filepath.Join(libRoot, "COPYING"), nil)
	os.RemoveAll("testdata")
	result := filepath.Join("src", ".libs", "libsamplerate.so.0.2.2.go")
	switch target {
	case "darwin/amd64", "darwin/arm64":
		result = filepath.Join("src", ".libs", "libsamplerate.0.dylib.go")
	}
	args := []string{os.Args[0]}
	util.MustInDir(true, makeRoot, func() (err error) {
		cflags := []string{"-w"}
		if s := cc.LongDouble64Flag(goos, goarch); s != "" {
			cflags = append(cflags, s)
		}
		util.MustShell(true, "sh", "-c", "go mod init example.com/libsamplerate ; go get modernc.org/libc@latest modernc.org/libsamplerate@latest")
		if dev {
			util.MustShell(true, "sh", "-c", "go work init ; go work use $GOPATH/src/modernc.org/libc $GOPATH/src/modernc.org/libsamplerate")
		}
		util.MustShell(true, "sh", "./autogen.sh")
		util.MustShell(true, "sh", "-c", fmt.Sprintf("CFLAGS='%s' ./configure", strings.Join(cflags, " ")))
		util.Shell(sed, "-i", "s/__x86_64__/__UNDEFINED__/", "src/common.h")
		if dev {
			args = append(
				args,
				"-absolute-paths",
				"-keep-object-files",
				"-positions",
			)
		}
		args = append(args,
			// "--prefix-enumerator=_",
			"--prefix-external=x_",
			"--prefix-field=F",
			"--prefix-macro=m_",
			"--prefix-static-internal=_",
			"--prefix-static-none=_",
			"--prefix-tagged-enum=_",
			"--prefix-tagged-struct=T",
			"--prefix-tagged-union=T",
			"--prefix-typename=T",
			"--prefix-undefined=_",
			"-extended-errors",
		)
		switch target {
		case "linux/arm64", "darwin/arm64":
			args = append(args, "-ignore-unsupported-alignment")
		}
		if err := ccgo.NewTask(goos, goarch, append(args, "-exec", "make", "-j", j, "check"), os.Stdout, os.Stderr, nil).Exec(); err != nil {
			return err
		}

		util.MustShell(true, sed, "-i", `s/\<T__\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/t__\1/g`, result)
		util.MustShell(true, sed, "-i", `s/\<x_\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/X\1/g`, result)
		return nil
	})

	fn := fmt.Sprintf("ccgo_%s_%s.go", goos, goarch)
	util.MustCopyFile(false, fn, filepath.Join(makeRoot, result), nil)
	util.MustShell(true, sed, "-i", "s/^package main/package libsamplerate/", fn)

	m, err := filepath.Glob(filepath.Join(makeRoot, "tests", ".libs", "*_test.go"))
	if err != nil {
		fail(1, "cannot glob tests: %v\n", err)
	}

	for _, src := range m {
		dst := filepath.Base(src)
		dst = dst[:len(dst)-len("_test.go")]
		dst = filepath.Join("tests", dst, fn)
		util.MustCopyFile(true, dst, src, nil)
	}

}
