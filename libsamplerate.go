// Copyright 2024 The libsamplerate-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libsamplerate is a ccgo/v4 version of libsamplerate.a (https://github.com/libsndfile/libsamplerate)
//
// Builders: https://modern-c.appspot.com/-/builder/?importpath=modernc.org/libsamplerate
package libsamplerate // import "modernc.org/libsamplerate"

//TODO Idiomatic Go wrappers
