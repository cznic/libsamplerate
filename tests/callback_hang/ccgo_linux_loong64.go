// Code generated for linux/loong64 by 'gcc --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -w -o tests/.libs/callback_hang_test.go tests/callback_hang_test.o.go tests/util.o.go -lm -lsamplerate', DO NOT EDIT.

//go:build linux && loong64
// +build linux,loong64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libsamplerate"
)

var (
	_ reflect.Type
	_ unsafe.Pointer
)

const m_BIG_ENDIAN = "__BIG_ENDIAN"
const m_BUFSIZ = 8192
const m_BYTE_ORDER = "__BYTE_ORDER"
const m_CONTEXT_INFO_ALIGN = 16
const m_CPU_CLIPS_NEGATIVE = 0
const m_CPU_CLIPS_POSITIVE = 0
const m_CPU_IS_BIG_ENDIAN = 0
const m_CPU_IS_LITTLE_ENDIAN = 1
const m_ENABLE_SINC_BEST_CONVERTER = "yes"
const m_ENABLE_SINC_FAST_CONVERTER = "yes"
const m_ENABLE_SINC_MEDIUM_CONVERTER = "yes"
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_FD_SETSIZE = "__FD_SETSIZE"
const m_FILENAME_MAX = 4096
const m_FOPEN_MAX = 16
const m_FPU_CTX_ALIGN = 8
const m_FPU_CTX_MAGIC = 0x46505501
const m_FP_FAST_FMA = 1
const m_FP_FAST_FMAF = 1
const m_FP_ILOGBNAN = 2147483647
const m_FP_INFINITE = 1
const m_FP_NAN = 0
const m_FP_NORMAL = 4
const m_FP_SUBNORMAL = 3
const m_FP_ZERO = 2
const m_F_LOCK = 1
const m_F_OK = 0
const m_F_TEST = 3
const m_F_TLOCK = 2
const m_F_ULOCK = 0
const m_HAVE_ALARM = 1
const m_HAVE_CALLOC = 1
const m_HAVE_CEIL = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_FLOOR = 1
const m_HAVE_FMOD = 1
const m_HAVE_FREE = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_LRINT = 1
const m_HAVE_LRINTF = 1
const m_HAVE_MALLOC = 1
const m_HAVE_MEMCPY = 1
const m_HAVE_MEMMOVE = 1
const m_HAVE_SIGALRM = 1
const m_HAVE_SIGNAL = 1
const m_HAVE_STDBOOL_H = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TIMES_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_VISIBILITY = 1
const m_LARCH_NGREG = 32
const m_LARCH_REG_A0 = 4
const m_LARCH_REG_NARGS = 8
const m_LARCH_REG_RA = 1
const m_LARCH_REG_S0 = 23
const m_LARCH_REG_S1 = 24
const m_LARCH_REG_S2 = 25
const m_LARCH_REG_SP = 3
const m_LASX_CTX_ALIGN = 32
const m_LASX_CTX_MAGIC = 0x41535801
const m_LITTLE_ENDIAN = "__LITTLE_ENDIAN"
const m_LSX_CTX_ALIGN = 16
const m_LSX_CTX_MAGIC = 0x53580001
const m_LT_OBJDIR = ".libs/"
const m_L_INCR = "SEEK_CUR"
const m_L_SET = "SEEK_SET"
const m_L_XTND = "SEEK_END"
const m_L_ctermid = 9
const m_L_tmpnam = 20
const m_MATH_ERREXCEPT = 2
const m_MATH_ERRNO = 1
const m_MINSIGSTKSZ = 4096
const m_M_1_PI = 0.31830988618379067154
const m_M_2_PI = 0.63661977236758134308
const m_M_2_SQRTPI = 1.12837916709551257390
const m_M_E = 2.7182818284590452354
const m_M_LN10 = 2.30258509299404568402
const m_M_LN2 = 0.69314718055994530942
const m_M_LOG10E = 0.43429448190325182765
const m_M_LOG2E = 1.4426950408889634074
const m_M_PI = 3.14159265358979323846
const m_M_PI_2 = 1.57079632679489661923
const m_M_PI_4 = 0.78539816339744830962
const m_M_SQRT1_2 = 0.70710678118654752440
const m_M_SQRT2 = 1.41421356237309504880
const m_NFDBITS = "__NFDBITS"
const m_NSIG = "_NSIG"
const m_PACKAGE = "libsamplerate"
const m_PACKAGE_BUGREPORT = "erikd@mega-nerd.com"
const m_PACKAGE_NAME = "libsamplerate"
const m_PACKAGE_STRING = "libsamplerate 0.2.2"
const m_PACKAGE_TARNAME = "libsamplerate"
const m_PACKAGE_URL = "https://github.com/libsndfile/libsamplerate/"
const m_PACKAGE_VERSION = "0.2.2"
const m_PDP_ENDIAN = "__PDP_ENDIAN"
const m_P_tmpdir = "/tmp"
const m_RAND_MAX = 2147483647
const m_R_OK = 4
const m_SA_INTERRUPT = 0x20000000
const m_SA_NOCLDSTOP = 1
const m_SA_NOCLDWAIT = 2
const m_SA_NODEFER = 0x40000000
const m_SA_NOMASK = "SA_NODEFER"
const m_SA_ONESHOT = "SA_RESETHAND"
const m_SA_ONSTACK = 0x08000000
const m_SA_RESETHAND = 0x80000000
const m_SA_RESTART = 0x10000000
const m_SA_SIGINFO = 4
const m_SA_STACK = "SA_ONSTACK"
const m_SEEK_CUR = 1
const m_SEEK_END = 2
const m_SEEK_SET = 0
const m_SHORT_BUFFER_LEN = 512
const m_SIGABRT = 6
const m_SIGALRM = 14
const m_SIGBUS = 7
const m_SIGCHLD = 17
const m_SIGCLD = "SIGCHLD"
const m_SIGCONT = 18
const m_SIGFPE = 8
const m_SIGHUP = 1
const m_SIGILL = 4
const m_SIGINT = 2
const m_SIGIO = "SIGPOLL"
const m_SIGIOT = "SIGABRT"
const m_SIGKILL = 9
const m_SIGPIPE = 13
const m_SIGPOLL = 29
const m_SIGPROF = 27
const m_SIGPWR = 30
const m_SIGQUIT = 3
const m_SIGSEGV = 11
const m_SIGSTKFLT = 16
const m_SIGSTKSZ = 16384
const m_SIGSTOP = 19
const m_SIGSYS = 31
const m_SIGTERM = 15
const m_SIGTRAP = 5
const m_SIGTSTP = 20
const m_SIGTTIN = 21
const m_SIGTTOU = 22
const m_SIGURG = 23
const m_SIGUSR1 = 10
const m_SIGUSR2 = 12
const m_SIGVTALRM = 26
const m_SIGWINCH = 28
const m_SIGXCPU = 24
const m_SIGXFSZ = 25
const m_SIG_BLOCK = 0
const m_SIG_SETMASK = 2
const m_SIG_UNBLOCK = 1
const m_SIZEOF_DOUBLE = 8
const m_SIZEOF_FLOAT = 4
const m_SIZEOF_INT = 4
const m_SIZEOF_LONG = 8
const m_STDC_HEADERS = 1
const m_STDERR_FILENO = 2
const m_STDIN_FILENO = 0
const m_STDOUT_FILENO = 1
const m_TMP_MAX = 238328
const m_VERSION = "0.2.2"
const m_WCONTINUED = 8
const m_WEXITED = 4
const m_WNOHANG = 1
const m_WNOWAIT = 0x01000000
const m_WSTOPPED = 2
const m_WUNTRACED = 2
const m_W_OK = 2
const m_X_OK = 1
const m__ABILP64 = 3
const m__ALLOCA_H = 1
const m__ATFILE_SOURCE = 1
const m__BITS_BYTESWAP_H = 1
const m__BITS_ENDIANNESS_H = 1
const m__BITS_ENDIAN_H = 1
const m__BITS_LIBM_SIMD_DECL_STUBS_H = 1
const m__BITS_POSIX_OPT_H = 1
const m__BITS_PTHREADTYPES_ARCH_H = 1
const m__BITS_PTHREADTYPES_COMMON_H = 1
const m__BITS_SIGACTION_H = 1
const m__BITS_SIGCONTEXT_H = 1
const m__BITS_SIGEVENT_CONSTS_H = 1
const m__BITS_SIGINFO_ARCH_H = 1
const m__BITS_SIGINFO_CONSTS_H = 1
const m__BITS_SIGNUM_ARCH_H = 1
const m__BITS_SIGNUM_GENERIC_H = 1
const m__BITS_SIGSTACK_H = 1
const m__BITS_SIGTHREAD_H = 1
const m__BITS_SS_FLAGS_H = 1
const m__BITS_STDINT_INTN_H = 1
const m__BITS_STDIO_LIM_H = 1
const m__BITS_TIME64_H = 1
const m__BITS_TYPESIZES_H = 1
const m__BITS_TYPES_H = 1
const m__BITS_UINTN_IDENTITY_H = 1
const m__CS_POSIX_V5_WIDTH_RESTRICTED_ENVS = "_CS_V5_WIDTH_RESTRICTED_ENVS"
const m__CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = "_CS_V6_WIDTH_RESTRICTED_ENVS"
const m__CS_POSIX_V7_WIDTH_RESTRICTED_ENVS = "_CS_V7_WIDTH_RESTRICTED_ENVS"
const m__DEFAULT_SOURCE = 1
const m__ENDIAN_H = 1
const m__FEATURES_H = 1
const m__GETOPT_CORE_H = 1
const m__GETOPT_POSIX_H = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__IO_EOF_SEEN = 0x0010
const m__IO_ERR_SEEN = 0x0020
const m__IO_USER_LOCK = 0x8000
const m__LFS64_ASYNCHRONOUS_IO = 1
const m__LFS64_LARGEFILE = 1
const m__LFS64_STDIO = 1
const m__LFS_ASYNCHRONOUS_IO = 1
const m__LFS_LARGEFILE = 1
const m__LOONGARCH_ARCH = "loongarch64"
const m__LOONGARCH_ARCH_LOONGARCH64 = 1
const m__LOONGARCH_FPSET = 32
const m__LOONGARCH_SIM = "_ABILP64"
const m__LOONGARCH_SPFPSET = 32
const m__LOONGARCH_SZINT = 32
const m__LOONGARCH_SZLONG = 64
const m__LOONGARCH_SZPTR = 64
const m__LOONGARCH_TUNE = "la464"
const m__LOONGARCH_TUNE_LA464 = 1
const m__LP64 = 1
const m__MATH_H = 1
const m__POSIX2_CHAR_TERM = 200809
const m__POSIX2_C_BIND = "__POSIX2_THIS_VERSION"
const m__POSIX2_C_DEV = "__POSIX2_THIS_VERSION"
const m__POSIX2_C_VERSION = "__POSIX2_THIS_VERSION"
const m__POSIX2_LOCALEDEF = "__POSIX2_THIS_VERSION"
const m__POSIX2_SW_DEV = "__POSIX2_THIS_VERSION"
const m__POSIX2_VERSION = "__POSIX2_THIS_VERSION"
const m__POSIX_ADVISORY_INFO = 200809
const m__POSIX_ASYNCHRONOUS_IO = 200809
const m__POSIX_ASYNC_IO = 1
const m__POSIX_BARRIERS = 200809
const m__POSIX_CHOWN_RESTRICTED = 0
const m__POSIX_CLOCK_SELECTION = 200809
const m__POSIX_CPUTIME = 0
const m__POSIX_C_SOURCE = 200809
const m__POSIX_FSYNC = 200809
const m__POSIX_IPV6 = 200809
const m__POSIX_JOB_CONTROL = 1
const m__POSIX_MAPPED_FILES = 200809
const m__POSIX_MEMLOCK = 200809
const m__POSIX_MEMLOCK_RANGE = 200809
const m__POSIX_MEMORY_PROTECTION = 200809
const m__POSIX_MESSAGE_PASSING = 200809
const m__POSIX_MONOTONIC_CLOCK = 0
const m__POSIX_NO_TRUNC = 1
const m__POSIX_PRIORITIZED_IO = 200809
const m__POSIX_PRIORITY_SCHEDULING = 200809
const m__POSIX_RAW_SOCKETS = 200809
const m__POSIX_READER_WRITER_LOCKS = 200809
const m__POSIX_REALTIME_SIGNALS = 200809
const m__POSIX_REENTRANT_FUNCTIONS = 1
const m__POSIX_REGEXP = 1
const m__POSIX_SAVED_IDS = 1
const m__POSIX_SEMAPHORES = 200809
const m__POSIX_SHARED_MEMORY_OBJECTS = 200809
const m__POSIX_SHELL = 1
const m__POSIX_SOURCE = 1
const m__POSIX_SPAWN = 200809
const m__POSIX_SPIN_LOCKS = 200809
const m__POSIX_SYNCHRONIZED_IO = 200809
const m__POSIX_THREADS = 200809
const m__POSIX_THREAD_ATTR_STACKADDR = 200809
const m__POSIX_THREAD_ATTR_STACKSIZE = 200809
const m__POSIX_THREAD_CPUTIME = 0
const m__POSIX_THREAD_PRIORITY_SCHEDULING = 200809
const m__POSIX_THREAD_PRIO_INHERIT = 200809
const m__POSIX_THREAD_PRIO_PROTECT = 200809
const m__POSIX_THREAD_PROCESS_SHARED = 200809
const m__POSIX_THREAD_ROBUST_PRIO_INHERIT = 200809
const m__POSIX_THREAD_SAFE_FUNCTIONS = 200809
const m__POSIX_TIMEOUTS = 200809
const m__POSIX_TIMERS = 200809
const m__POSIX_V6_LP64_OFF64 = 1
const m__POSIX_V7_LP64_OFF64 = 1
const m__POSIX_VERSION = 200809
const m__SC_PAGE_SIZE = "_SC_PAGESIZE"
const m__STDC_PREDEF_H = 1
const m__STDIO_H = 1
const m__STDLIB_H = 1
const m__STRUCT_TIMESPEC = 1
const m__SYS_CDEFS_H = 1
const m__SYS_SELECT_H = 1
const m__SYS_TYPES_H = 1
const m__SYS_UCONTEXT_H = 1
const m__THREAD_MUTEX_INTERNAL_H = 1
const m__THREAD_SHARED_TYPES_H = 1
const m__UNISTD_H = 1
const m__XBS5_LP64_OFF64 = 1
const m__XOPEN_ENH_I18N = 1
const m__XOPEN_LEGACY = 1
const m__XOPEN_REALTIME = 1
const m__XOPEN_REALTIME_THREADS = 1
const m__XOPEN_SHM = 1
const m__XOPEN_UNIX = 1
const m__XOPEN_VERSION = 700
const m__XOPEN_XCU_VERSION = 4
const m__XOPEN_XPG2 = 1
const m__XOPEN_XPG3 = 1
const m__XOPEN_XPG4 = 1
const m___ACCUM_EPSILON__ = "0x1P-15K"
const m___ACCUM_FBIT__ = 15
const m___ACCUM_IBIT__ = 16
const m___ACCUM_MAX__ = "0X7FFFFFFFP-15K"
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BIG_ENDIAN = 4321
const m___BIT_TYPES_DEFINED__ = 1
const m___BLKCNT64_T_TYPE = "__SQUAD_TYPE"
const m___BLKCNT_T_TYPE = "__SLONGWORD_TYPE"
const m___BLKSIZE_T_TYPE = "__S32_TYPE"
const m___BYTE_ORDER = "__LITTLE_ENDIAN"
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___CLOCKID_T_TYPE = "__S32_TYPE"
const m___CLOCK_T_TYPE = "__SLONGWORD_TYPE"
const m___CPU_MASK_TYPE = "__ULONGWORD_TYPE"
const m___DADDR_T_TYPE = "__S32_TYPE"
const m___DA_FBIT__ = 31
const m___DA_IBIT__ = 32
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_IS_IEC_60559__ = 1
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DECIMAL_DIG__ = 36
const m___DEC_EVAL_METHOD__ = 2
const m___DEV_T_TYPE = "__UQUAD_TYPE"
const m___DQ_FBIT__ = 63
const m___DQ_IBIT__ = 0
const m___ELF__ = 1
const m___FD_SETSIZE = 1024
const m___FILE_defined = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT_WORD_ORDER = "__BYTE_ORDER"
const m___FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___FLT128_DECIMAL_DIG__ = 36
const m___FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT128_DIG__ = 33
const m___FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT128_HAS_DENORM__ = 1
const m___FLT128_HAS_INFINITY__ = 1
const m___FLT128_HAS_QUIET_NAN__ = 1
const m___FLT128_IS_IEC_60559__ = 1
const m___FLT128_MANT_DIG__ = 113
const m___FLT128_MAX_10_EXP__ = 4932
const m___FLT128_MAX_EXP__ = 16384
const m___FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_IS_IEC_60559__ = 1
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_IS_IEC_60559__ = 1
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT64X_DECIMAL_DIG__ = 36
const m___FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT64X_DIG__ = 33
const m___FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT64X_HAS_DENORM__ = 1
const m___FLT64X_HAS_INFINITY__ = 1
const m___FLT64X_HAS_QUIET_NAN__ = 1
const m___FLT64X_IS_IEC_60559__ = 1
const m___FLT64X_MANT_DIG__ = 113
const m___FLT64X_MAX_10_EXP__ = 4932
const m___FLT64X_MAX_EXP__ = 16384
const m___FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_IS_IEC_60559__ = 1
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_IS_IEC_60559__ = 1
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_RADIX__ = 2
const m___FP_FAST_FMA = 1
const m___FP_FAST_FMAF = 1
const m___FP_FAST_FMAF32 = 1
const m___FP_FAST_FMAF32x = 1
const m___FP_FAST_FMAF64 = 1
const m___FP_LOGB0_IS_MIN = 0
const m___FP_LOGBNAN_IS_MIN = 0
const m___FRACT_EPSILON__ = "0x1P-15R"
const m___FRACT_FBIT__ = 15
const m___FRACT_IBIT__ = 0
const m___FRACT_MAX__ = "0X7FFFP-15R"
const m___FSBLKCNT64_T_TYPE = "__UQUAD_TYPE"
const m___FSBLKCNT_T_TYPE = "__ULONGWORD_TYPE"
const m___FSFILCNT64_T_TYPE = "__UQUAD_TYPE"
const m___FSFILCNT_T_TYPE = "__ULONGWORD_TYPE"
const m___FSWORD_T_TYPE = "__SWORD_TYPE"
const m___FUNCTION__ = "__func__"
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GID_T_TYPE = "__U32_TYPE"
const m___GLIBC_FLT_EVAL_METHOD = "__FLT_EVAL_METHOD__"
const m___GLIBC_MINOR__ = 38
const m___GLIBC_USE_C2X_STRTOL = 0
const m___GLIBC_USE_DEPRECATED_GETS = 0
const m___GLIBC_USE_DEPRECATED_SCANF = 0
const m___GLIBC_USE_IEC_60559_BFP_EXT = 0
const m___GLIBC_USE_IEC_60559_BFP_EXT_C2X = 0
const m___GLIBC_USE_IEC_60559_EXT = 0
const m___GLIBC_USE_IEC_60559_FUNCS_EXT = 0
const m___GLIBC_USE_IEC_60559_FUNCS_EXT_C2X = 0
const m___GLIBC_USE_IEC_60559_TYPES_EXT = 0
const m___GLIBC_USE_ISOC2X = 0
const m___GLIBC_USE_LIB_EXT2 = 0
const m___GLIBC__ = 2
const m___GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 1
const m___GNUC_RH_RELEASE__ = 2
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const m___GNUC__ = 13
const m___GNU_LIBRARY__ = 6
const m___GXX_ABI_VERSION = 1018
const m___HAVE_DISTINCT_FLOAT128 = 0
const m___HAVE_DISTINCT_FLOAT128X = "__HAVE_FLOAT128X"
const m___HAVE_DISTINCT_FLOAT16 = "__HAVE_FLOAT16"
const m___HAVE_DISTINCT_FLOAT32 = 0
const m___HAVE_DISTINCT_FLOAT32X = 0
const m___HAVE_DISTINCT_FLOAT64 = 0
const m___HAVE_DISTINCT_FLOAT64X = 0
const m___HAVE_FLOAT128 = 1
const m___HAVE_FLOAT128X = 0
const m___HAVE_FLOAT16 = 0
const m___HAVE_FLOAT32 = 1
const m___HAVE_FLOAT32X = 1
const m___HAVE_FLOAT64 = 1
const m___HAVE_FLOAT64X = "__HAVE_FLOAT128"
const m___HAVE_FLOAT64X_LONG_DOUBLE = "__HAVE_FLOAT128"
const m___HAVE_FLOATN_NOT_TYPEDEF = 1
const m___HAVE_GENERIC_SELECTION = 1
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___HA_FBIT__ = 7
const m___HA_IBIT__ = 8
const m___HQ_FBIT__ = 15
const m___HQ_IBIT__ = 0
const m___ID_T_TYPE = "__U32_TYPE"
const m___INO64_T_TYPE = "__UQUAD_TYPE"
const m___INO_T_MATCHES_INO64_T = 1
const m___INO_T_TYPE = "__ULONGWORD_TYPE"
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffffffffffff
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_MAX__ = 0x7fffffffffffffff
const m___INT_FAST16_WIDTH__ = 64
const m___INT_FAST32_MAX__ = 0x7fffffffffffffff
const m___INT_FAST32_WIDTH__ = 64
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___KEY_T_TYPE = "__S32_TYPE"
const m___LACCUM_EPSILON__ = "0x1P-31LK"
const m___LACCUM_FBIT__ = 31
const m___LACCUM_IBIT__ = 32
const m___LACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LK"
const m___LDBL_DECIMAL_DIG__ = 36
const m___LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___LDBL_DIG__ = 33
const m___LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_IS_IEC_60559__ = 1
const m___LDBL_MANT_DIG__ = 113
const m___LDBL_MAX_10_EXP__ = 4932
const m___LDBL_MAX_EXP__ = 16384
const m___LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = 0
const m___LFRACT_EPSILON__ = "0x1P-31LR"
const m___LFRACT_FBIT__ = 31
const m___LFRACT_IBIT__ = 0
const m___LFRACT_MAX__ = "0X7FFFFFFFP-31LR"
const m___LITTLE_ENDIAN = 1234
const m___LLACCUM_EPSILON__ = "0x1P-63LLK"
const m___LLACCUM_FBIT__ = 63
const m___LLACCUM_IBIT__ = 64
const m___LLACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFP-63LLK"
const m___LLFRACT_EPSILON__ = "0x1P-63LLR"
const m___LLFRACT_FBIT__ = 63
const m___LLFRACT_IBIT__ = 0
const m___LLFRACT_MAX__ = "0X7FFFFFFFFFFFFFFFP-63LLR"
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___MATH_DECLARE_LDOUBLE = 1
const m___MODE_T_TYPE = "__U32_TYPE"
const m___NLINK_T_TYPE = "__U32_TYPE"
const m___NO_INLINE__ = 1
const m___OFF64_T_TYPE = "__SQUAD_TYPE"
const m___OFF_T_MATCHES_OFF64_T = 1
const m___OFF_T_TYPE = "__SLONGWORD_TYPE"
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PDP_ENDIAN = 3412
const m___PID_T_TYPE = "__S32_TYPE"
const m___POSIX2_THIS_VERSION = 200809
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTHREAD_MUTEX_HAVE_PREV = 1
const m___PTRDIFF_MAX__ = 0x7fffffffffffffff
const m___PTRDIFF_WIDTH__ = 64
const m___QQ_FBIT__ = 7
const m___QQ_IBIT__ = 0
const m___REDIRECT_FORTIFY = "__REDIRECT"
const m___REDIRECT_FORTIFY_NTH = "__REDIRECT_NTH"
const m___REGISTER_PREFIX__ = "$"
const m___RLIM64_T_TYPE = "__UQUAD_TYPE"
const m___RLIM_T_MATCHES_RLIM64_T = 1
const m___RLIM_T_TYPE = "__ULONGWORD_TYPE"
const m___S32_TYPE = "int"
const m___SACCUM_EPSILON__ = "0x1P-7HK"
const m___SACCUM_FBIT__ = 7
const m___SACCUM_IBIT__ = 8
const m___SACCUM_MAX__ = "0X7FFFP-7HK"
const m___SA_FBIT__ = 15
const m___SA_IBIT__ = 16
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SFRACT_EPSILON__ = "0x1P-7HR"
const m___SFRACT_FBIT__ = 7
const m___SFRACT_IBIT__ = 0
const m___SFRACT_MAX__ = "0X7FP-7HR"
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIGEV_MAX_SIZE = 64
const m___SIGRTMAX = 64
const m___SIGRTMIN = 32
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTHREAD_ATTR_T = 56
const m___SIZEOF_PTHREAD_BARRIERATTR_T = 4
const m___SIZEOF_PTHREAD_BARRIER_T = 32
const m___SIZEOF_PTHREAD_CONDATTR_T = 4
const m___SIZEOF_PTHREAD_COND_T = 48
const m___SIZEOF_PTHREAD_MUTEXATTR_T = 4
const m___SIZEOF_PTHREAD_MUTEX_T = 40
const m___SIZEOF_PTHREAD_RWLOCKATTR_T = 8
const m___SIZEOF_PTHREAD_RWLOCK_T = 56
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffffffffffff
const m___SIZE_WIDTH__ = 64
const m___SI_ASYNCIO_AFTER_SIGIO = 1
const m___SI_CLOCK_T = "__clock_t"
const m___SI_ERRNO_THEN_CODE = 1
const m___SI_HAVE_SIGSYS = 1
const m___SI_MAX_SIZE = 128
const m___SLONG32_TYPE = "int"
const m___SQ_FBIT__ = 31
const m___SQ_IBIT__ = 0
const m___SSIZE_T_TYPE = "__SWORD_TYPE"
const m___STATFS_MATCHES_STATFS64 = 1
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_IEC_60559_BFP__ = 201404
const m___STDC_IEC_60559_COMPLEX__ = 201404
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC__ = 1
const m___SUSECONDS64_T_TYPE = "__SQUAD_TYPE"
const m___SUSECONDS_T_TYPE = "__SLONGWORD_TYPE"
const m___SYSCALL_SLONG_TYPE = "__SLONGWORD_TYPE"
const m___SYSCALL_ULONG_TYPE = "__ULONGWORD_TYPE"
const m___TA_FBIT__ = 63
const m___TA_IBIT__ = 64
const m___TIME64_T_TYPE = "__TIME_T_TYPE"
const m___TIMESIZE = 64
const m___TIME_T_TYPE = "__SLONGWORD_TYPE"
const m___TQ_FBIT__ = 127
const m___TQ_IBIT__ = 0
const m___UACCUM_EPSILON__ = "0x1P-16UK"
const m___UACCUM_FBIT__ = 16
const m___UACCUM_IBIT__ = 16
const m___UACCUM_MAX__ = "0XFFFFFFFFP-16UK"
const m___UACCUM_MIN__ = "0.0UK"
const m___UDA_FBIT__ = 32
const m___UDA_IBIT__ = 32
const m___UDQ_FBIT__ = 64
const m___UDQ_IBIT__ = 0
const m___UFRACT_EPSILON__ = "0x1P-16UR"
const m___UFRACT_FBIT__ = 16
const m___UFRACT_IBIT__ = 0
const m___UFRACT_MAX__ = "0XFFFFP-16UR"
const m___UFRACT_MIN__ = "0.0UR"
const m___UHA_FBIT__ = 8
const m___UHA_IBIT__ = 8
const m___UHQ_FBIT__ = 16
const m___UHQ_IBIT__ = 0
const m___UID_T_TYPE = "__U32_TYPE"
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = 0xffffffffffffffff
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = 0xffffffffffffffff
const m___UINTPTR_MAX__ = 0xffffffffffffffff
const m___UINT_FAST16_MAX__ = 0xffffffffffffffff
const m___UINT_FAST32_MAX__ = 0xffffffffffffffff
const m___UINT_FAST64_MAX__ = 0xffffffffffffffff
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = 0xffffffffffffffff
const m___UINT_LEAST8_MAX__ = 0xff
const m___ULACCUM_EPSILON__ = "0x1P-32ULK"
const m___ULACCUM_FBIT__ = 32
const m___ULACCUM_IBIT__ = 32
const m___ULACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULK"
const m___ULACCUM_MIN__ = "0.0ULK"
const m___ULFRACT_EPSILON__ = "0x1P-32ULR"
const m___ULFRACT_FBIT__ = 32
const m___ULFRACT_IBIT__ = 0
const m___ULFRACT_MAX__ = "0XFFFFFFFFP-32ULR"
const m___ULFRACT_MIN__ = "0.0ULR"
const m___ULLACCUM_EPSILON__ = "0x1P-64ULLK"
const m___ULLACCUM_FBIT__ = 64
const m___ULLACCUM_IBIT__ = 64
const m___ULLACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFP-64ULLK"
const m___ULLACCUM_MIN__ = "0.0ULLK"
const m___ULLFRACT_EPSILON__ = "0x1P-64ULLR"
const m___ULLFRACT_FBIT__ = 64
const m___ULLFRACT_IBIT__ = 0
const m___ULLFRACT_MAX__ = "0XFFFFFFFFFFFFFFFFP-64ULLR"
const m___ULLFRACT_MIN__ = "0.0ULLR"
const m___UQQ_FBIT__ = 8
const m___UQQ_IBIT__ = 0
const m___USACCUM_EPSILON__ = "0x1P-8UHK"
const m___USACCUM_FBIT__ = 8
const m___USACCUM_IBIT__ = 8
const m___USACCUM_MAX__ = "0XFFFFP-8UHK"
const m___USACCUM_MIN__ = "0.0UHK"
const m___USA_FBIT__ = 16
const m___USA_IBIT__ = 16
const m___USECONDS_T_TYPE = "__U32_TYPE"
const m___USE_ATFILE = 1
const m___USE_FORTIFY_LEVEL = 0
const m___USE_ISOC11 = 1
const m___USE_ISOC95 = 1
const m___USE_ISOC99 = 1
const m___USE_MISC = 1
const m___USE_POSIX = 1
const m___USE_POSIX199309 = 1
const m___USE_POSIX199506 = 1
const m___USE_POSIX2 = 1
const m___USE_POSIX_IMPLICITLY = 1
const m___USE_XOPEN2K = 1
const m___USE_XOPEN2K8 = 1
const m___USFRACT_EPSILON__ = "0x1P-8UHR"
const m___USFRACT_FBIT__ = 8
const m___USFRACT_IBIT__ = 0
const m___USFRACT_MAX__ = "0XFFP-8UHR"
const m___USFRACT_MIN__ = "0.0UHR"
const m___USQ_FBIT__ = 32
const m___USQ_IBIT__ = 0
const m___UTA_FBIT__ = 64
const m___UTA_IBIT__ = 64
const m___UTQ_FBIT__ = 128
const m___UTQ_IBIT__ = 0
const m___VERSION__ = "13.2.1 20230728 (Red Hat 13.2.1-2)"
const m___WALL = 0x40000000
const m___WCHAR_MAX__ = 0x7fffffff
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WCLONE = 0x80000000
const m___WCOREFLAG = 0x80
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___WNOTHREAD = 0x20000000
const m___WORDSIZE = 64
const m___WORDSIZE_TIME64_COMPAT32 = 0
const m___W_CONTINUED = 0xffff
const m_____FILE_defined = 1
const m______fpos64_t_defined = 1
const m______fpos_t_defined = 1
const m_____mbstate_t_defined = 1
const m___bitwise__ = "__bitwise"
const m___clock_t_defined = 1
const m___clockid_t_defined = 1
const m___cookie_io_functions_t_defined = 1
const m___glibc_c99_flexarr_available = 1
const m___gnu_linux__ = 1
const m___have_pthread_attr_t = 1
const m___ldiv_t_defined = 1
const m___linux = 1
const m___linux__ = 1
const m___lldiv_t_defined = 1
const m___loongarch64 = 1
const m___loongarch__ = 1
const m___loongarch_double_float = 1
const m___loongarch_frlen = 64
const m___loongarch_grlen = 64
const m___loongarch_hard_float = 1
const m___loongarch_lp64 = 1
const m___sig_atomic_t_defined = 1
const m___sigevent_t_defined = 1
const m___siginfo_t_defined = 1
const m___sigset_t_defined = 1
const m___sigstack_defined = 1
const m___stack_t_defined = 1
const m___struct_FILE_defined = 1
const m___time_t_defined = 1
const m___timer_t_defined = 1
const m___timeval_defined = 1
const m___unix = 1
const m___unix__ = 1
const m_linux = 1
const m_sigcontext_struct = "sigcontext"
const m_unix = 1

type T__builtin_va_list = uintptr

type T__predefined_size_t = uint64

type T__predefined_wchar_t = int32

type T__predefined_ptrdiff_t = int64

type Tsize_t = uint64

type T__gnuc_va_list = uintptr

type T__u_char = uint8

type T__u_short = uint16

type T__u_int = uint32

type T__u_long = uint64

type T__int8_t = int8

type T__uint8_t = uint8

type T__int16_t = int16

type T__uint16_t = uint16

type T__int32_t = int32

type T__uint32_t = uint32

type T__int64_t = int64

type T__uint64_t = uint64

type T__int_least8_t = int8

type T__uint_least8_t = uint8

type T__int_least16_t = int16

type T__uint_least16_t = uint16

type T__int_least32_t = int32

type T__uint_least32_t = uint32

type T__int_least64_t = int64

type T__uint_least64_t = uint64

type T__quad_t = int64

type T__u_quad_t = uint64

type T__intmax_t = int64

type T__uintmax_t = uint64

type T__dev_t = uint64

type T__uid_t = uint32

type T__gid_t = uint32

type T__ino_t = uint64

type T__ino64_t = uint64

type T__mode_t = uint32

type T__nlink_t = uint32

type T__off_t = int64

type T__off64_t = int64

type T__pid_t = int32

type T__fsid_t = struct {
	F__val [2]int32
}

type T__clock_t = int64

type T__rlim_t = uint64

type T__rlim64_t = uint64

type T__id_t = uint32

type T__time_t = int64

type T__useconds_t = uint32

type T__suseconds_t = int64

type T__suseconds64_t = int64

type T__daddr_t = int32

type T__key_t = int32

type T__clockid_t = int32

type T__timer_t = uintptr

type T__blksize_t = int32

type T__blkcnt_t = int64

type T__blkcnt64_t = int64

type T__fsblkcnt_t = uint64

type T__fsblkcnt64_t = uint64

type T__fsfilcnt_t = uint64

type T__fsfilcnt64_t = uint64

type T__fsword_t = int64

type T__ssize_t = int64

type T__syscall_slong_t = int64

type T__syscall_ulong_t = uint64

type T__loff_t = int64

type T__caddr_t = uintptr

type T__intptr_t = int64

type T__socklen_t = uint32

type T__sig_atomic_t = int32

type T__mbstate_t = struct {
	F__count int32
	F__value struct {
		F__wchb [0][4]int8
		F__wch  uint32
	}
}

type T__fpos_t = struct {
	F__pos   T__off_t
	F__state T__mbstate_t
}

type T_G_fpos_t = T__fpos_t

type T__fpos64_t = struct {
	F__pos   T__off64_t
	F__state T__mbstate_t
}

type T_G_fpos64_t = T__fpos64_t

type T_IO_FILE = struct {
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     T__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]int8
	F_lock           uintptr
	F_offset         T__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [20]int8
}

type T__FILE = struct {
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     T__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]int8
	F_lock           uintptr
	F_offset         T__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [20]int8
}

type TFILE = struct {
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     T__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]int8
	F_lock           uintptr
	F_offset         T__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [20]int8
}

type T_IO_lock_t = struct{}

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type T_IO_cookie_io_functions_t = Tcookie_io_functions_t

type Tva_list = uintptr

type Toff_t = int64

type Tssize_t = int64

type Tfpos_t = struct {
	F__pos   T__off_t
	F__state T__mbstate_t
}

type Twchar_t = int32

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tu_char = uint8

type Tu_short = uint16

type Tu_int = uint32

type Tu_long = uint64

type Tquad_t = int64

type Tu_quad_t = uint64

type Tfsid_t = struct {
	F__val [2]int32
}

type Tloff_t = int64

type Tino_t = uint64

type Tdev_t = uint64

type Tgid_t = uint32

type Tmode_t = uint32

type Tnlink_t = uint32

type Tuid_t = uint32

type Tpid_t = int32

type Tid_t = uint32

type Tdaddr_t = int32

type Tcaddr_t = uintptr

type Tkey_t = int32

type Tclock_t = int64

type Tclockid_t = int32

type Ttime_t = int64

type Ttimer_t = uintptr

type Tulong = uint64

type Tushort = uint16

type Tuint = uint32

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tu_int64_t = uint64

type Tregister_t = int32

type T__sigset_t = struct {
	F__val [16]uint64
}

type Tsigset_t = struct {
	F__val [16]uint64
}

type Ttimeval = struct {
	Ftv_sec  T__time_t
	Ftv_usec T__suseconds_t
}

type Ttimespec = struct {
	Ftv_sec  T__time_t
	Ftv_nsec T__syscall_slong_t
}

type Tsuseconds_t = int64

type T__fd_mask = int64

type Tfd_set = struct {
	F__fds_bits [16]T__fd_mask
}

type Tfd_mask = int64

type Tblksize_t = int32

type Tblkcnt_t = int64

type Tfsblkcnt_t = uint64

type Tfsfilcnt_t = uint64

type T__atomic_wide_counter = struct {
	F__value32 [0]struct {
		F__low  uint32
		F__high uint32
	}
	F__value64 uint64
}

type T__pthread_list_t = struct {
	F__prev uintptr
	F__next uintptr
}

type T__pthread_internal_list = T__pthread_list_t

type T__pthread_slist_t = struct {
	F__next uintptr
}

type T__pthread_internal_slist = T__pthread_slist_t

type T__pthread_mutex_s = struct {
	F__lock   int32
	F__count  uint32
	F__owner  int32
	F__nusers uint32
	F__kind   int32
	F__spins  int32
	F__list   T__pthread_list_t
}

type T__pthread_rwlock_arch_t = struct {
	F__readers       uint32
	F__writers       uint32
	F__wrphase_futex uint32
	F__writers_futex uint32
	F__pad3          uint32
	F__pad4          uint32
	F__flags         uint8
	F__shared        uint8
	F__pad1          uint8
	F__pad2          uint8
	F__cur_writer    int32
}

type T__pthread_cond_s = struct {
	F__wseq         T__atomic_wide_counter
	F__g1_start     T__atomic_wide_counter
	F__g_refs       [2]uint32
	F__g_size       [2]uint32
	F__g1_orig_size uint32
	F__wrefs        uint32
	F__g_signals    [2]uint32
}

type T__tss_t = uint32

type T__thrd_t = uint64

type T__once_flag = struct {
	F__data int32
}

type Tpthread_t = uint64

type Tpthread_mutexattr_t = struct {
	F__align [0]int32
	F__size  [4]int8
}

type Tpthread_condattr_t = struct {
	F__align [0]int32
	F__size  [4]int8
}

type Tpthread_key_t = uint32

type Tpthread_once_t = int32

type Tpthread_attr_t1 = struct {
	F__align [0]int64
	F__size  [56]int8
}

type Tpthread_attr_t = struct {
	F__align [0]int64
	F__size  [56]int8
}

type Tpthread_mutex_t = struct {
	F__size  [0][40]int8
	F__align [0]int64
	F__data  T__pthread_mutex_s
}

type Tpthread_cond_t = struct {
	F__size  [0][48]int8
	F__align [0]int64
	F__data  T__pthread_cond_s
}

type Tpthread_rwlock_t = struct {
	F__size      [0][56]int8
	F__align     [0]int64
	F__data      T__pthread_rwlock_arch_t
	F__ccgo_pad3 [24]byte
}

type Tpthread_rwlockattr_t = struct {
	F__align [0]int64
	F__size  [8]int8
}

type Tpthread_spinlock_t = int32

type Tpthread_barrier_t = struct {
	F__align [0]int64
	F__size  [32]int8
}

type Tpthread_barrierattr_t = struct {
	F__align [0]int32
	F__size  [4]int8
}

type Trandom_data = struct {
	Ffptr      uintptr
	Frptr      uintptr
	Fstate     uintptr
	Frand_type int32
	Frand_deg  int32
	Frand_sep  int32
	Fend_ptr   uintptr
}

type Tdrand48_data = struct {
	F__x     [3]uint16
	F__old_x [3]uint16
	F__c     uint16
	F__init  uint16
	F__a     uint64
}

type T__compar_fn_t = uintptr

type Tuseconds_t = uint32

type Tintptr_t = int64

type Tsocklen_t = uint32

const _PC_LINK_MAX = 0
const _PC_MAX_CANON = 1
const _PC_MAX_INPUT = 2
const _PC_NAME_MAX = 3
const _PC_PATH_MAX = 4
const _PC_PIPE_BUF = 5
const _PC_CHOWN_RESTRICTED = 6
const _PC_NO_TRUNC = 7
const _PC_VDISABLE = 8
const _PC_SYNC_IO = 9
const _PC_ASYNC_IO = 10
const _PC_PRIO_IO = 11
const _PC_SOCK_MAXBUF = 12
const _PC_FILESIZEBITS = 13
const _PC_REC_INCR_XFER_SIZE = 14
const _PC_REC_MAX_XFER_SIZE = 15
const _PC_REC_MIN_XFER_SIZE = 16
const _PC_REC_XFER_ALIGN = 17
const _PC_ALLOC_SIZE_MIN = 18
const _PC_SYMLINK_MAX = 19
const _PC_2_SYMLINKS = 20
const _SC_ARG_MAX = 0
const _SC_CHILD_MAX = 1
const _SC_CLK_TCK = 2
const _SC_NGROUPS_MAX = 3
const _SC_OPEN_MAX = 4
const _SC_STREAM_MAX = 5
const _SC_TZNAME_MAX = 6
const _SC_JOB_CONTROL = 7
const _SC_SAVED_IDS = 8
const _SC_REALTIME_SIGNALS = 9
const _SC_PRIORITY_SCHEDULING = 10
const _SC_TIMERS = 11
const _SC_ASYNCHRONOUS_IO = 12
const _SC_PRIORITIZED_IO = 13
const _SC_SYNCHRONIZED_IO = 14
const _SC_FSYNC = 15
const _SC_MAPPED_FILES = 16
const _SC_MEMLOCK = 17
const _SC_MEMLOCK_RANGE = 18
const _SC_MEMORY_PROTECTION = 19
const _SC_MESSAGE_PASSING = 20
const _SC_SEMAPHORES = 21
const _SC_SHARED_MEMORY_OBJECTS = 22
const _SC_AIO_LISTIO_MAX = 23
const _SC_AIO_MAX = 24
const _SC_AIO_PRIO_DELTA_MAX = 25
const _SC_DELAYTIMER_MAX = 26
const _SC_MQ_OPEN_MAX = 27
const _SC_MQ_PRIO_MAX = 28
const _SC_VERSION = 29
const _SC_PAGESIZE = 30
const _SC_RTSIG_MAX = 31
const _SC_SEM_NSEMS_MAX = 32
const _SC_SEM_VALUE_MAX = 33
const _SC_SIGQUEUE_MAX = 34
const _SC_TIMER_MAX = 35
const _SC_BC_BASE_MAX = 36
const _SC_BC_DIM_MAX = 37
const _SC_BC_SCALE_MAX = 38
const _SC_BC_STRING_MAX = 39
const _SC_COLL_WEIGHTS_MAX = 40
const _SC_EQUIV_CLASS_MAX = 41
const _SC_EXPR_NEST_MAX = 42
const _SC_LINE_MAX = 43
const _SC_RE_DUP_MAX = 44
const _SC_CHARCLASS_NAME_MAX = 45
const _SC_2_VERSION = 46
const _SC_2_C_BIND = 47
const _SC_2_C_DEV = 48
const _SC_2_FORT_DEV = 49
const _SC_2_FORT_RUN = 50
const _SC_2_SW_DEV = 51
const _SC_2_LOCALEDEF = 52
const _SC_PII = 53
const _SC_PII_XTI = 54
const _SC_PII_SOCKET = 55
const _SC_PII_INTERNET = 56
const _SC_PII_OSI = 57
const _SC_POLL = 58
const _SC_SELECT = 59
const _SC_UIO_MAXIOV = 60
const _SC_IOV_MAX = 60
const _SC_PII_INTERNET_STREAM = 61
const _SC_PII_INTERNET_DGRAM = 62
const _SC_PII_OSI_COTS = 63
const _SC_PII_OSI_CLTS = 64
const _SC_PII_OSI_M = 65
const _SC_T_IOV_MAX = 66
const _SC_THREADS = 67
const _SC_THREAD_SAFE_FUNCTIONS = 68
const _SC_GETGR_R_SIZE_MAX = 69
const _SC_GETPW_R_SIZE_MAX = 70
const _SC_LOGIN_NAME_MAX = 71
const _SC_TTY_NAME_MAX = 72
const _SC_THREAD_DESTRUCTOR_ITERATIONS = 73
const _SC_THREAD_KEYS_MAX = 74
const _SC_THREAD_STACK_MIN = 75
const _SC_THREAD_THREADS_MAX = 76
const _SC_THREAD_ATTR_STACKADDR = 77
const _SC_THREAD_ATTR_STACKSIZE = 78
const _SC_THREAD_PRIORITY_SCHEDULING = 79
const _SC_THREAD_PRIO_INHERIT = 80
const _SC_THREAD_PRIO_PROTECT = 81
const _SC_THREAD_PROCESS_SHARED = 82
const _SC_NPROCESSORS_CONF = 83
const _SC_NPROCESSORS_ONLN = 84
const _SC_PHYS_PAGES = 85
const _SC_AVPHYS_PAGES = 86
const _SC_ATEXIT_MAX = 87
const _SC_PASS_MAX = 88
const _SC_XOPEN_VERSION = 89
const _SC_XOPEN_XCU_VERSION = 90
const _SC_XOPEN_UNIX = 91
const _SC_XOPEN_CRYPT = 92
const _SC_XOPEN_ENH_I18N = 93
const _SC_XOPEN_SHM = 94
const _SC_2_CHAR_TERM = 95
const _SC_2_C_VERSION = 96
const _SC_2_UPE = 97
const _SC_XOPEN_XPG2 = 98
const _SC_XOPEN_XPG3 = 99
const _SC_XOPEN_XPG4 = 100
const _SC_CHAR_BIT = 101
const _SC_CHAR_MAX = 102
const _SC_CHAR_MIN = 103
const _SC_INT_MAX = 104
const _SC_INT_MIN = 105
const _SC_LONG_BIT = 106
const _SC_WORD_BIT = 107
const _SC_MB_LEN_MAX = 108
const _SC_NZERO = 109
const _SC_SSIZE_MAX = 110
const _SC_SCHAR_MAX = 111
const _SC_SCHAR_MIN = 112
const _SC_SHRT_MAX = 113
const _SC_SHRT_MIN = 114
const _SC_UCHAR_MAX = 115
const _SC_UINT_MAX = 116
const _SC_ULONG_MAX = 117
const _SC_USHRT_MAX = 118
const _SC_NL_ARGMAX = 119
const _SC_NL_LANGMAX = 120
const _SC_NL_MSGMAX = 121
const _SC_NL_NMAX = 122
const _SC_NL_SETMAX = 123
const _SC_NL_TEXTMAX = 124
const _SC_XBS5_ILP32_OFF32 = 125
const _SC_XBS5_ILP32_OFFBIG = 126
const _SC_XBS5_LP64_OFF64 = 127
const _SC_XBS5_LPBIG_OFFBIG = 128
const _SC_XOPEN_LEGACY = 129
const _SC_XOPEN_REALTIME = 130
const _SC_XOPEN_REALTIME_THREADS = 131
const _SC_ADVISORY_INFO = 132
const _SC_BARRIERS = 133
const _SC_BASE = 134
const _SC_C_LANG_SUPPORT = 135
const _SC_C_LANG_SUPPORT_R = 136
const _SC_CLOCK_SELECTION = 137
const _SC_CPUTIME = 138
const _SC_THREAD_CPUTIME = 139
const _SC_DEVICE_IO = 140
const _SC_DEVICE_SPECIFIC = 141
const _SC_DEVICE_SPECIFIC_R = 142
const _SC_FD_MGMT = 143
const _SC_FIFO = 144
const _SC_PIPE = 145
const _SC_FILE_ATTRIBUTES = 146
const _SC_FILE_LOCKING = 147
const _SC_FILE_SYSTEM = 148
const _SC_MONOTONIC_CLOCK = 149
const _SC_MULTI_PROCESS = 150
const _SC_SINGLE_PROCESS = 151
const _SC_NETWORKING = 152
const _SC_READER_WRITER_LOCKS = 153
const _SC_SPIN_LOCKS = 154
const _SC_REGEXP = 155
const _SC_REGEX_VERSION = 156
const _SC_SHELL = 157
const _SC_SIGNALS = 158
const _SC_SPAWN = 159
const _SC_SPORADIC_SERVER = 160
const _SC_THREAD_SPORADIC_SERVER = 161
const _SC_SYSTEM_DATABASE = 162
const _SC_SYSTEM_DATABASE_R = 163
const _SC_TIMEOUTS = 164
const _SC_TYPED_MEMORY_OBJECTS = 165
const _SC_USER_GROUPS = 166
const _SC_USER_GROUPS_R = 167
const _SC_2_PBS = 168
const _SC_2_PBS_ACCOUNTING = 169
const _SC_2_PBS_LOCATE = 170
const _SC_2_PBS_MESSAGE = 171
const _SC_2_PBS_TRACK = 172
const _SC_SYMLOOP_MAX = 173
const _SC_STREAMS = 174
const _SC_2_PBS_CHECKPOINT = 175
const _SC_V6_ILP32_OFF32 = 176
const _SC_V6_ILP32_OFFBIG = 177
const _SC_V6_LP64_OFF64 = 178
const _SC_V6_LPBIG_OFFBIG = 179
const _SC_HOST_NAME_MAX = 180
const _SC_TRACE = 181
const _SC_TRACE_EVENT_FILTER = 182
const _SC_TRACE_INHERIT = 183
const _SC_TRACE_LOG = 184
const _SC_LEVEL1_ICACHE_SIZE = 185
const _SC_LEVEL1_ICACHE_ASSOC = 186
const _SC_LEVEL1_ICACHE_LINESIZE = 187
const _SC_LEVEL1_DCACHE_SIZE = 188
const _SC_LEVEL1_DCACHE_ASSOC = 189
const _SC_LEVEL1_DCACHE_LINESIZE = 190
const _SC_LEVEL2_CACHE_SIZE = 191
const _SC_LEVEL2_CACHE_ASSOC = 192
const _SC_LEVEL2_CACHE_LINESIZE = 193
const _SC_LEVEL3_CACHE_SIZE = 194
const _SC_LEVEL3_CACHE_ASSOC = 195
const _SC_LEVEL3_CACHE_LINESIZE = 196
const _SC_LEVEL4_CACHE_SIZE = 197
const _SC_LEVEL4_CACHE_ASSOC = 198
const _SC_LEVEL4_CACHE_LINESIZE = 199
const _SC_IPV6 = 235
const _SC_RAW_SOCKETS = 236
const _SC_V7_ILP32_OFF32 = 237
const _SC_V7_ILP32_OFFBIG = 238
const _SC_V7_LP64_OFF64 = 239
const _SC_V7_LPBIG_OFFBIG = 240
const _SC_SS_REPL_MAX = 241
const _SC_TRACE_EVENT_NAME_MAX = 242
const _SC_TRACE_NAME_MAX = 243
const _SC_TRACE_SYS_MAX = 244
const _SC_TRACE_USER_EVENT_MAX = 245
const _SC_XOPEN_STREAMS = 246
const _SC_THREAD_ROBUST_PRIO_INHERIT = 247
const _SC_THREAD_ROBUST_PRIO_PROTECT = 248
const _SC_MINSIGSTKSZ = 249
const _SC_SIGSTKSZ = 250
const _CS_PATH = 0
const _CS_V6_WIDTH_RESTRICTED_ENVS = 1
const _CS_GNU_LIBC_VERSION = 2
const _CS_GNU_LIBPTHREAD_VERSION = 3
const _CS_V5_WIDTH_RESTRICTED_ENVS = 4
const _CS_V7_WIDTH_RESTRICTED_ENVS = 5
const _CS_LFS_CFLAGS = 1000
const _CS_LFS_LDFLAGS = 1001
const _CS_LFS_LIBS = 1002
const _CS_LFS_LINTFLAGS = 1003
const _CS_LFS64_CFLAGS = 1004
const _CS_LFS64_LDFLAGS = 1005
const _CS_LFS64_LIBS = 1006
const _CS_LFS64_LINTFLAGS = 1007
const _CS_XBS5_ILP32_OFF32_CFLAGS = 1100
const _CS_XBS5_ILP32_OFF32_LDFLAGS = 1101
const _CS_XBS5_ILP32_OFF32_LIBS = 1102
const _CS_XBS5_ILP32_OFF32_LINTFLAGS = 1103
const _CS_XBS5_ILP32_OFFBIG_CFLAGS = 1104
const _CS_XBS5_ILP32_OFFBIG_LDFLAGS = 1105
const _CS_XBS5_ILP32_OFFBIG_LIBS = 1106
const _CS_XBS5_ILP32_OFFBIG_LINTFLAGS = 1107
const _CS_XBS5_LP64_OFF64_CFLAGS = 1108
const _CS_XBS5_LP64_OFF64_LDFLAGS = 1109
const _CS_XBS5_LP64_OFF64_LIBS = 1110
const _CS_XBS5_LP64_OFF64_LINTFLAGS = 1111
const _CS_XBS5_LPBIG_OFFBIG_CFLAGS = 1112
const _CS_XBS5_LPBIG_OFFBIG_LDFLAGS = 1113
const _CS_XBS5_LPBIG_OFFBIG_LIBS = 1114
const _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS = 1115
const _CS_POSIX_V6_ILP32_OFF32_CFLAGS = 1116
const _CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 1117
const _CS_POSIX_V6_ILP32_OFF32_LIBS = 1118
const _CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = 1119
const _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 1120
const _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 1121
const _CS_POSIX_V6_ILP32_OFFBIG_LIBS = 1122
const _CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = 1123
const _CS_POSIX_V6_LP64_OFF64_CFLAGS = 1124
const _CS_POSIX_V6_LP64_OFF64_LDFLAGS = 1125
const _CS_POSIX_V6_LP64_OFF64_LIBS = 1126
const _CS_POSIX_V6_LP64_OFF64_LINTFLAGS = 1127
const _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 1128
const _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 1129
const _CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 1130
const _CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = 1131
const _CS_POSIX_V7_ILP32_OFF32_CFLAGS = 1132
const _CS_POSIX_V7_ILP32_OFF32_LDFLAGS = 1133
const _CS_POSIX_V7_ILP32_OFF32_LIBS = 1134
const _CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = 1135
const _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = 1136
const _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = 1137
const _CS_POSIX_V7_ILP32_OFFBIG_LIBS = 1138
const _CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = 1139
const _CS_POSIX_V7_LP64_OFF64_CFLAGS = 1140
const _CS_POSIX_V7_LP64_OFF64_LDFLAGS = 1141
const _CS_POSIX_V7_LP64_OFF64_LIBS = 1142
const _CS_POSIX_V7_LP64_OFF64_LINTFLAGS = 1143
const _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = 1144
const _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = 1145
const _CS_POSIX_V7_LPBIG_OFFBIG_LIBS = 1146
const _CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = 1147
const _CS_V6_ENV = 1148
const _CS_V7_ENV = 1149

type Tfloat_t = float32

type Tdouble_t = float64

const FP_NAN = 0
const FP_INFINITE = 1
const FP_ZERO = 2
const FP_SUBNORMAL = 3
const FP_NORMAL = 4

type Tsig_atomic_t = int32

type Tsigval = struct {
	Fsival_ptr   [0]uintptr
	Fsival_int   int32
	F__ccgo_pad2 [4]byte
}

type T__sigval_t = struct {
	Fsival_ptr   [0]uintptr
	Fsival_int   int32
	F__ccgo_pad2 [4]byte
}

type Tsiginfo_t = struct {
	Fsi_signo  int32
	Fsi_errno  int32
	Fsi_code   int32
	F__pad0    int32
	F_sifields struct {
		F_kill [0]struct {
			Fsi_pid T__pid_t
			Fsi_uid T__uid_t
		}
		F_timer [0]struct {
			Fsi_tid     int32
			Fsi_overrun int32
			Fsi_sigval  T__sigval_t
		}
		F_rt [0]struct {
			Fsi_pid    T__pid_t
			Fsi_uid    T__uid_t
			Fsi_sigval T__sigval_t
		}
		F_sigchld [0]struct {
			Fsi_pid    T__pid_t
			Fsi_uid    T__uid_t
			Fsi_status int32
			Fsi_utime  T__clock_t
			Fsi_stime  T__clock_t
		}
		F_sigfault [0]struct {
			Fsi_addr     uintptr
			Fsi_addr_lsb int16
			F_bounds     struct {
				F_pkey     [0]T__uint32_t
				F_addr_bnd struct {
					F_lower uintptr
					F_upper uintptr
				}
			}
		}
		F_sigpoll [0]struct {
			Fsi_band int64
			Fsi_fd   int32
		}
		F_sigsys [0]struct {
			F_call_addr uintptr
			F_syscall   int32
			F_arch      uint32
		}
		F_pad [28]int32
	}
}

const SI_ASYNCNL = -60
const SI_DETHREAD = -7
const SI_TKILL = -6
const SI_SIGIO = -5
const SI_ASYNCIO = -4
const SI_MESGQ = -3
const SI_TIMER = -2
const SI_QUEUE = -1
const SI_USER = 0
const SI_KERNEL = 128
const ILL_ILLOPC = 1
const ILL_ILLOPN = 2
const ILL_ILLADR = 3
const ILL_ILLTRP = 4
const ILL_PRVOPC = 5
const ILL_PRVREG = 6
const ILL_COPROC = 7
const ILL_BADSTK = 8
const ILL_BADIADDR = 9
const FPE_INTDIV = 1
const FPE_INTOVF = 2
const FPE_FLTDIV = 3
const FPE_FLTOVF = 4
const FPE_FLTUND = 5
const FPE_FLTRES = 6
const FPE_FLTINV = 7
const FPE_FLTSUB = 8
const FPE_FLTUNK = 14
const FPE_CONDTRAP = 15
const SEGV_MAPERR = 1
const SEGV_ACCERR = 2
const SEGV_BNDERR = 3
const SEGV_PKUERR = 4
const SEGV_ACCADI = 5
const SEGV_ADIDERR = 6
const SEGV_ADIPERR = 7
const SEGV_MTEAERR = 8
const SEGV_MTESERR = 9
const BUS_ADRALN = 1
const BUS_ADRERR = 2
const BUS_OBJERR = 3
const BUS_MCEERR_AR = 4
const BUS_MCEERR_AO = 5
const CLD_EXITED = 1
const CLD_KILLED = 2
const CLD_DUMPED = 3
const CLD_TRAPPED = 4
const CLD_STOPPED = 5
const CLD_CONTINUED = 6
const POLL_IN = 1
const POLL_OUT = 2
const POLL_MSG = 3
const POLL_ERR = 4
const POLL_PRI = 5
const POLL_HUP = 6

type Tsigval_t = struct {
	Fsival_ptr   [0]uintptr
	Fsival_int   int32
	F__ccgo_pad2 [4]byte
}

type Tsigevent_t = struct {
	Fsigev_value  T__sigval_t
	Fsigev_signo  int32
	Fsigev_notify int32
	F_sigev_un    struct {
		F_tid          [0]T__pid_t
		F_sigev_thread [0]struct {
			F_function  uintptr
			F_attribute uintptr
		}
		F_pad [12]int32
	}
}

type Tsigevent = Tsigevent_t

const SIGEV_SIGNAL = 0
const SIGEV_NONE = 1
const SIGEV_THREAD = 2
const SIGEV_THREAD_ID = 4

type T__sighandler_t = uintptr

type Tsig_t = uintptr

type Tsigaction = struct {
	F__sigaction_handler struct {
		Fsa_sigaction [0]uintptr
		Fsa_handler   T__sighandler_t
	}
	Fsa_mask     T__sigset_t
	Fsa_flags    int32
	Fsa_restorer uintptr
}

type T__s8 = int8

type T__u8 = uint8

type T__s16 = int16

type T__u16 = uint16

type T__s32 = int32

type T__u32 = uint32

type T__s64 = int64

type T__u64 = uint64

type T__kernel_fd_set = struct {
	Ffds_bits [16]uint64
}

type T__kernel_sighandler_t = uintptr

type T__kernel_key_t = int32

type T__kernel_mqd_t = int32

type T__kernel_long_t = int64

type T__kernel_ulong_t = uint64

type T__kernel_ino_t = uint64

type T__kernel_mode_t = uint32

type T__kernel_pid_t = int32

type T__kernel_ipc_pid_t = int32

type T__kernel_uid_t = uint32

type T__kernel_gid_t = uint32

type T__kernel_suseconds_t = int64

type T__kernel_daddr_t = int32

type T__kernel_uid32_t = uint32

type T__kernel_gid32_t = uint32

type T__kernel_old_uid_t = uint32

type T__kernel_old_gid_t = uint32

type T__kernel_old_dev_t = uint32

type T__kernel_size_t = uint64

type T__kernel_ssize_t = int64

type T__kernel_ptrdiff_t = int64

type T__kernel_fsid_t = struct {
	Fval [2]int32
}

type T__kernel_off_t = int64

type T__kernel_loff_t = int64

type T__kernel_old_time_t = int64

type T__kernel_time_t = int64

type T__kernel_time64_t = int64

type T__kernel_clock_t = int64

type T__kernel_timer_t = int32

type T__kernel_clockid_t = int32

type T__kernel_caddr_t = uintptr

type T__kernel_uid16_t = uint16

type T__kernel_gid16_t = uint16

type T__le16 = uint16

type T__be16 = uint16

type T__le32 = uint32

type T__be32 = uint32

type T__le64 = uint64

type T__be64 = uint64

type T__sum16 = uint16

type T__wsum = uint32

type T__poll_t = uint32

type Tsigcontext = struct {
	Fsc_pc    T__u64
	Fsc_regs  [32]T__u64
	Fsc_flags T__u32
}

type Tsctx_info = struct {
	Fmagic   T__u32
	Fsize    T__u32
	Fpadding T__u64
}

type Tfpu_context = struct {
	Fregs [32]T__u64
	Ffcc  T__u64
	Ffcsr T__u32
}

type Tlsx_context = struct {
	Fregs [64]T__u64
	Ffcc  T__u64
	Ffcsr T__u32
}

type Tlasx_context = struct {
	Fregs [128]T__u64
	Ffcc  T__u64
	Ffcsr T__u32
}

type Tstack_t = struct {
	Fss_sp    uintptr
	Fss_flags int32
	Fss_size  Tsize_t
}

type Tgreg_t = uint64

type Tgregset_t = [32]Tgreg_t

type Tmcontext_t = struct {
	F__pc    uint64
	F__gregs [32]uint64
	F__flags uint32
}

type Tucontext_t = struct {
	F__uc_flags  uint64
	Fuc_link     uintptr
	Fuc_stack    Tstack_t
	Fuc_sigmask  Tsigset_t
	Fuc_mcontext Tmcontext_t
}

const SS_ONSTACK = 1
const SS_DISABLE = 2

type Tsigstack = struct {
	Fss_sp      uintptr
	Fss_onstack int32
}

type TSRC_DATA = struct {
	Fdata_in           uintptr
	Fdata_out          uintptr
	Finput_frames      int64
	Foutput_frames     int64
	Finput_frames_used int64
	Foutput_frames_gen int64
	Fend_of_input      int32
	Fsrc_ratio         float64
}

type Tsrc_callback_t = uintptr

const SRC_SINC_BEST_QUALITY = 0
const SRC_SINC_MEDIUM_QUALITY = 1
const SRC_SINC_FASTEST = 2
const SRC_ZERO_ORDER_HOLD = 3
const SRC_LINEAR = 4

type TSRC_PAIR = struct {
	Fratio float64
	Fcount int32
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	/* Set up SIGALRM handler. */
	libc.Xsignal(tls, int32(m_SIGALRM), __ccgo_fp(_alarm_handler))
	libc.Xputs(tls, __ccgo_ts)
	_callback_hang_test(tls, SRC_ZERO_ORDER_HOLD)
	_callback_hang_test(tls, SRC_LINEAR)
	_callback_hang_test(tls, SRC_SINC_FASTEST)
	libc.Xputs(tls, __ccgo_ts)
	return 0
}

/* main */

func _callback_hang_test(tls *libc.TLS, converter int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var k int32
	var src_ratio float64
	var src_state uintptr
	var _ /* error at bp+0 */ int32
	_, _, _ = k, src_ratio, src_state
	src_ratio = float64(1)
	libc.Xprintf(tls, __ccgo_ts+1, libc.VaList(bp+16, libsamplerate.Xsrc_get_name(tls, converter)))
	libc.Xfflush(tls, libc.Xstdout)
	/* Perform sample rate conversion. */
	src_state = libsamplerate.Xsrc_callback_new(tls, __ccgo_fp(_input_callback), converter, int32(1), bp, libc.UintptrFromInt32(0))
	if src_state == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+39, libc.VaList(bp+16, int32(81), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp)))))
		libc.Xexit(tls, int32(1))
	}
	k = 0
	for {
		if !(k < int32(libc.Uint64FromInt64(144)/libc.Uint64FromInt64(16))) {
			break
		}
		libc.Xalarm(tls, uint32(1))
		src_ratio = _pairs[k].Fratio
		libsamplerate.Xsrc_callback_read(tls, src_state, src_ratio, int64(_pairs[k].Fcount), uintptr(unsafe.Pointer(&_output)))
		goto _1
	_1:
		k++
	}
	src_state = libsamplerate.Xsrc_delete(tls, src_state)
	libc.Xalarm(tls, uint32(0))
	libc.Xputs(tls, __ccgo_ts+85)
	return
}

var _output [16384]float32

var _pairs = [9]TSRC_PAIR{
	0: {
		Fratio: float64(1.2),
		Fcount: int32(5),
	},
	1: {
		Fratio: float64(1.1),
		Fcount: int32(1),
	},
	2: {
		Fratio: float64(1),
		Fcount: int32(1),
	},
	3: {
		Fratio: float64(3),
		Fcount: int32(1),
	},
	4: {
		Fratio: float64(2),
		Fcount: int32(1),
	},
	5: {
		Fratio: float64(0.3),
		Fcount: int32(1),
	},
	6: {
		Fratio: float64(1.2),
	},
	7: {
		Fratio: float64(1.1),
		Fcount: int32(10),
	},
	8: {
		Fratio: float64(1),
		Fcount: int32(1),
	},
}

/* callback_hang_test */

func _alarm_handler(tls *libc.TLS, number int32) {
	_ = number
	libc.Xprintf(tls, __ccgo_ts+88, 0)
	libc.Xexit(tls, int32(1))
}

/* alarm_handler */

func _input_callback(tls *libc.TLS, cb_data uintptr, data uintptr) (r int64) {
	_ = cb_data
	*(*uintptr)(unsafe.Pointer(data)) = uintptr(unsafe.Pointer(&_buffer))
	return int64(int32(libc.Uint64FromInt64(80) / libc.Uint64FromInt64(4)))
}

var _buffer [20]float32

/* input_callback */

func main() {
	libc.Start(x_main)
}

const m_M_PI1 = 3.141592653589793
const m__BITS_TYPES_LOCALE_T_H = 1
const m__BITS_TYPES___LOCALE_T_H = 1
const m__CTYPE_H = 1
const m__STRINGS_H = 1
const m__STRING_H = 1

type T__locale_struct = struct {
	F__locales       [13]uintptr
	F__ctype_b       uintptr
	F__ctype_tolower uintptr
	F__ctype_toupper uintptr
	F__names         [13]uintptr
}

type T__locale_t = uintptr

type Tlocale_t = uintptr

const _ISupper = 256
const _ISlower = 512
const _ISalpha = 1024
const _ISdigit = 2048
const _ISxdigit = 4096
const _ISspace = 8192
const _ISprint = 16384
const _ISgraph = 32768
const _ISblank = 1
const _IScntrl = 2
const _ISpunct = 4
const _ISalnum = 8

func x_gen_windowed_sines(tls *libc.TLS, freq_count int32, freqs uintptr, max float64, output uintptr, output_len int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var amplitude, phase float64
	var freq, k int32
	_, _, _, _ = amplitude, freq, k, phase
	amplitude = max / float64(freq_count)
	k = 0
	for {
		if !(k < output_len) {
			break
		}
		*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(0)
		goto _1
	_1:
		k++
	}
	freq = 0
	for {
		if !(freq < freq_count) {
			break
		}
		phase = libc.Float64FromFloat64(0.9) * libc.Float64FromFloat64(3.141592653589793) / float64(freq_count)
		if *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8)) <= float64(0) || *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8)) >= float64(0.5) {
			libc.Xprintf(tls, __ccgo_ts+155, libc.VaList(bp+8, __ccgo_ts+220, freq, *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8))))
			libc.Xexit(tls, int32(1))
		}
		k = 0
		for {
			if !(k < output_len) {
				break
			}
			*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4))) + amplitude*libc.Xsin(tls, *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8))*float64(libc.Int32FromInt32(2)*k)*float64(3.141592653589793)+phase))
			goto _3
		_3:
			k++
		}
		goto _2
	_2:
		freq++
	}
	/* Apply Hanning Window. */
	k = 0
	for {
		if !(k < output_len) {
			break
		}
		*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4))) * (libc.Float64FromFloat64(0.5) - libc.Float64FromFloat64(0.5)*libc.Xcos(tls, float64(libc.Int32FromInt32(2)*k)*float64(3.141592653589793)/float64(output_len-libc.Int32FromInt32(1)))))
		goto _4
	_4:
		k++
	}
	/*	data [k] *= 0.3635819 - 0.4891775 * cos ((2 * k) * M_PI / (output_len - 1))
		+ 0.1365995 * cos ((4 * k) * M_PI / (output_len - 1))
		- 0.0106411 * cos ((6 * k) * M_PI / (output_len - 1)) ;
	*/
	return
}

/* gen_windowed_sines */

func x_save_oct_float(tls *libc.TLS, filename uintptr, input uintptr, in_len int32, output uintptr, out_len int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var file, v1 uintptr
	var k int32
	_, _, _ = file, k, v1
	libc.Xprintf(tls, __ccgo_ts+233, libc.VaList(bp+8, filename))
	v1 = libc.Xfopen(tls, filename, __ccgo_ts+279)
	file = v1
	if !(v1 != 0) {
		return
	}
	libc.Xfprintf(tls, file, __ccgo_ts+281, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+306, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+321, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+337, libc.VaList(bp+8, in_len))
	libc.Xfprintf(tls, file, __ccgo_ts+349, 0)
	k = 0
	for {
		if !(k < in_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+363, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(input + uintptr(k)*4)))))
		goto _2
	_2:
		k++
	}
	libc.Xfprintf(tls, file, __ccgo_ts+368, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+321, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+337, libc.VaList(bp+8, out_len))
	libc.Xfprintf(tls, file, __ccgo_ts+349, 0)
	k = 0
	for {
		if !(k < out_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+363, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4)))))
		goto _3
	_3:
		k++
	}
	libc.Xfclose(tls, file)
	return
}

/* save_oct_float */

func x_save_oct_double(tls *libc.TLS, filename uintptr, input uintptr, in_len int32, output uintptr, out_len int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var file, v1 uintptr
	var k int32
	_, _, _ = file, k, v1
	libc.Xprintf(tls, __ccgo_ts+233, libc.VaList(bp+8, filename))
	v1 = libc.Xfopen(tls, filename, __ccgo_ts+279)
	file = v1
	if !(v1 != 0) {
		return
	}
	libc.Xfprintf(tls, file, __ccgo_ts+281, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+306, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+321, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+337, libc.VaList(bp+8, in_len))
	libc.Xfprintf(tls, file, __ccgo_ts+349, 0)
	k = 0
	for {
		if !(k < in_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+363, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(input + uintptr(k)*8))))
		goto _2
	_2:
		k++
	}
	libc.Xfprintf(tls, file, __ccgo_ts+368, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+321, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+337, libc.VaList(bp+8, out_len))
	libc.Xfprintf(tls, file, __ccgo_ts+349, 0)
	k = 0
	for {
		if !(k < out_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+363, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(output + uintptr(k)*8))))
		goto _3
	_3:
		k++
	}
	libc.Xfclose(tls, file)
	return
}

/* save_oct_double */

func x_interleave_data(tls *libc.TLS, in uintptr, out uintptr, frames int32, channels int32) {
	var ch, fr int32
	_, _ = ch, fr
	fr = 0
	for {
		if !(fr < frames) {
			break
		}
		ch = 0
		for {
			if !(ch < channels) {
				break
			}
			*(*float32)(unsafe.Pointer(out + uintptr(ch+channels*fr)*4)) = *(*float32)(unsafe.Pointer(in + uintptr(fr+frames*ch)*4))
			goto _2
		_2:
			ch++
		}
		goto _1
	_1:
		fr++
	}
	return
}

/* interleave_data */

func x_deinterleave_data(tls *libc.TLS, in uintptr, out uintptr, frames int32, channels int32) {
	var ch, fr int32
	_, _ = ch, fr
	ch = 0
	for {
		if !(ch < channels) {
			break
		}
		fr = 0
		for {
			if !(fr < frames) {
				break
			}
			*(*float32)(unsafe.Pointer(out + uintptr(fr+frames*ch)*4)) = *(*float32)(unsafe.Pointer(in + uintptr(ch+channels*fr)*4))
			goto _2
		_2:
			fr++
		}
		goto _1
	_1:
		ch++
	}
	return
}

/* deinterleave_data */

func x_reverse_data(tls *libc.TLS, data uintptr, datalen int32) {
	var left, right int32
	var temp float32
	_, _, _ = left, right, temp
	left = 0
	right = datalen - int32(1)
	for left < right {
		temp = *(*float32)(unsafe.Pointer(data + uintptr(left)*4))
		*(*float32)(unsafe.Pointer(data + uintptr(left)*4)) = *(*float32)(unsafe.Pointer(data + uintptr(right)*4))
		*(*float32)(unsafe.Pointer(data + uintptr(right)*4)) = temp
		left++
		right--
	}
}

/* reverse_data */

func x_get_cpu_name(tls *libc.TLS) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var dest, file, name, search, src, v1 uintptr
	var is_pipe int32
	_, _, _, _, _, _, _ = dest, file, is_pipe, name, search, src, v1
	name = __ccgo_ts + 384
	search = libc.UintptrFromInt32(0)
	file = libc.UintptrFromInt32(0)
	is_pipe = 0
	file = libc.Xfopen(tls, __ccgo_ts+392, __ccgo_ts+406)
	search = __ccgo_ts + 408
	if search == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+419, libc.VaList(bp+8, uintptr(unsafe.Pointer(&___func__))))
		return name
	}
	for libc.Xfgets(tls, uintptr(unsafe.Pointer(&_buffer1)), int32(512), file) != libc.UintptrFromInt32(0) {
		if libc.Xstrstr(tls, uintptr(unsafe.Pointer(&_buffer1)), search) != 0 {
			v1 = libc.Xstrchr(tls, uintptr(unsafe.Pointer(&_buffer1)), int32(':'))
			src = v1
			if v1 != libc.UintptrFromInt32(0) {
				src++
				for int32(*(*uint16)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(libc.X__ctype_b_loc(tls))) + uintptr(int32(*(*int8)(unsafe.Pointer(src))))*2)))&int32(uint16(_ISspace)) != 0 {
					src++
				}
				name = src
				/* Remove consecutive spaces. */
				src++
				dest = src
				for {
					if !(*(*int8)(unsafe.Pointer(src)) != 0) {
						break
					}
					if int32(*(*uint16)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(libc.X__ctype_b_loc(tls))) + uintptr(int32(*(*int8)(unsafe.Pointer(src))))*2)))&int32(uint16(_ISspace)) != 0 && int32(*(*uint16)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(libc.X__ctype_b_loc(tls))) + uintptr(int32(*(*int8)(unsafe.Pointer(dest + uintptr(-libc.Int32FromInt32(1))))))*2)))&int32(uint16(_ISspace)) != 0 {
						goto _2
					}
					*(*int8)(unsafe.Pointer(dest)) = *(*int8)(unsafe.Pointer(src))
					dest++
					goto _2
				_2:
					src++
				}
				*(*int8)(unsafe.Pointer(dest)) = 0
				break
			}
		}
	}
	if is_pipe != 0 {
		libc.Xpclose(tls, file)
	} else {
		libc.Xfclose(tls, file)
	}
	return name
}

var ___func__ = [13]int8{'g', 'e', 't', '_', 'c', 'p', 'u', '_', 'n', 'a', 'm', 'e'}

var _buffer1 [512]int8

func __ccgo_fp(f interface{}) uintptr {
	type iface [2]uintptr
	return (*iface)(unsafe.Pointer(&f))[1]
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "\x00\tcallback_hang_test  (%-28s) ....... \x00\n\nLine %d : src_callback_new () failed : %s\n\n\x00ok\x00\n\n    Error : Hang inside src_callback_read() detected. Exiting!\n\n\x00\n%s : Error : freq [%d] == %g is out of range. Should be < 0.5.\n\x00tests/util.c\x00Dumping input and output data to file : %s.\n\n\x00w\x00# Not created by Octave\n\x00# name: input\n\x00# type: matrix\n\x00# rows: %d\n\x00# columns: 1\n\x00% g\n\x00# name: output\n\x00Unknown\x00/proc/cpuinfo\x00r\x00model name\x00Error : search is NULL in function %s.\n\x00"
