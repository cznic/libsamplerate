// Code generated for darwin/amd64 by 'gcc --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -w -mlong-double-64 -o tests/.libs/multichan_throughput_test.go tests/multichan_throughput_test-multichan_throughput_test.o.go tests/multichan_throughput_test-util.o.go tests/multichan_throughput_test-calc_snr.o.go -lsamplerate', DO NOT EDIT.

//go:build darwin && amd64
// +build darwin,amd64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libsamplerate"
)

var (
	_ reflect.Type
	_ unsafe.Pointer
)

const m_ACCESSX_MAX_DESCRIPTORS = 100
const m_BADSIG = "SIG_ERR"
const m_BIG_ENDIAN = "__DARWIN_BIG_ENDIAN"
const m_BUFSIZ = 1024
const m_BUS_ADRALN = 1
const m_BUS_ADRERR = 2
const m_BUS_NOOP = 0
const m_BUS_OBJERR = 3
const m_BYTE_ORDER = "__DARWIN_BYTE_ORDER"
const m_CLD_CONTINUED = 6
const m_CLD_DUMPED = 3
const m_CLD_EXITED = 1
const m_CLD_KILLED = 2
const m_CLD_NOOP = 0
const m_CLD_STOPPED = 5
const m_CLD_TRAPPED = 4
const m_CLOCK_MONOTONIC = "_CLOCK_MONOTONIC"
const m_CLOCK_MONOTONIC_RAW = "_CLOCK_MONOTONIC_RAW"
const m_CLOCK_MONOTONIC_RAW_APPROX = "_CLOCK_MONOTONIC_RAW_APPROX"
const m_CLOCK_PROCESS_CPUTIME_ID = "_CLOCK_PROCESS_CPUTIME_ID"
const m_CLOCK_REALTIME = "_CLOCK_REALTIME"
const m_CLOCK_THREAD_CPUTIME_ID = "_CLOCK_THREAD_CPUTIME_ID"
const m_CLOCK_UPTIME_RAW = "_CLOCK_UPTIME_RAW"
const m_CLOCK_UPTIME_RAW_APPROX = "_CLOCK_UPTIME_RAW_APPROX"
const m_CPUMON_MAKE_FATAL = 0x1000
const m_CPU_CLIPS_NEGATIVE = 0
const m_CPU_CLIPS_POSITIVE = 0
const m_CPU_IS_BIG_ENDIAN = 0
const m_CPU_IS_LITTLE_ENDIAN = 1
const m_DOMAIN = 1
const m_ENABLE_SINC_BEST_CONVERTER = "yes"
const m_ENABLE_SINC_FAST_CONVERTER = "yes"
const m_ENABLE_SINC_MEDIUM_CONVERTER = "yes"
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_FD_SETSIZE = "__DARWIN_FD_SETSIZE"
const m_FILENAME_MAX = 1024
const m_FOOTPRINT_INTERVAL_RESET = 0x1
const m_FOPEN_MAX = 20
const m_FPE_FLTDIV = 1
const m_FPE_FLTINV = 5
const m_FPE_FLTOVF = 2
const m_FPE_FLTRES = 4
const m_FPE_FLTSUB = 6
const m_FPE_FLTUND = 3
const m_FPE_INTDIV = 7
const m_FPE_INTOVF = 8
const m_FPE_NOOP = 0
const m_FP_CHOP = 3
const m_FP_INFINITE = 2
const m_FP_NAN = 1
const m_FP_NORMAL = 4
const m_FP_PREC_24B = 0
const m_FP_PREC_53B = 2
const m_FP_PREC_64B = 3
const m_FP_QNAN = "FP_NAN"
const m_FP_RND_DOWN = 1
const m_FP_RND_NEAR = 0
const m_FP_RND_UP = 2
const m_FP_SNAN = "FP_NAN"
const m_FP_STATE_BYTES = 512
const m_FP_SUBNORMAL = 5
const m_FP_SUPERNORMAL = 6
const m_FP_ZERO = 3
const m_F_LOCK = 1
const m_F_OK = 0
const m_F_TEST = 3
const m_F_TLOCK = 2
const m_F_ULOCK = 0
const m_HAVE_ALARM = 1
const m_HAVE_CALLOC = 1
const m_HAVE_CEIL = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_FLOOR = 1
const m_HAVE_FMOD = 1
const m_HAVE_FREE = 1
const m_HAVE_IMMINTRIN_H = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_LRINT = 1
const m_HAVE_LRINTF = 1
const m_HAVE_MALLOC = 1
const m_HAVE_MEMCPY = 1
const m_HAVE_MEMMOVE = 1
const m_HAVE_SIGALRM = 1
const m_HAVE_SIGNAL = 1
const m_HAVE_STDBOOL_H = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TIMES_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_VISIBILITY = 1
const m_HUGE = "MAXFLOAT"
const m_ILL_BADSTK = 8
const m_ILL_COPROC = 7
const m_ILL_ILLADR = 5
const m_ILL_ILLOPC = 1
const m_ILL_ILLOPN = 4
const m_ILL_ILLTRP = 2
const m_ILL_NOOP = 0
const m_ILL_PRVOPC = 3
const m_ILL_PRVREG = 6
const m_INFINITY = "HUGE_VALF"
const m_INTMAX_MAX = "__INTMAX_MAX__"
const m_INTPTR_MAX = "__INTPTR_MAX__"
const m_INT_FAST16_MAX = "__INT_LEAST16_MAX"
const m_INT_FAST16_MIN = "__INT_LEAST16_MIN"
const m_INT_FAST32_MAX = "__INT_LEAST32_MAX"
const m_INT_FAST32_MIN = "__INT_LEAST32_MIN"
const m_INT_FAST64_MAX = "__INT_LEAST64_MAX"
const m_INT_FAST64_MIN = "__INT_LEAST64_MIN"
const m_INT_FAST8_MAX = "__INT_LEAST8_MAX"
const m_INT_FAST8_MIN = "__INT_LEAST8_MIN"
const m_INT_LEAST16_MAX = "__INT_LEAST16_MAX"
const m_INT_LEAST16_MIN = "__INT_LEAST16_MIN"
const m_INT_LEAST32_MAX = "__INT_LEAST32_MAX"
const m_INT_LEAST32_MIN = "__INT_LEAST32_MIN"
const m_INT_LEAST64_MAX = "__INT_LEAST64_MAX"
const m_INT_LEAST64_MIN = "__INT_LEAST64_MIN"
const m_INT_LEAST8_MAX = "__INT_LEAST8_MAX"
const m_INT_LEAST8_MIN = "__INT_LEAST8_MIN"
const m_IOPOL_APPLICATION = "IOPOL_STANDARD"
const m_IOPOL_ATIME_UPDATES_DEFAULT = 0
const m_IOPOL_ATIME_UPDATES_OFF = 1
const m_IOPOL_DEFAULT = 0
const m_IOPOL_IMPORTANT = 1
const m_IOPOL_MATERIALIZE_DATALESS_FILES_DEFAULT = 0
const m_IOPOL_MATERIALIZE_DATALESS_FILES_OFF = 1
const m_IOPOL_MATERIALIZE_DATALESS_FILES_ON = 2
const m_IOPOL_NORMAL = "IOPOL_IMPORTANT"
const m_IOPOL_PASSIVE = 2
const m_IOPOL_SCOPE_DARWIN_BG = 2
const m_IOPOL_SCOPE_PROCESS = 0
const m_IOPOL_SCOPE_THREAD = 1
const m_IOPOL_STANDARD = 5
const m_IOPOL_THROTTLE = 3
const m_IOPOL_TYPE_DISK = 0
const m_IOPOL_TYPE_VFS_ALLOW_LOW_SPACE_WRITES = 9
const m_IOPOL_TYPE_VFS_ATIME_UPDATES = 2
const m_IOPOL_TYPE_VFS_DISALLOW_RW_FOR_O_EVTONLY = 10
const m_IOPOL_TYPE_VFS_IGNORE_CONTENT_PROTECTION = 6
const m_IOPOL_TYPE_VFS_IGNORE_PERMISSIONS = 7
const m_IOPOL_TYPE_VFS_MATERIALIZE_DATALESS_FILES = 3
const m_IOPOL_TYPE_VFS_SKIP_MTIME_UPDATE = 8
const m_IOPOL_TYPE_VFS_STATFS_NO_DATA_VOLUME = 4
const m_IOPOL_TYPE_VFS_TRIGGER_RESOLVE = 5
const m_IOPOL_UTILITY = 4
const m_IOPOL_VFS_ALLOW_LOW_SPACE_WRITES_OFF = 0
const m_IOPOL_VFS_ALLOW_LOW_SPACE_WRITES_ON = 1
const m_IOPOL_VFS_CONTENT_PROTECTION_DEFAULT = 0
const m_IOPOL_VFS_CONTENT_PROTECTION_IGNORE = 1
const m_IOPOL_VFS_DISALLOW_RW_FOR_O_EVTONLY_DEFAULT = 0
const m_IOPOL_VFS_DISALLOW_RW_FOR_O_EVTONLY_ON = 1
const m_IOPOL_VFS_IGNORE_PERMISSIONS_OFF = 0
const m_IOPOL_VFS_IGNORE_PERMISSIONS_ON = 1
const m_IOPOL_VFS_NOCACHE_WRITE_FS_BLKSIZE_DEFAULT = 0
const m_IOPOL_VFS_NOCACHE_WRITE_FS_BLKSIZE_ON = 1
const m_IOPOL_VFS_SKIP_MTIME_UPDATE_OFF = 0
const m_IOPOL_VFS_SKIP_MTIME_UPDATE_ON = 1
const m_IOPOL_VFS_STATFS_FORCE_NO_DATA_VOLUME = 1
const m_IOPOL_VFS_STATFS_NO_DATA_VOLUME_DEFAULT = 0
const m_IOPOL_VFS_TRIGGER_RESOLVE_DEFAULT = 0
const m_IOPOL_VFS_TRIGGER_RESOLVE_OFF = 1
const m_LITTLE_ENDIAN = "__DARWIN_LITTLE_ENDIAN"
const m_LT_OBJDIR = ".libs/"
const m_L_INCR = "SEEK_CUR"
const m_L_SET = "SEEK_SET"
const m_L_XTND = "SEEK_END"
const m_L_ctermid = 1024
const m_L_tmpnam = 1024
const m_MAC_OS_VERSION_11_0 = "__MAC_11_0"
const m_MAC_OS_VERSION_11_1 = "__MAC_11_1"
const m_MAC_OS_VERSION_11_3 = "__MAC_11_3"
const m_MAC_OS_VERSION_11_4 = "__MAC_11_4"
const m_MAC_OS_VERSION_11_5 = "__MAC_11_5"
const m_MAC_OS_VERSION_11_6 = "__MAC_11_6"
const m_MAC_OS_VERSION_12_0 = "__MAC_12_0"
const m_MAC_OS_VERSION_12_1 = "__MAC_12_1"
const m_MAC_OS_VERSION_12_2 = "__MAC_12_2"
const m_MAC_OS_VERSION_12_3 = "__MAC_12_3"
const m_MAC_OS_VERSION_12_4 = "__MAC_12_4"
const m_MAC_OS_VERSION_12_5 = "__MAC_12_5"
const m_MAC_OS_VERSION_12_6 = "__MAC_12_6"
const m_MAC_OS_VERSION_12_7 = "__MAC_12_7"
const m_MAC_OS_VERSION_13_0 = "__MAC_13_0"
const m_MAC_OS_VERSION_13_1 = "__MAC_13_1"
const m_MAC_OS_VERSION_13_2 = "__MAC_13_2"
const m_MAC_OS_VERSION_13_3 = "__MAC_13_3"
const m_MAC_OS_VERSION_13_4 = "__MAC_13_4"
const m_MAC_OS_VERSION_13_5 = "__MAC_13_5"
const m_MAC_OS_VERSION_13_6 = "__MAC_13_6"
const m_MAC_OS_VERSION_14_0 = "__MAC_14_0"
const m_MAC_OS_VERSION_14_1 = "__MAC_14_1"
const m_MAC_OS_VERSION_14_2 = "__MAC_14_2"
const m_MAC_OS_X_VERSION_10_0 = "__MAC_10_0"
const m_MAC_OS_X_VERSION_10_1 = "__MAC_10_1"
const m_MAC_OS_X_VERSION_10_10 = "__MAC_10_10"
const m_MAC_OS_X_VERSION_10_10_2 = "__MAC_10_10_2"
const m_MAC_OS_X_VERSION_10_10_3 = "__MAC_10_10_3"
const m_MAC_OS_X_VERSION_10_11 = "__MAC_10_11"
const m_MAC_OS_X_VERSION_10_11_2 = "__MAC_10_11_2"
const m_MAC_OS_X_VERSION_10_11_3 = "__MAC_10_11_3"
const m_MAC_OS_X_VERSION_10_11_4 = "__MAC_10_11_4"
const m_MAC_OS_X_VERSION_10_12 = "__MAC_10_12"
const m_MAC_OS_X_VERSION_10_12_1 = "__MAC_10_12_1"
const m_MAC_OS_X_VERSION_10_12_2 = "__MAC_10_12_2"
const m_MAC_OS_X_VERSION_10_12_4 = "__MAC_10_12_4"
const m_MAC_OS_X_VERSION_10_13 = "__MAC_10_13"
const m_MAC_OS_X_VERSION_10_13_1 = "__MAC_10_13_1"
const m_MAC_OS_X_VERSION_10_13_2 = "__MAC_10_13_2"
const m_MAC_OS_X_VERSION_10_13_4 = "__MAC_10_13_4"
const m_MAC_OS_X_VERSION_10_14 = "__MAC_10_14"
const m_MAC_OS_X_VERSION_10_14_1 = "__MAC_10_14_1"
const m_MAC_OS_X_VERSION_10_14_4 = "__MAC_10_14_4"
const m_MAC_OS_X_VERSION_10_14_5 = "__MAC_10_14_5"
const m_MAC_OS_X_VERSION_10_14_6 = "__MAC_10_14_6"
const m_MAC_OS_X_VERSION_10_15 = "__MAC_10_15"
const m_MAC_OS_X_VERSION_10_15_1 = "__MAC_10_15_1"
const m_MAC_OS_X_VERSION_10_15_4 = "__MAC_10_15_4"
const m_MAC_OS_X_VERSION_10_16 = "__MAC_10_16"
const m_MAC_OS_X_VERSION_10_2 = "__MAC_10_2"
const m_MAC_OS_X_VERSION_10_3 = "__MAC_10_3"
const m_MAC_OS_X_VERSION_10_4 = "__MAC_10_4"
const m_MAC_OS_X_VERSION_10_5 = "__MAC_10_5"
const m_MAC_OS_X_VERSION_10_6 = "__MAC_10_6"
const m_MAC_OS_X_VERSION_10_7 = "__MAC_10_7"
const m_MAC_OS_X_VERSION_10_8 = "__MAC_10_8"
const m_MAC_OS_X_VERSION_10_9 = "__MAC_10_9"
const m_MATH_ERREXCEPT = 2
const m_MATH_ERRNO = 1
const m_MAXFLOAT = "0x1.fffffep+127f"
const m_MB_CUR_MAX = "__mb_cur_max"
const m_MINSIGSTKSZ = 32768
const m_M_1_PI = 0.318309886183790671537767526745028724
const m_M_2_PI = 0.636619772367581343075535053490057448
const m_M_2_SQRTPI = 1.12837916709551257389615890312154517
const m_M_E = 2.71828182845904523536028747135266250
const m_M_LN10 = 2.30258509299404568401799145468436421
const m_M_LN2 = 0.693147180559945309417232121458176568
const m_M_LOG10E = 0.434294481903251827651128918916605082
const m_M_LOG2E = 1.44269504088896340735992468100189214
const m_M_PI = 3.14159265358979323846264338327950288
const m_M_PI_2 = 1.57079632679489661923132169163975144
const m_M_PI_4 = 0.785398163397448309615660845819875721
const m_M_SQRT1_2 = 0.707106781186547524400844362104849039
const m_M_SQRT2 = 1.41421356237309504880168872420969808
const m_NSIG = "__DARWIN_NSIG"
const m_NULL = "__DARWIN_NULL"
const m_OVERFLOW = 3
const m_PACKAGE = "libsamplerate"
const m_PACKAGE_BUGREPORT = "erikd@mega-nerd.com"
const m_PACKAGE_NAME = "libsamplerate"
const m_PACKAGE_STRING = "libsamplerate 0.2.2"
const m_PACKAGE_TARNAME = "libsamplerate"
const m_PACKAGE_URL = "https://github.com/libsndfile/libsamplerate/"
const m_PACKAGE_VERSION = "0.2.2"
const m_PDP_ENDIAN = "__DARWIN_PDP_ENDIAN"
const m_PLOSS = 6
const m_POLL_ERR = 4
const m_POLL_HUP = 6
const m_POLL_IN = 1
const m_POLL_MSG = 3
const m_POLL_OUT = 2
const m_POLL_PRI = 5
const m_PRIO_DARWIN_BG = 0x1000
const m_PRIO_DARWIN_NONUI = 0x1001
const m_PRIO_DARWIN_PROCESS = 4
const m_PRIO_DARWIN_THREAD = 3
const m_PRIO_MAX = 20
const m_PRIO_PGRP = 1
const m_PRIO_PROCESS = 0
const m_PRIO_USER = 2
const m_PTRDIFF_MAX = "__PTRDIFF_MAX__"
const m_P_tmpdir = "/var/tmp/"
const m_RAND_MAX = 0x7fffffff
const m_RENAME_EXCL = 0x00000004
const m_RENAME_NOFOLLOW_ANY = 0x00000010
const m_RENAME_RESERVED1 = 0x00000008
const m_RENAME_SECLUDE = 0x00000001
const m_RENAME_SWAP = 0x00000002
const m_RLIMIT_AS = 5
const m_RLIMIT_CORE = 4
const m_RLIMIT_CPU = 0
const m_RLIMIT_CPU_USAGE_MONITOR = 0x2
const m_RLIMIT_DATA = 2
const m_RLIMIT_FOOTPRINT_INTERVAL = 0x4
const m_RLIMIT_FSIZE = 1
const m_RLIMIT_MEMLOCK = 6
const m_RLIMIT_NOFILE = 8
const m_RLIMIT_NPROC = 7
const m_RLIMIT_RSS = "RLIMIT_AS"
const m_RLIMIT_STACK = 3
const m_RLIMIT_THREAD_CPULIMITS = 0x3
const m_RLIMIT_WAKEUPS_MONITOR = 0x1
const m_RLIM_NLIMITS = 9
const m_RLIM_SAVED_CUR = "RLIM_INFINITY"
const m_RLIM_SAVED_MAX = "RLIM_INFINITY"
const m_RUSAGE_INFO_CURRENT = "RUSAGE_INFO_V6"
const m_RUSAGE_INFO_V0 = 0
const m_RUSAGE_INFO_V1 = 1
const m_RUSAGE_INFO_V2 = 2
const m_RUSAGE_INFO_V3 = 3
const m_RUSAGE_INFO_V4 = 4
const m_RUSAGE_INFO_V5 = 5
const m_RUSAGE_INFO_V6 = 6
const m_RUSAGE_SELF = 0
const m_RU_PROC_RUNS_RESLIDE = 0x00000001
const m_SA_64REGSET = 0x0200
const m_SA_NOCLDSTOP = 0x0008
const m_SA_NOCLDWAIT = 0x0020
const m_SA_NODEFER = 0x0010
const m_SA_ONSTACK = 0x0001
const m_SA_RESETHAND = 0x0004
const m_SA_RESTART = 0x0002
const m_SA_SIGINFO = 0x0040
const m_SA_USERTRAMP = 0x0100
const m_SEEK_CUR = 1
const m_SEEK_DATA = 4
const m_SEEK_END = 2
const m_SEEK_HOLE = 3
const m_SEEK_SET = 0
const m_SEGV_ACCERR = 2
const m_SEGV_MAPERR = 1
const m_SEGV_NOOP = 0
const m_SIGABRT = 6
const m_SIGALRM = 14
const m_SIGBUS = 10
const m_SIGCHLD = 20
const m_SIGCONT = 19
const m_SIGEMT = 7
const m_SIGEV_NONE = 0
const m_SIGEV_SIGNAL = 1
const m_SIGEV_THREAD = 3
const m_SIGFPE = 8
const m_SIGHUP = 1
const m_SIGILL = 4
const m_SIGINFO = 29
const m_SIGINT = 2
const m_SIGIO = 23
const m_SIGIOT = "SIGABRT"
const m_SIGKILL = 9
const m_SIGPIPE = 13
const m_SIGPROF = 27
const m_SIGQUIT = 3
const m_SIGSEGV = 11
const m_SIGSTKSZ = 131072
const m_SIGSTOP = 17
const m_SIGSYS = 12
const m_SIGTERM = 15
const m_SIGTRAP = 5
const m_SIGTSTP = 18
const m_SIGTTIN = 21
const m_SIGTTOU = 22
const m_SIGURG = 16
const m_SIGUSR1 = 30
const m_SIGUSR2 = 31
const m_SIGVTALRM = 26
const m_SIGWINCH = 28
const m_SIGXCPU = 24
const m_SIGXFSZ = 25
const m_SIG_BLOCK = 1
const m_SIG_SETMASK = 3
const m_SIG_UNBLOCK = 2
const m_SING = 2
const m_SIZEOF_DOUBLE = 8
const m_SIZEOF_FLOAT = 4
const m_SIZEOF_INT = 4
const m_SIZEOF_LONG = 8
const m_SIZE_MAX = "__SIZE_MAX__"
const m_SI_ASYNCIO = 0x10004
const m_SI_MESGQ = 0x10005
const m_SI_QUEUE = 0x10002
const m_SI_TIMER = 0x10003
const m_SI_USER = 0x10001
const m_SS_DISABLE = 0x0004
const m_SS_ONSTACK = 0x0001
const m_STDC_HEADERS = 1
const m_STDERR_FILENO = 2
const m_STDIN_FILENO = 0
const m_STDOUT_FILENO = 1
const m_SV_INTERRUPT = "SA_RESTART"
const m_SV_NOCLDSTOP = "SA_NOCLDSTOP"
const m_SV_NODEFER = "SA_NODEFER"
const m_SV_ONSTACK = "SA_ONSTACK"
const m_SV_RESETHAND = "SA_RESETHAND"
const m_SV_SIGINFO = "SA_SIGINFO"
const m_SYNC_VOLUME_FULLSYNC = 0x01
const m_SYNC_VOLUME_WAIT = 0x02
const m_TIME_UTC = 1
const m_TLOSS = 5
const m_TMP_MAX = 308915776
const m_TRAP_BRKPT = 1
const m_TRAP_TRACE = 2
const m_UINTMAX_MAX = "__UINTMAX_MAX__"
const m_UINTPTR_MAX = "__UINTPTR_MAX__"
const m_UINT_FAST16_MAX = "__UINT_LEAST16_MAX"
const m_UINT_FAST32_MAX = "__UINT_LEAST32_MAX"
const m_UINT_FAST64_MAX = "__UINT_LEAST64_MAX"
const m_UINT_FAST8_MAX = "__UINT_LEAST8_MAX"
const m_UINT_LEAST16_MAX = "__UINT_LEAST16_MAX"
const m_UINT_LEAST32_MAX = "__UINT_LEAST32_MAX"
const m_UINT_LEAST64_MAX = "__UINT_LEAST64_MAX"
const m_UINT_LEAST8_MAX = "__UINT_LEAST8_MAX"
const m_UNDERFLOW = 4
const m_VERSION = "0.2.2"
const m_WAIT_MYPGRP = 0
const m_WAKEMON_DISABLE = 0x02
const m_WAKEMON_ENABLE = 0x01
const m_WAKEMON_GET_PARAMS = 0x04
const m_WAKEMON_MAKE_FATAL = 0x10
const m_WAKEMON_SET_DEFAULTS = 0x08
const m_WCHAR_MAX = "__WCHAR_MAX__"
const m_WCONTINUED = 0x00000010
const m_WCOREFLAG = 0200
const m_WEXITED = 0x00000004
const m_WNOHANG = 0x00000001
const m_WNOWAIT = 0x00000020
const m_WSTOPPED = 0x00000008
const m_WUNTRACED = 0x00000002
const m_X_TLOSS = 1.41484755040568800000e+16
const m__CS_DARWIN_USER_CACHE_DIR = 65538
const m__CS_DARWIN_USER_DIR = 65536
const m__CS_DARWIN_USER_TEMP_DIR = 65537
const m__CS_PATH = 1
const m__CS_POSIX_V6_ILP32_OFF32_CFLAGS = 2
const m__CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 3
const m__CS_POSIX_V6_ILP32_OFF32_LIBS = 4
const m__CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 5
const m__CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 6
const m__CS_POSIX_V6_ILP32_OFFBIG_LIBS = 7
const m__CS_POSIX_V6_LP64_OFF64_CFLAGS = 8
const m__CS_POSIX_V6_LP64_OFF64_LDFLAGS = 9
const m__CS_POSIX_V6_LP64_OFF64_LIBS = 10
const m__CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 11
const m__CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 12
const m__CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 13
const m__CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = 14
const m__CS_XBS5_ILP32_OFF32_CFLAGS = 20
const m__CS_XBS5_ILP32_OFF32_LDFLAGS = 21
const m__CS_XBS5_ILP32_OFF32_LIBS = 22
const m__CS_XBS5_ILP32_OFF32_LINTFLAGS = 23
const m__CS_XBS5_ILP32_OFFBIG_CFLAGS = 24
const m__CS_XBS5_ILP32_OFFBIG_LDFLAGS = 25
const m__CS_XBS5_ILP32_OFFBIG_LIBS = 26
const m__CS_XBS5_ILP32_OFFBIG_LINTFLAGS = 27
const m__CS_XBS5_LP64_OFF64_CFLAGS = 28
const m__CS_XBS5_LP64_OFF64_LDFLAGS = 29
const m__CS_XBS5_LP64_OFF64_LIBS = 30
const m__CS_XBS5_LP64_OFF64_LINTFLAGS = 31
const m__CS_XBS5_LPBIG_OFFBIG_CFLAGS = 32
const m__CS_XBS5_LPBIG_OFFBIG_LDFLAGS = 33
const m__CS_XBS5_LPBIG_OFFBIG_LIBS = 34
const m__CS_XBS5_LPBIG_OFFBIG_LINTFLAGS = 35
const m__DARWIN_FEATURE_64_BIT_INODE = 1
const m__DARWIN_FEATURE_ONLY_UNIX_CONFORMANCE = 1
const m__DARWIN_FEATURE_UNIX_CONFORMANCE = 3
const m__FORTIFY_SOURCE = 2
const m__I386_SIGNAL_H_ = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__LP64 = 1
const m__PC_2_SYMLINKS = 15
const m__PC_ALLOC_SIZE_MIN = 16
const m__PC_ASYNC_IO = 17
const m__PC_AUTH_OPAQUE_NP = 14
const m__PC_CASE_PRESERVING = 12
const m__PC_CASE_SENSITIVE = 11
const m__PC_CHOWN_RESTRICTED = 7
const m__PC_EXTENDED_SECURITY_NP = 13
const m__PC_FILESIZEBITS = 18
const m__PC_LINK_MAX = 1
const m__PC_MAX_CANON = 2
const m__PC_MAX_INPUT = 3
const m__PC_MIN_HOLE_SIZE = 27
const m__PC_NAME_CHARS_MAX = 10
const m__PC_NAME_MAX = 4
const m__PC_NO_TRUNC = 8
const m__PC_PATH_MAX = 5
const m__PC_PIPE_BUF = 6
const m__PC_PRIO_IO = 19
const m__PC_REC_INCR_XFER_SIZE = 20
const m__PC_REC_MAX_XFER_SIZE = 21
const m__PC_REC_MIN_XFER_SIZE = 22
const m__PC_REC_XFER_ALIGN = 23
const m__PC_SYMLINK_MAX = 24
const m__PC_SYNC_IO = 25
const m__PC_VDISABLE = 9
const m__PC_XATTR_SIZE_BITS = 26
const m__POSIX2_CHAR_TERM = 200112
const m__POSIX2_C_BIND = 200112
const m__POSIX2_C_DEV = 200112
const m__POSIX2_FORT_RUN = 200112
const m__POSIX2_LOCALEDEF = 200112
const m__POSIX2_SW_DEV = 200112
const m__POSIX2_UPE = 200112
const m__POSIX2_VERSION = 200112
const m__POSIX_CHOWN_RESTRICTED = 200112
const m__POSIX_FSYNC = 200112
const m__POSIX_IPV6 = 200112
const m__POSIX_JOB_CONTROL = 200112
const m__POSIX_MAPPED_FILES = 200112
const m__POSIX_MEMORY_PROTECTION = 200112
const m__POSIX_NO_TRUNC = 200112
const m__POSIX_READER_WRITER_LOCKS = 200112
const m__POSIX_REGEXP = 200112
const m__POSIX_SAVED_IDS = 200112
const m__POSIX_SHELL = 200112
const m__POSIX_SPAWN = 200112
const m__POSIX_THREADS = 200112
const m__POSIX_THREAD_ATTR_STACKADDR = 200112
const m__POSIX_THREAD_ATTR_STACKSIZE = 200112
const m__POSIX_THREAD_KEYS_MAX = 128
const m__POSIX_THREAD_PROCESS_SHARED = 200112
const m__POSIX_THREAD_SAFE_FUNCTIONS = 200112
const m__POSIX_V6_ILP32_OFF32 = "__ILP32_OFF32"
const m__POSIX_V6_ILP32_OFFBIG = "__ILP32_OFFBIG"
const m__POSIX_V6_LP64_OFF64 = "__LP64_OFF64"
const m__POSIX_V6_LPBIG_OFFBIG = "__LPBIG_OFFBIG"
const m__POSIX_V7_ILP32_OFF32 = "__ILP32_OFF32"
const m__POSIX_V7_ILP32_OFFBIG = "__ILP32_OFFBIG"
const m__POSIX_V7_LP64_OFF64 = "__LP64_OFF64"
const m__POSIX_V7_LPBIG_OFFBIG = "__LPBIG_OFFBIG"
const m__POSIX_VERSION = 200112
const m__QUAD_HIGHWORD = 1
const m__QUAD_LOWWORD = 0
const m__RLIMIT_POSIX_FLAG = 0x1000
const m__SC_2_CHAR_TERM = 20
const m__SC_2_C_BIND = 18
const m__SC_2_C_DEV = 19
const m__SC_2_FORT_DEV = 21
const m__SC_2_FORT_RUN = 22
const m__SC_2_LOCALEDEF = 23
const m__SC_2_PBS = 59
const m__SC_2_PBS_ACCOUNTING = 60
const m__SC_2_PBS_CHECKPOINT = 61
const m__SC_2_PBS_LOCATE = 62
const m__SC_2_PBS_MESSAGE = 63
const m__SC_2_PBS_TRACK = 64
const m__SC_2_SW_DEV = 24
const m__SC_2_UPE = 25
const m__SC_2_VERSION = 17
const m__SC_ADVISORY_INFO = 65
const m__SC_AIO_LISTIO_MAX = 42
const m__SC_AIO_MAX = 43
const m__SC_AIO_PRIO_DELTA_MAX = 44
const m__SC_ARG_MAX = 1
const m__SC_ASYNCHRONOUS_IO = 28
const m__SC_ATEXIT_MAX = 107
const m__SC_BARRIERS = 66
const m__SC_BC_BASE_MAX = 9
const m__SC_BC_DIM_MAX = 10
const m__SC_BC_SCALE_MAX = 11
const m__SC_BC_STRING_MAX = 12
const m__SC_CHILD_MAX = 2
const m__SC_CLK_TCK = 3
const m__SC_CLOCK_SELECTION = 67
const m__SC_COLL_WEIGHTS_MAX = 13
const m__SC_CPUTIME = 68
const m__SC_DELAYTIMER_MAX = 45
const m__SC_EXPR_NEST_MAX = 14
const m__SC_FILE_LOCKING = 69
const m__SC_FSYNC = 38
const m__SC_GETGR_R_SIZE_MAX = 70
const m__SC_GETPW_R_SIZE_MAX = 71
const m__SC_HOST_NAME_MAX = 72
const m__SC_IOV_MAX = 56
const m__SC_IPV6 = 118
const m__SC_JOB_CONTROL = 6
const m__SC_LINE_MAX = 15
const m__SC_LOGIN_NAME_MAX = 73
const m__SC_MAPPED_FILES = 47
const m__SC_MEMLOCK = 30
const m__SC_MEMLOCK_RANGE = 31
const m__SC_MEMORY_PROTECTION = 32
const m__SC_MESSAGE_PASSING = 33
const m__SC_MONOTONIC_CLOCK = 74
const m__SC_MQ_OPEN_MAX = 46
const m__SC_MQ_PRIO_MAX = 75
const m__SC_NGROUPS_MAX = 4
const m__SC_NPROCESSORS_CONF = 57
const m__SC_NPROCESSORS_ONLN = 58
const m__SC_OPEN_MAX = 5
const m__SC_PAGESIZE = 29
const m__SC_PAGE_SIZE = "_SC_PAGESIZE"
const m__SC_PASS_MAX = 131
const m__SC_PHYS_PAGES = 200
const m__SC_PRIORITIZED_IO = 34
const m__SC_PRIORITY_SCHEDULING = 35
const m__SC_RAW_SOCKETS = 119
const m__SC_READER_WRITER_LOCKS = 76
const m__SC_REALTIME_SIGNALS = 36
const m__SC_REGEXP = 77
const m__SC_RE_DUP_MAX = 16
const m__SC_RTSIG_MAX = 48
const m__SC_SAVED_IDS = 7
const m__SC_SEMAPHORES = 37
const m__SC_SEM_NSEMS_MAX = 49
const m__SC_SEM_VALUE_MAX = 50
const m__SC_SHARED_MEMORY_OBJECTS = 39
const m__SC_SHELL = 78
const m__SC_SIGQUEUE_MAX = 51
const m__SC_SPAWN = 79
const m__SC_SPIN_LOCKS = 80
const m__SC_SPORADIC_SERVER = 81
const m__SC_SS_REPL_MAX = 126
const m__SC_STREAM_MAX = 26
const m__SC_SYMLOOP_MAX = 120
const m__SC_SYNCHRONIZED_IO = 40
const m__SC_THREADS = 96
const m__SC_THREAD_ATTR_STACKADDR = 82
const m__SC_THREAD_ATTR_STACKSIZE = 83
const m__SC_THREAD_CPUTIME = 84
const m__SC_THREAD_DESTRUCTOR_ITERATIONS = 85
const m__SC_THREAD_KEYS_MAX = 86
const m__SC_THREAD_PRIORITY_SCHEDULING = 89
const m__SC_THREAD_PRIO_INHERIT = 87
const m__SC_THREAD_PRIO_PROTECT = 88
const m__SC_THREAD_PROCESS_SHARED = 90
const m__SC_THREAD_SAFE_FUNCTIONS = 91
const m__SC_THREAD_SPORADIC_SERVER = 92
const m__SC_THREAD_STACK_MIN = 93
const m__SC_THREAD_THREADS_MAX = 94
const m__SC_TIMEOUTS = 95
const m__SC_TIMERS = 41
const m__SC_TIMER_MAX = 52
const m__SC_TRACE = 97
const m__SC_TRACE_EVENT_FILTER = 98
const m__SC_TRACE_EVENT_NAME_MAX = 127
const m__SC_TRACE_INHERIT = 99
const m__SC_TRACE_LOG = 100
const m__SC_TRACE_NAME_MAX = 128
const m__SC_TRACE_SYS_MAX = 129
const m__SC_TRACE_USER_EVENT_MAX = 130
const m__SC_TTY_NAME_MAX = 101
const m__SC_TYPED_MEMORY_OBJECTS = 102
const m__SC_TZNAME_MAX = 27
const m__SC_V6_ILP32_OFF32 = 103
const m__SC_V6_ILP32_OFFBIG = 104
const m__SC_V6_LP64_OFF64 = 105
const m__SC_V6_LPBIG_OFFBIG = 106
const m__SC_VERSION = 8
const m__SC_XBS5_ILP32_OFF32 = 122
const m__SC_XBS5_ILP32_OFFBIG = 123
const m__SC_XBS5_LP64_OFF64 = 124
const m__SC_XBS5_LPBIG_OFFBIG = 125
const m__SC_XOPEN_CRYPT = 108
const m__SC_XOPEN_ENH_I18N = 109
const m__SC_XOPEN_LEGACY = 110
const m__SC_XOPEN_REALTIME = 111
const m__SC_XOPEN_REALTIME_THREADS = 112
const m__SC_XOPEN_SHM = 113
const m__SC_XOPEN_STREAMS = 114
const m__SC_XOPEN_UNIX = 115
const m__SC_XOPEN_VERSION = 116
const m__SC_XOPEN_XCU_VERSION = 121
const m__STRUCT_MCONTEXT = "_STRUCT_MCONTEXT64"
const m__USE_FORTIFY_LEVEL = 2
const m__V6_ILP32_OFF32 = "__ILP32_OFF32"
const m__V6_ILP32_OFFBIG = "__ILP32_OFFBIG"
const m__V6_LP64_OFF64 = "__LP64_OFF64"
const m__V6_LPBIG_OFFBIG = "__LPBIG_OFFBIG"
const m__WSTOPPED = 0177
const m__X86_INSTRUCTION_STATE_CACHELINE_SIZE = 64
const m__XBS5_ILP32_OFF32 = "__ILP32_OFF32"
const m__XBS5_ILP32_OFFBIG = "__ILP32_OFFBIG"
const m__XBS5_LP64_OFF64 = "__LP64_OFF64"
const m__XBS5_LPBIG_OFFBIG = "__LPBIG_OFFBIG"
const m__XOPEN_CRYPT = 1
const m__XOPEN_ENH_I18N = 1
const m__XOPEN_SHM = 1
const m__XOPEN_UNIX = 1
const m__XOPEN_VERSION = 600
const m__XOPEN_XCU_VERSION = 4
const m___API_TO_BE_DEPRECATED = 100000
const m___API_TO_BE_DEPRECATED_DRIVERKIT = 100000
const m___API_TO_BE_DEPRECATED_IOS = 100000
const m___API_TO_BE_DEPRECATED_MACCATALYST = 100000
const m___API_TO_BE_DEPRECATED_MACOS = 100000
const m___API_TO_BE_DEPRECATED_TVOS = 100000
const m___API_TO_BE_DEPRECATED_VISIONOS = 100000
const m___API_TO_BE_DEPRECATED_WATCHOS = 100000
const m___APPLE_CC__ = 6000
const m___APPLE__ = 1
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BITINT_MAXWIDTH__ = 128
const m___BLOCKS__ = 1
const m___BOOL_WIDTH__ = 8
const m___BRIDGEOS_2_0 = 20000
const m___BRIDGEOS_3_0 = 30000
const m___BRIDGEOS_3_1 = 30100
const m___BRIDGEOS_3_4 = 30400
const m___BRIDGEOS_4_0 = 40000
const m___BRIDGEOS_4_1 = 40100
const m___BRIDGEOS_5_0 = 50000
const m___BRIDGEOS_5_1 = 50100
const m___BRIDGEOS_5_3 = 50300
const m___BRIDGEOS_6_0 = 60000
const m___BRIDGEOS_6_2 = 60200
const m___BRIDGEOS_6_4 = 60400
const m___BRIDGEOS_6_5 = 60500
const m___BRIDGEOS_6_6 = 60600
const m___BRIDGEOS_7_0 = 70000
const m___BRIDGEOS_7_1 = 70100
const m___BRIDGEOS_7_2 = 70200
const m___BRIDGEOS_7_3 = 70300
const m___BRIDGEOS_7_4 = 70400
const m___BRIDGEOS_7_6 = 70600
const m___BRIDGEOS_8_0 = 80000
const m___BRIDGEOS_8_1 = 80100
const m___BRIDGEOS_8_2 = 80200
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___CLANG_ATOMIC_BOOL_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR_LOCK_FREE = 2
const m___CLANG_ATOMIC_INT_LOCK_FREE = 2
const m___CLANG_ATOMIC_LLONG_LOCK_FREE = 2
const m___CLANG_ATOMIC_LONG_LOCK_FREE = 2
const m___CLANG_ATOMIC_POINTER_LOCK_FREE = 2
const m___CLANG_ATOMIC_SHORT_LOCK_FREE = 2
const m___CLANG_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___CONSTANT_CFSTRINGS__ = 1
const m___DARWIN_64_BIT_INO_T = 1
const m___DARWIN_BIG_ENDIAN = 4321
const m___DARWIN_BYTE_ORDER = "__DARWIN_LITTLE_ENDIAN"
const m___DARWIN_C_ANSI = 010000
const m___DARWIN_C_FULL = 900000
const m___DARWIN_C_LEVEL = "__DARWIN_C_FULL"
const m___DARWIN_FD_SETSIZE = 1024
const m___DARWIN_LITTLE_ENDIAN = 1234
const m___DARWIN_NBBY = 8
const m___DARWIN_NON_CANCELABLE = 0
const m___DARWIN_NO_LONG_LONG = 0
const m___DARWIN_NSIG = 32
const m___DARWIN_ONLY_64_BIT_INO_T = 0
const m___DARWIN_ONLY_UNIX_CONFORMANCE = 1
const m___DARWIN_ONLY_VERS_1050 = 0
const m___DARWIN_PDP_ENDIAN = 3412
const m___DARWIN_SUF_1050 = "$1050"
const m___DARWIN_SUF_64_BIT_INO_T = "$INODE64"
const m___DARWIN_SUF_EXTSN = "$DARWIN_EXTSN"
const m___DARWIN_UNIX03 = 1
const m___DARWIN_VERS_1050 = 1
const m___DARWIN_WCHAR_MAX = "__WCHAR_MAX__"
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DENORM_MIN__ = 4.9406564584124654e-324
const m___DBL_DIG__ = 15
const m___DBL_EPSILON__ = 2.2204460492503131e-16
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DBL_MAX__ = 1.7976931348623157e+308
const m___DBL_MIN__ = 2.2250738585072014e-308
const m___DECIMAL_DIG__ = "__LDBL_DECIMAL_DIG__"
const m___DRIVERKIT_19_0 = 190000
const m___DRIVERKIT_20_0 = 200000
const m___DRIVERKIT_21_0 = 210000
const m___DRIVERKIT_22_0 = 220000
const m___DRIVERKIT_22_4 = 220400
const m___DRIVERKIT_22_5 = 220500
const m___DRIVERKIT_22_6 = 220600
const m___DRIVERKIT_23_0 = 230000
const m___DRIVERKIT_23_1 = 230100
const m___DRIVERKIT_23_2 = 230200
const m___DYNAMIC__ = 1
const m___ENABLE_LEGACY_MAC_AVAILABILITY = 1
const m___ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__ = 140000
const m___ENVIRONMENT_OS_VERSION_MIN_REQUIRED__ = 140000
const m___FINITE_MATH_ONLY__ = 0
const m___FLT16_DECIMAL_DIG__ = 5
const m___FLT16_DENORM_MIN__ = 5.9604644775390625e-8
const m___FLT16_DIG__ = 3
const m___FLT16_EPSILON__ = 9.765625e-4
const m___FLT16_HAS_DENORM__ = 1
const m___FLT16_HAS_INFINITY__ = 1
const m___FLT16_HAS_QUIET_NAN__ = 1
const m___FLT16_MANT_DIG__ = 11
const m___FLT16_MAX_10_EXP__ = 4
const m___FLT16_MAX_EXP__ = 16
const m___FLT16_MAX__ = 6.5504e+4
const m___FLT16_MIN__ = 6.103515625e-5
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209290e-7
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282347e+38
const m___FLT_MIN__ = 1.17549435e-38
const m___FLT_RADIX__ = 2
const m___FUNCTION__ = "__func__"
const m___FXSR__ = 1
const m___GCC_ASM_FLAG_OUTPUTS__ = 1
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 1
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC__ = 4
const m___GXX_ABI_VERSION = 1002
const m___INT16_FMTd__ = "hd"
const m___INT16_FMTi__ = "hi"
const m___INT16_MAX__ = 32767
const m___INT16_TYPE__ = "short"
const m___INT32_FMTd__ = "d"
const m___INT32_FMTi__ = "i"
const m___INT32_MAX__ = 2147483647
const m___INT32_TYPE__ = "int"
const m___INT64_C_SUFFIX__ = "LL"
const m___INT64_FMTd__ = "lld"
const m___INT64_FMTi__ = "lli"
const m___INT64_MAX__ = 9223372036854775807
const m___INT8_FMTd__ = "hhd"
const m___INT8_FMTi__ = "hhi"
const m___INT8_MAX__ = 127
const m___INTMAX_C_SUFFIX__ = "L"
const m___INTMAX_FMTd__ = "ld"
const m___INTMAX_FMTi__ = "li"
const m___INTMAX_MAX__ = 9223372036854775807
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_FMTd__ = "ld"
const m___INTPTR_FMTi__ = "li"
const m___INTPTR_MAX__ = 9223372036854775807
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_FMTd__ = "hd"
const m___INT_FAST16_FMTi__ = "hi"
const m___INT_FAST16_MAX__ = 32767
const m___INT_FAST16_TYPE__ = "short"
const m___INT_FAST16_WIDTH__ = 16
const m___INT_FAST32_FMTd__ = "d"
const m___INT_FAST32_FMTi__ = "i"
const m___INT_FAST32_MAX__ = 2147483647
const m___INT_FAST32_TYPE__ = "int"
const m___INT_FAST32_WIDTH__ = 32
const m___INT_FAST64_FMTd__ = "lld"
const m___INT_FAST64_FMTi__ = "lli"
const m___INT_FAST64_MAX__ = 9223372036854775807
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_FMTd__ = "hhd"
const m___INT_FAST8_FMTi__ = "hhi"
const m___INT_FAST8_MAX__ = 127
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_FMTd__ = "hd"
const m___INT_LEAST16_FMTi__ = "hi"
const m___INT_LEAST16_MAX = "INT16_MAX"
const m___INT_LEAST16_MAX__ = 32767
const m___INT_LEAST16_MIN = "INT16_MIN"
const m___INT_LEAST16_TYPE__ = "short"
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_FMTd__ = "d"
const m___INT_LEAST32_FMTi__ = "i"
const m___INT_LEAST32_MAX = "INT32_MAX"
const m___INT_LEAST32_MAX__ = 2147483647
const m___INT_LEAST32_MIN = "INT32_MIN"
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_FMTd__ = "lld"
const m___INT_LEAST64_FMTi__ = "lli"
const m___INT_LEAST64_MAX = "INT64_MAX"
const m___INT_LEAST64_MAX__ = 9223372036854775807
const m___INT_LEAST64_MIN = "INT64_MIN"
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_FMTd__ = "hhd"
const m___INT_LEAST8_FMTi__ = "hhi"
const m___INT_LEAST8_MAX = "INT8_MAX"
const m___INT_LEAST8_MAX__ = 127
const m___INT_LEAST8_MIN = "INT8_MIN"
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 2147483647
const m___INT_WIDTH__ = 32
const m___IPHONE_10_0 = 100000
const m___IPHONE_10_1 = 100100
const m___IPHONE_10_2 = 100200
const m___IPHONE_10_3 = 100300
const m___IPHONE_11_0 = 110000
const m___IPHONE_11_1 = 110100
const m___IPHONE_11_2 = 110200
const m___IPHONE_11_3 = 110300
const m___IPHONE_11_4 = 110400
const m___IPHONE_12_0 = 120000
const m___IPHONE_12_1 = 120100
const m___IPHONE_12_2 = 120200
const m___IPHONE_12_3 = 120300
const m___IPHONE_12_4 = 120400
const m___IPHONE_13_0 = 130000
const m___IPHONE_13_1 = 130100
const m___IPHONE_13_2 = 130200
const m___IPHONE_13_3 = 130300
const m___IPHONE_13_4 = 130400
const m___IPHONE_13_5 = 130500
const m___IPHONE_13_6 = 130600
const m___IPHONE_13_7 = 130700
const m___IPHONE_14_0 = 140000
const m___IPHONE_14_1 = 140100
const m___IPHONE_14_2 = 140200
const m___IPHONE_14_3 = 140300
const m___IPHONE_14_4 = 140400
const m___IPHONE_14_5 = 140500
const m___IPHONE_14_6 = 140600
const m___IPHONE_14_7 = 140700
const m___IPHONE_14_8 = 140800
const m___IPHONE_15_0 = 150000
const m___IPHONE_15_1 = 150100
const m___IPHONE_15_2 = 150200
const m___IPHONE_15_3 = 150300
const m___IPHONE_15_4 = 150400
const m___IPHONE_15_5 = 150500
const m___IPHONE_15_6 = 150600
const m___IPHONE_16_0 = 160000
const m___IPHONE_16_1 = 160100
const m___IPHONE_16_2 = 160200
const m___IPHONE_16_3 = 160300
const m___IPHONE_16_4 = 160400
const m___IPHONE_16_5 = 160500
const m___IPHONE_16_6 = 160600
const m___IPHONE_16_7 = 160700
const m___IPHONE_17_0 = 170000
const m___IPHONE_17_1 = 170100
const m___IPHONE_17_2 = 170200
const m___IPHONE_2_0 = 20000
const m___IPHONE_2_1 = 20100
const m___IPHONE_2_2 = 20200
const m___IPHONE_3_0 = 30000
const m___IPHONE_3_1 = 30100
const m___IPHONE_3_2 = 30200
const m___IPHONE_4_0 = 40000
const m___IPHONE_4_1 = 40100
const m___IPHONE_4_2 = 40200
const m___IPHONE_4_3 = 40300
const m___IPHONE_5_0 = 50000
const m___IPHONE_5_1 = 50100
const m___IPHONE_6_0 = 60000
const m___IPHONE_6_1 = 60100
const m___IPHONE_7_0 = 70000
const m___IPHONE_7_1 = 70100
const m___IPHONE_8_0 = 80000
const m___IPHONE_8_1 = 80100
const m___IPHONE_8_2 = 80200
const m___IPHONE_8_3 = 80300
const m___IPHONE_8_4 = 80400
const m___IPHONE_9_0 = 90000
const m___IPHONE_9_1 = 90100
const m___IPHONE_9_2 = 90200
const m___IPHONE_9_3 = 90300
const m___LAHF_SAHF__ = 1
const m___LASTBRANCH_MAX = 32
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.2204460492503131e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.7976931348623157e+308
const m___LDBL_MIN__ = 2.2250738585072014e-308
const m___LITTLE_ENDIAN__ = 1
const m___LLONG_WIDTH__ = 64
const m___LONG_LONG_MAX__ = 9223372036854775807
const m___LONG_MAX__ = 9223372036854775807
const m___LONG_WIDTH__ = 64
const m___LP64_OFF64 = 1
const m___LP64__ = 1
const m___LPBIG_OFFBIG = 1
const m___MACH__ = 1
const m___MAC_10_0 = 1000
const m___MAC_10_1 = 1010
const m___MAC_10_10 = 101000
const m___MAC_10_10_2 = 101002
const m___MAC_10_10_3 = 101003
const m___MAC_10_11 = 101100
const m___MAC_10_11_2 = 101102
const m___MAC_10_11_3 = 101103
const m___MAC_10_11_4 = 101104
const m___MAC_10_12 = 101200
const m___MAC_10_12_1 = 101201
const m___MAC_10_12_2 = 101202
const m___MAC_10_12_4 = 101204
const m___MAC_10_13 = 101300
const m___MAC_10_13_1 = 101301
const m___MAC_10_13_2 = 101302
const m___MAC_10_13_4 = 101304
const m___MAC_10_14 = 101400
const m___MAC_10_14_1 = 101401
const m___MAC_10_14_4 = 101404
const m___MAC_10_14_5 = 101405
const m___MAC_10_14_6 = 101406
const m___MAC_10_15 = 101500
const m___MAC_10_15_1 = 101501
const m___MAC_10_15_4 = 101504
const m___MAC_10_16 = 101600
const m___MAC_10_2 = 1020
const m___MAC_10_3 = 1030
const m___MAC_10_4 = 1040
const m___MAC_10_5 = 1050
const m___MAC_10_6 = 1060
const m___MAC_10_7 = 1070
const m___MAC_10_8 = 1080
const m___MAC_10_9 = 1090
const m___MAC_11_0 = 110000
const m___MAC_11_1 = 110100
const m___MAC_11_3 = 110300
const m___MAC_11_4 = 110400
const m___MAC_11_5 = 110500
const m___MAC_11_6 = 110600
const m___MAC_12_0 = 120000
const m___MAC_12_1 = 120100
const m___MAC_12_2 = 120200
const m___MAC_12_3 = 120300
const m___MAC_12_4 = 120400
const m___MAC_12_5 = 120500
const m___MAC_12_6 = 120600
const m___MAC_12_7 = 120700
const m___MAC_13_0 = 130000
const m___MAC_13_1 = 130100
const m___MAC_13_2 = 130200
const m___MAC_13_3 = 130300
const m___MAC_13_4 = 130400
const m___MAC_13_5 = 130500
const m___MAC_13_6 = 130600
const m___MAC_14_0 = 140000
const m___MAC_14_1 = 140100
const m___MAC_14_2 = 140200
const m___MAC_OS_X_VERSION_MAX_ALLOWED = "__MAC_14_2"
const m___MAC_OS_X_VERSION_MIN_REQUIRED = "__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__"
const m___MMX__ = 1
const m___NO_INLINE__ = 1
const m___NO_MATH_ERRNO__ = 1
const m___NO_MATH_INLINES = 1
const m___OBJC_BOOL_IS_BOOL = 0
const m___OPENCL_MEMORY_SCOPE_ALL_SVM_DEVICES = 3
const m___OPENCL_MEMORY_SCOPE_DEVICE = 2
const m___OPENCL_MEMORY_SCOPE_SUB_GROUP = 4
const m___OPENCL_MEMORY_SCOPE_WORK_GROUP = 1
const m___OPENCL_MEMORY_SCOPE_WORK_ITEM = 0
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PIC__ = 2
const m___POINTER_WIDTH__ = 64
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTHREAD_ATTR_SIZE__ = 56
const m___PTHREAD_CONDATTR_SIZE__ = 8
const m___PTHREAD_COND_SIZE__ = 40
const m___PTHREAD_MUTEXATTR_SIZE__ = 8
const m___PTHREAD_MUTEX_SIZE__ = 56
const m___PTHREAD_ONCE_SIZE__ = 8
const m___PTHREAD_RWLOCKATTR_SIZE__ = 16
const m___PTHREAD_RWLOCK_SIZE__ = 192
const m___PTHREAD_SIZE__ = 8176
const m___PTRDIFF_FMTd__ = "ld"
const m___PTRDIFF_FMTi__ = "li"
const m___PTRDIFF_MAX__ = 9223372036854775807
const m___PTRDIFF_WIDTH__ = 64
const m___SALC = 0x4000
const m___SAPP = 0x0100
const m___SCHAR_MAX__ = 127
const m___SEG_FS = 1
const m___SEG_GS = 1
const m___SEOF = 0x0020
const m___SERR = 0x0040
const m___SHRT_MAX__ = 32767
const m___SHRT_WIDTH__ = 16
const m___SIGN = 0x8000
const m___SIG_ATOMIC_MAX__ = 2147483647
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_FMTX__ = "lX"
const m___SIZE_FMTo__ = "lo"
const m___SIZE_FMTu__ = "lu"
const m___SIZE_FMTx__ = "lx"
const m___SIZE_MAX__ = 18446744073709551615
const m___SIZE_WIDTH__ = 64
const m___SLBF = 0x0001
const m___SMBF = 0x0080
const m___SMOD = 0x2000
const m___SNBF = 0x0002
const m___SNPT = 0x0800
const m___SOFF = 0x1000
const m___SOPT = 0x0400
const m___SRD = 0x0004
const m___SRW = 0x0010
const m___SSE2_MATH__ = 1
const m___SSE2__ = 1
const m___SSE3__ = 1
const m___SSE4_1__ = 1
const m___SSE_MATH__ = 1
const m___SSE__ = 1
const m___SSP__ = 1
const m___SSSE3__ = 1
const m___SSTR = 0x0200
const m___STDC_HOSTED__ = 1
const m___STDC_NO_THREADS__ = 1
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC_WANT_LIB_EXT1__ = 1
const m___STDC__ = 1
const m___SWR = 0x0008
const m___TVOS_10_0 = 100000
const m___TVOS_10_0_1 = 100001
const m___TVOS_10_1 = 100100
const m___TVOS_10_2 = 100200
const m___TVOS_11_0 = 110000
const m___TVOS_11_1 = 110100
const m___TVOS_11_2 = 110200
const m___TVOS_11_3 = 110300
const m___TVOS_11_4 = 110400
const m___TVOS_12_0 = 120000
const m___TVOS_12_1 = 120100
const m___TVOS_12_2 = 120200
const m___TVOS_12_3 = 120300
const m___TVOS_12_4 = 120400
const m___TVOS_13_0 = 130000
const m___TVOS_13_2 = 130200
const m___TVOS_13_3 = 130300
const m___TVOS_13_4 = 130400
const m___TVOS_14_0 = 140000
const m___TVOS_14_1 = 140100
const m___TVOS_14_2 = 140200
const m___TVOS_14_3 = 140300
const m___TVOS_14_5 = 140500
const m___TVOS_14_6 = 140600
const m___TVOS_14_7 = 140700
const m___TVOS_15_0 = 150000
const m___TVOS_15_1 = 150100
const m___TVOS_15_2 = 150200
const m___TVOS_15_3 = 150300
const m___TVOS_15_4 = 150400
const m___TVOS_15_5 = 150500
const m___TVOS_15_6 = 150600
const m___TVOS_16_0 = 160000
const m___TVOS_16_1 = 160100
const m___TVOS_16_2 = 160200
const m___TVOS_16_3 = 160300
const m___TVOS_16_4 = 160400
const m___TVOS_16_5 = 160500
const m___TVOS_16_6 = 160600
const m___TVOS_17_0 = 170000
const m___TVOS_17_1 = 170100
const m___TVOS_17_2 = 170200
const m___TVOS_9_0 = 90000
const m___TVOS_9_1 = 90100
const m___TVOS_9_2 = 90200
const m___UINT16_FMTX__ = "hX"
const m___UINT16_FMTo__ = "ho"
const m___UINT16_FMTu__ = "hu"
const m___UINT16_FMTx__ = "hx"
const m___UINT16_MAX__ = 65535
const m___UINT32_C_SUFFIX__ = "U"
const m___UINT32_FMTX__ = "X"
const m___UINT32_FMTo__ = "o"
const m___UINT32_FMTu__ = "u"
const m___UINT32_FMTx__ = "x"
const m___UINT32_MAX__ = 4294967295
const m___UINT64_C_SUFFIX__ = "ULL"
const m___UINT64_FMTX__ = "llX"
const m___UINT64_FMTo__ = "llo"
const m___UINT64_FMTu__ = "llu"
const m___UINT64_FMTx__ = "llx"
const m___UINT64_MAX__ = "18446744073709551615U"
const m___UINT8_FMTX__ = "hhX"
const m___UINT8_FMTo__ = "hho"
const m___UINT8_FMTu__ = "hhu"
const m___UINT8_FMTx__ = "hhx"
const m___UINT8_MAX__ = 255
const m___UINTMAX_C_SUFFIX__ = "UL"
const m___UINTMAX_FMTX__ = "lX"
const m___UINTMAX_FMTo__ = "lo"
const m___UINTMAX_FMTu__ = "lu"
const m___UINTMAX_FMTx__ = "lx"
const m___UINTMAX_MAX__ = 18446744073709551615
const m___UINTMAX_WIDTH__ = 64
const m___UINTPTR_FMTX__ = "lX"
const m___UINTPTR_FMTo__ = "lo"
const m___UINTPTR_FMTu__ = "lu"
const m___UINTPTR_FMTx__ = "lx"
const m___UINTPTR_MAX__ = 18446744073709551615
const m___UINTPTR_WIDTH__ = 64
const m___UINT_FAST16_FMTX__ = "hX"
const m___UINT_FAST16_FMTo__ = "ho"
const m___UINT_FAST16_FMTu__ = "hu"
const m___UINT_FAST16_FMTx__ = "hx"
const m___UINT_FAST16_MAX__ = 65535
const m___UINT_FAST32_FMTX__ = "X"
const m___UINT_FAST32_FMTo__ = "o"
const m___UINT_FAST32_FMTu__ = "u"
const m___UINT_FAST32_FMTx__ = "x"
const m___UINT_FAST32_MAX__ = 4294967295
const m___UINT_FAST64_FMTX__ = "llX"
const m___UINT_FAST64_FMTo__ = "llo"
const m___UINT_FAST64_FMTu__ = "llu"
const m___UINT_FAST64_FMTx__ = "llx"
const m___UINT_FAST64_MAX__ = "18446744073709551615U"
const m___UINT_FAST8_FMTX__ = "hhX"
const m___UINT_FAST8_FMTo__ = "hho"
const m___UINT_FAST8_FMTu__ = "hhu"
const m___UINT_FAST8_FMTx__ = "hhx"
const m___UINT_FAST8_MAX__ = 255
const m___UINT_LEAST16_FMTX__ = "hX"
const m___UINT_LEAST16_FMTo__ = "ho"
const m___UINT_LEAST16_FMTu__ = "hu"
const m___UINT_LEAST16_FMTx__ = "hx"
const m___UINT_LEAST16_MAX = "UINT16_MAX"
const m___UINT_LEAST16_MAX__ = 65535
const m___UINT_LEAST32_FMTX__ = "X"
const m___UINT_LEAST32_FMTo__ = "o"
const m___UINT_LEAST32_FMTu__ = "u"
const m___UINT_LEAST32_FMTx__ = "x"
const m___UINT_LEAST32_MAX = "UINT32_MAX"
const m___UINT_LEAST32_MAX__ = 4294967295
const m___UINT_LEAST64_FMTX__ = "llX"
const m___UINT_LEAST64_FMTo__ = "llo"
const m___UINT_LEAST64_FMTu__ = "llu"
const m___UINT_LEAST64_FMTx__ = "llx"
const m___UINT_LEAST64_MAX = "UINT64_MAX"
const m___UINT_LEAST64_MAX__ = "18446744073709551615U"
const m___UINT_LEAST8_FMTX__ = "hhX"
const m___UINT_LEAST8_FMTo__ = "hho"
const m___UINT_LEAST8_FMTu__ = "hhu"
const m___UINT_LEAST8_FMTx__ = "hhx"
const m___UINT_LEAST8_MAX = "UINT8_MAX"
const m___UINT_LEAST8_MAX__ = 255
const m___USER_LABEL_PREFIX__ = "_"
const m___VERSION__ = "Apple LLVM 15.0.0 (clang-1500.1.0.2.5)"
const m___VISIONOS_1_0 = 10000
const m___WATCHOS_10_0 = 100000
const m___WATCHOS_10_1 = 100100
const m___WATCHOS_10_2 = 100200
const m___WATCHOS_1_0 = 10000
const m___WATCHOS_2_0 = 20000
const m___WATCHOS_2_1 = 20100
const m___WATCHOS_2_2 = 20200
const m___WATCHOS_3_0 = 30000
const m___WATCHOS_3_1 = 30100
const m___WATCHOS_3_1_1 = 30101
const m___WATCHOS_3_2 = 30200
const m___WATCHOS_4_0 = 40000
const m___WATCHOS_4_1 = 40100
const m___WATCHOS_4_2 = 40200
const m___WATCHOS_4_3 = 40300
const m___WATCHOS_5_0 = 50000
const m___WATCHOS_5_1 = 50100
const m___WATCHOS_5_2 = 50200
const m___WATCHOS_5_3 = 50300
const m___WATCHOS_6_0 = 60000
const m___WATCHOS_6_1 = 60100
const m___WATCHOS_6_2 = 60200
const m___WATCHOS_7_0 = 70000
const m___WATCHOS_7_1 = 70100
const m___WATCHOS_7_2 = 70200
const m___WATCHOS_7_3 = 70300
const m___WATCHOS_7_4 = 70400
const m___WATCHOS_7_5 = 70500
const m___WATCHOS_7_6 = 70600
const m___WATCHOS_8_0 = 80000
const m___WATCHOS_8_1 = 80100
const m___WATCHOS_8_3 = 80300
const m___WATCHOS_8_4 = 80400
const m___WATCHOS_8_5 = 80500
const m___WATCHOS_8_6 = 80600
const m___WATCHOS_8_7 = 80700
const m___WATCHOS_9_0 = 90000
const m___WATCHOS_9_1 = 90100
const m___WATCHOS_9_2 = 90200
const m___WATCHOS_9_3 = 90300
const m___WATCHOS_9_4 = 90400
const m___WATCHOS_9_5 = 90500
const m___WATCHOS_9_6 = 90600
const m___WCHAR_MAX__ = 2147483647
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 2147483647
const m___WINT_TYPE__ = "int"
const m___WINT_WIDTH__ = 32
const m___amd64 = 1
const m___amd64__ = 1
const m___apple_build_version__ = 15000100
const m___clang__ = 1
const m___clang_literal_encoding__ = "UTF-8"
const m___clang_major__ = 15
const m___clang_minor__ = 0
const m___clang_patchlevel__ = 0
const m___clang_version__ = "15.0.0 (clang-1500.1.0.2.5)"
const m___clang_wide_literal_encoding__ = "UTF-32"
const m___code_model_small__ = 1
const m___const = "const"
const m___core2 = 1
const m___core2__ = 1
const m___has_ptrcheck = 0
const m___header_inline = "inline"
const m___int16_c_suffix = "__INT16_C_SUFFIX__"
const m___int32_c_suffix = "__INT32_C_SUFFIX__"
const m___int64_c_suffix = "__INT64_C_SUFFIX__"
const m___int8_c_suffix = "__INT8_C_SUFFIX__"
const m___int_least16_t = "int16_t"
const m___int_least32_t = "int32_t"
const m___int_least64_t = "int64_t"
const m___int_least8_t = "int8_t"
const m___llvm__ = 1
const m___nonnull = "_Nonnull"
const m___null_unspecified = "_Null_unspecified"
const m___nullable = "_Nullable"
const m___pic__ = 2
const m___restrict = "restrict"
const m___restrict_arr = "restrict"
const m___signed = "signed"
const m___tune_core2__ = 1
const m___uint_least16_t = "uint16_t"
const m___uint_least32_t = "uint32_t"
const m___uint_least64_t = "uint64_t"
const m___uint_least8_t = "uint8_t"
const m___volatile = "volatile"
const m___x86_64 = 1
const m___x86_64__ = 1
const m_ru_first = "ru_ixrss"
const m_ru_last = "ru_nivcsw"
const m_stderr = "__stderrp"
const m_stdin = "__stdinp"
const m_stdout = "__stdoutp"
const m_sv_onstack = "sv_flags"

type T__builtin_va_list = uintptr

type T__predefined_size_t = uint64

type T__predefined_wchar_t = int32

type T__predefined_ptrdiff_t = int64

type T__int8_t = int8

type T__uint8_t = uint8

type T__int16_t = int16

type T__uint16_t = uint16

type T__int32_t = int32

type T__uint32_t = uint32

type T__int64_t = int64

type T__uint64_t = uint64

type T__darwin_intptr_t = int64

type T__darwin_natural_t = uint32

type T__darwin_ct_rune_t = int32

type T__mbstate_t = struct {
	F_mbstateL  [0]int64
	F__mbstate8 [128]int8
}

type T__darwin_mbstate_t = struct {
	F_mbstateL  [0]int64
	F__mbstate8 [128]int8
}

type T__darwin_ptrdiff_t = int64

type T__darwin_size_t = uint64

type T__darwin_va_list = uintptr

type T__darwin_wchar_t = int32

type T__darwin_rune_t = int32

type T__darwin_wint_t = int32

type T__darwin_clock_t = uint64

type T__darwin_socklen_t = uint32

type T__darwin_ssize_t = int64

type T__darwin_time_t = int64

type T__darwin_blkcnt_t = int64

type T__darwin_blksize_t = int32

type T__darwin_dev_t = int32

type T__darwin_fsblkcnt_t = uint32

type T__darwin_fsfilcnt_t = uint32

type T__darwin_gid_t = uint32

type T__darwin_id_t = uint32

type T__darwin_ino64_t = uint64

type T__darwin_ino_t = uint64

type T__darwin_mach_port_name_t = uint32

type T__darwin_mach_port_t = uint32

type T__darwin_mode_t = uint16

type T__darwin_off_t = int64

type T__darwin_pid_t = int32

type T__darwin_sigset_t = uint32

type T__darwin_suseconds_t = int32

type T__darwin_uid_t = uint32

type T__darwin_useconds_t = uint32

type T__darwin_uuid_t = [16]uint8

type T__darwin_uuid_string_t = [37]int8

type T__darwin_pthread_handler_rec = struct {
	F__routine uintptr
	F__arg     uintptr
	F__next    uintptr
}

type T_opaque_pthread_attr_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type T_opaque_pthread_cond_t = struct {
	F__sig    int64
	F__opaque [40]int8
}

type T_opaque_pthread_condattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T_opaque_pthread_mutex_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type T_opaque_pthread_mutexattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T_opaque_pthread_once_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T_opaque_pthread_rwlock_t = struct {
	F__sig    int64
	F__opaque [192]int8
}

type T_opaque_pthread_rwlockattr_t = struct {
	F__sig    int64
	F__opaque [16]int8
}

type T_opaque_pthread_t = struct {
	F__sig           int64
	F__cleanup_stack uintptr
	F__opaque        [8176]int8
}

type T__darwin_pthread_attr_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type T__darwin_pthread_cond_t = struct {
	F__sig    int64
	F__opaque [40]int8
}

type T__darwin_pthread_condattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T__darwin_pthread_key_t = uint64

type T__darwin_pthread_mutex_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type T__darwin_pthread_mutexattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T__darwin_pthread_once_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T__darwin_pthread_rwlock_t = struct {
	F__sig    int64
	F__opaque [192]int8
}

type T__darwin_pthread_rwlockattr_t = struct {
	F__sig    int64
	F__opaque [16]int8
}

type T__darwin_pthread_t = uintptr

type T__darwin_nl_item = int32

type T__darwin_wctrans_t = int32

type T__darwin_wctype_t = uint32

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tu_int64_t = uint64

type Tregister_t = int64

type Tintptr_t = int64

type Tuintptr_t = uint64

type Tuser_addr_t = uint64

type Tuser_size_t = uint64

type Tuser_ssize_t = int64

type Tuser_long_t = int64

type Tuser_ulong_t = uint64

type Tuser_time_t = int64

type Tuser_off_t = int64

type Tsyscall_arg_t = uint64

type Tva_list = uintptr

type Tsize_t = uint64

type Tfpos_t = int64

type T__sbuf = struct {
	F_base uintptr
	F_size int32
}

type TFILE = struct {
	F_p       uintptr
	F_r       int32
	F_w       int32
	F_flags   int16
	F_file    int16
	F_bf      T__sbuf
	F_lbfsize int32
	F_cookie  uintptr
	F_close   uintptr
	F_read    uintptr
	F_seek    uintptr
	F_write   uintptr
	F_ub      T__sbuf
	F_extra   uintptr
	F_ur      int32
	F_ubuf    [3]uint8
	F_nbuf    [1]uint8
	F_lb      T__sbuf
	F_blksize int32
	F_offset  Tfpos_t
}

type T__sFILE = TFILE

type Toff_t = int64

type Tssize_t = int64

type Tidtype_t = int32

const P_ALL = 0
const P_PID = 1
const P_PGID = 2

type Tpid_t = int32

type Tid_t = uint32

type Tsig_atomic_t = int32

type T__darwin_i386_thread_state = struct {
	F__eax    uint32
	F__ebx    uint32
	F__ecx    uint32
	F__edx    uint32
	F__edi    uint32
	F__esi    uint32
	F__ebp    uint32
	F__esp    uint32
	F__ss     uint32
	F__eflags uint32
	F__eip    uint32
	F__cs     uint32
	F__ds     uint32
	F__es     uint32
	F__fs     uint32
	F__gs     uint32
}

type T__darwin_fp_control = struct {
	F__ccgo0 uint16
}

type T__darwin_fp_control_t = struct {
	F__ccgo0 uint16
}

type T__darwin_fp_status = struct {
	F__ccgo0 uint16
}

type T__darwin_fp_status_t = struct {
	F__ccgo0 uint16
}

type T__darwin_mmst_reg = struct {
	F__mmst_reg  [10]int8
	F__mmst_rsrv [6]int8
}

type T__darwin_xmm_reg = struct {
	F__xmm_reg [16]int8
}

type T__darwin_ymm_reg = struct {
	F__ymm_reg [32]int8
}

type T__darwin_zmm_reg = struct {
	F__zmm_reg [64]int8
}

type T__darwin_opmask_reg = struct {
	F__opmask_reg [8]int8
}

type T__darwin_i386_float_state = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       T__darwin_fp_control
	F__fpu_fsw       T__darwin_fp_status
	F__fpu_ftw       T__uint8_t
	F__fpu_rsrv1     T__uint8_t
	F__fpu_fop       T__uint16_t
	F__fpu_ip        T__uint32_t
	F__fpu_cs        T__uint16_t
	F__fpu_rsrv2     T__uint16_t
	F__fpu_dp        T__uint32_t
	F__fpu_ds        T__uint16_t
	F__fpu_rsrv3     T__uint16_t
	F__fpu_mxcsr     T__uint32_t
	F__fpu_mxcsrmask T__uint32_t
	F__fpu_stmm0     T__darwin_mmst_reg
	F__fpu_stmm1     T__darwin_mmst_reg
	F__fpu_stmm2     T__darwin_mmst_reg
	F__fpu_stmm3     T__darwin_mmst_reg
	F__fpu_stmm4     T__darwin_mmst_reg
	F__fpu_stmm5     T__darwin_mmst_reg
	F__fpu_stmm6     T__darwin_mmst_reg
	F__fpu_stmm7     T__darwin_mmst_reg
	F__fpu_xmm0      T__darwin_xmm_reg
	F__fpu_xmm1      T__darwin_xmm_reg
	F__fpu_xmm2      T__darwin_xmm_reg
	F__fpu_xmm3      T__darwin_xmm_reg
	F__fpu_xmm4      T__darwin_xmm_reg
	F__fpu_xmm5      T__darwin_xmm_reg
	F__fpu_xmm6      T__darwin_xmm_reg
	F__fpu_xmm7      T__darwin_xmm_reg
	F__fpu_rsrv4     [224]int8
	F__fpu_reserved1 int32
}

type T__darwin_i386_avx_state = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       T__darwin_fp_control
	F__fpu_fsw       T__darwin_fp_status
	F__fpu_ftw       T__uint8_t
	F__fpu_rsrv1     T__uint8_t
	F__fpu_fop       T__uint16_t
	F__fpu_ip        T__uint32_t
	F__fpu_cs        T__uint16_t
	F__fpu_rsrv2     T__uint16_t
	F__fpu_dp        T__uint32_t
	F__fpu_ds        T__uint16_t
	F__fpu_rsrv3     T__uint16_t
	F__fpu_mxcsr     T__uint32_t
	F__fpu_mxcsrmask T__uint32_t
	F__fpu_stmm0     T__darwin_mmst_reg
	F__fpu_stmm1     T__darwin_mmst_reg
	F__fpu_stmm2     T__darwin_mmst_reg
	F__fpu_stmm3     T__darwin_mmst_reg
	F__fpu_stmm4     T__darwin_mmst_reg
	F__fpu_stmm5     T__darwin_mmst_reg
	F__fpu_stmm6     T__darwin_mmst_reg
	F__fpu_stmm7     T__darwin_mmst_reg
	F__fpu_xmm0      T__darwin_xmm_reg
	F__fpu_xmm1      T__darwin_xmm_reg
	F__fpu_xmm2      T__darwin_xmm_reg
	F__fpu_xmm3      T__darwin_xmm_reg
	F__fpu_xmm4      T__darwin_xmm_reg
	F__fpu_xmm5      T__darwin_xmm_reg
	F__fpu_xmm6      T__darwin_xmm_reg
	F__fpu_xmm7      T__darwin_xmm_reg
	F__fpu_rsrv4     [224]int8
	F__fpu_reserved1 int32
	F__avx_reserved1 [64]int8
	F__fpu_ymmh0     T__darwin_xmm_reg
	F__fpu_ymmh1     T__darwin_xmm_reg
	F__fpu_ymmh2     T__darwin_xmm_reg
	F__fpu_ymmh3     T__darwin_xmm_reg
	F__fpu_ymmh4     T__darwin_xmm_reg
	F__fpu_ymmh5     T__darwin_xmm_reg
	F__fpu_ymmh6     T__darwin_xmm_reg
	F__fpu_ymmh7     T__darwin_xmm_reg
}

type T__darwin_i386_avx512_state = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       T__darwin_fp_control
	F__fpu_fsw       T__darwin_fp_status
	F__fpu_ftw       T__uint8_t
	F__fpu_rsrv1     T__uint8_t
	F__fpu_fop       T__uint16_t
	F__fpu_ip        T__uint32_t
	F__fpu_cs        T__uint16_t
	F__fpu_rsrv2     T__uint16_t
	F__fpu_dp        T__uint32_t
	F__fpu_ds        T__uint16_t
	F__fpu_rsrv3     T__uint16_t
	F__fpu_mxcsr     T__uint32_t
	F__fpu_mxcsrmask T__uint32_t
	F__fpu_stmm0     T__darwin_mmst_reg
	F__fpu_stmm1     T__darwin_mmst_reg
	F__fpu_stmm2     T__darwin_mmst_reg
	F__fpu_stmm3     T__darwin_mmst_reg
	F__fpu_stmm4     T__darwin_mmst_reg
	F__fpu_stmm5     T__darwin_mmst_reg
	F__fpu_stmm6     T__darwin_mmst_reg
	F__fpu_stmm7     T__darwin_mmst_reg
	F__fpu_xmm0      T__darwin_xmm_reg
	F__fpu_xmm1      T__darwin_xmm_reg
	F__fpu_xmm2      T__darwin_xmm_reg
	F__fpu_xmm3      T__darwin_xmm_reg
	F__fpu_xmm4      T__darwin_xmm_reg
	F__fpu_xmm5      T__darwin_xmm_reg
	F__fpu_xmm6      T__darwin_xmm_reg
	F__fpu_xmm7      T__darwin_xmm_reg
	F__fpu_rsrv4     [224]int8
	F__fpu_reserved1 int32
	F__avx_reserved1 [64]int8
	F__fpu_ymmh0     T__darwin_xmm_reg
	F__fpu_ymmh1     T__darwin_xmm_reg
	F__fpu_ymmh2     T__darwin_xmm_reg
	F__fpu_ymmh3     T__darwin_xmm_reg
	F__fpu_ymmh4     T__darwin_xmm_reg
	F__fpu_ymmh5     T__darwin_xmm_reg
	F__fpu_ymmh6     T__darwin_xmm_reg
	F__fpu_ymmh7     T__darwin_xmm_reg
	F__fpu_k0        T__darwin_opmask_reg
	F__fpu_k1        T__darwin_opmask_reg
	F__fpu_k2        T__darwin_opmask_reg
	F__fpu_k3        T__darwin_opmask_reg
	F__fpu_k4        T__darwin_opmask_reg
	F__fpu_k5        T__darwin_opmask_reg
	F__fpu_k6        T__darwin_opmask_reg
	F__fpu_k7        T__darwin_opmask_reg
	F__fpu_zmmh0     T__darwin_ymm_reg
	F__fpu_zmmh1     T__darwin_ymm_reg
	F__fpu_zmmh2     T__darwin_ymm_reg
	F__fpu_zmmh3     T__darwin_ymm_reg
	F__fpu_zmmh4     T__darwin_ymm_reg
	F__fpu_zmmh5     T__darwin_ymm_reg
	F__fpu_zmmh6     T__darwin_ymm_reg
	F__fpu_zmmh7     T__darwin_ymm_reg
}

type T__darwin_i386_exception_state = struct {
	F__trapno     T__uint16_t
	F__cpu        T__uint16_t
	F__err        T__uint32_t
	F__faultvaddr T__uint32_t
}

type T__darwin_x86_debug_state32 = struct {
	F__dr0 uint32
	F__dr1 uint32
	F__dr2 uint32
	F__dr3 uint32
	F__dr4 uint32
	F__dr5 uint32
	F__dr6 uint32
	F__dr7 uint32
}

type T__x86_instruction_state = struct {
	F__insn_stream_valid_bytes int32
	F__insn_offset             int32
	F__out_of_synch            int32
	F__insn_bytes              [2380]T__uint8_t
	F__insn_cacheline          [64]T__uint8_t
}

type T__last_branch_record = struct {
	F__from_ip T__uint64_t
	F__to_ip   T__uint64_t
	F__ccgo16  uint32
}

type T__last_branch_state = struct {
	F__lbr_count int32
	F__ccgo4     uint32
	F__lbrs      [32]T__last_branch_record
}

type T__x86_pagein_state = struct {
	F__pagein_error int32
}

type T__darwin_x86_thread_state64 = struct {
	F__rax    T__uint64_t
	F__rbx    T__uint64_t
	F__rcx    T__uint64_t
	F__rdx    T__uint64_t
	F__rdi    T__uint64_t
	F__rsi    T__uint64_t
	F__rbp    T__uint64_t
	F__rsp    T__uint64_t
	F__r8     T__uint64_t
	F__r9     T__uint64_t
	F__r10    T__uint64_t
	F__r11    T__uint64_t
	F__r12    T__uint64_t
	F__r13    T__uint64_t
	F__r14    T__uint64_t
	F__r15    T__uint64_t
	F__rip    T__uint64_t
	F__rflags T__uint64_t
	F__cs     T__uint64_t
	F__fs     T__uint64_t
	F__gs     T__uint64_t
}

type T__darwin_x86_thread_full_state64 = struct {
	F__ss64   T__darwin_x86_thread_state64
	F__ds     T__uint64_t
	F__es     T__uint64_t
	F__ss     T__uint64_t
	F__gsbase T__uint64_t
}

type T__darwin_x86_float_state64 = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       T__darwin_fp_control
	F__fpu_fsw       T__darwin_fp_status
	F__fpu_ftw       T__uint8_t
	F__fpu_rsrv1     T__uint8_t
	F__fpu_fop       T__uint16_t
	F__fpu_ip        T__uint32_t
	F__fpu_cs        T__uint16_t
	F__fpu_rsrv2     T__uint16_t
	F__fpu_dp        T__uint32_t
	F__fpu_ds        T__uint16_t
	F__fpu_rsrv3     T__uint16_t
	F__fpu_mxcsr     T__uint32_t
	F__fpu_mxcsrmask T__uint32_t
	F__fpu_stmm0     T__darwin_mmst_reg
	F__fpu_stmm1     T__darwin_mmst_reg
	F__fpu_stmm2     T__darwin_mmst_reg
	F__fpu_stmm3     T__darwin_mmst_reg
	F__fpu_stmm4     T__darwin_mmst_reg
	F__fpu_stmm5     T__darwin_mmst_reg
	F__fpu_stmm6     T__darwin_mmst_reg
	F__fpu_stmm7     T__darwin_mmst_reg
	F__fpu_xmm0      T__darwin_xmm_reg
	F__fpu_xmm1      T__darwin_xmm_reg
	F__fpu_xmm2      T__darwin_xmm_reg
	F__fpu_xmm3      T__darwin_xmm_reg
	F__fpu_xmm4      T__darwin_xmm_reg
	F__fpu_xmm5      T__darwin_xmm_reg
	F__fpu_xmm6      T__darwin_xmm_reg
	F__fpu_xmm7      T__darwin_xmm_reg
	F__fpu_xmm8      T__darwin_xmm_reg
	F__fpu_xmm9      T__darwin_xmm_reg
	F__fpu_xmm10     T__darwin_xmm_reg
	F__fpu_xmm11     T__darwin_xmm_reg
	F__fpu_xmm12     T__darwin_xmm_reg
	F__fpu_xmm13     T__darwin_xmm_reg
	F__fpu_xmm14     T__darwin_xmm_reg
	F__fpu_xmm15     T__darwin_xmm_reg
	F__fpu_rsrv4     [96]int8
	F__fpu_reserved1 int32
}

type T__darwin_x86_avx_state64 = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       T__darwin_fp_control
	F__fpu_fsw       T__darwin_fp_status
	F__fpu_ftw       T__uint8_t
	F__fpu_rsrv1     T__uint8_t
	F__fpu_fop       T__uint16_t
	F__fpu_ip        T__uint32_t
	F__fpu_cs        T__uint16_t
	F__fpu_rsrv2     T__uint16_t
	F__fpu_dp        T__uint32_t
	F__fpu_ds        T__uint16_t
	F__fpu_rsrv3     T__uint16_t
	F__fpu_mxcsr     T__uint32_t
	F__fpu_mxcsrmask T__uint32_t
	F__fpu_stmm0     T__darwin_mmst_reg
	F__fpu_stmm1     T__darwin_mmst_reg
	F__fpu_stmm2     T__darwin_mmst_reg
	F__fpu_stmm3     T__darwin_mmst_reg
	F__fpu_stmm4     T__darwin_mmst_reg
	F__fpu_stmm5     T__darwin_mmst_reg
	F__fpu_stmm6     T__darwin_mmst_reg
	F__fpu_stmm7     T__darwin_mmst_reg
	F__fpu_xmm0      T__darwin_xmm_reg
	F__fpu_xmm1      T__darwin_xmm_reg
	F__fpu_xmm2      T__darwin_xmm_reg
	F__fpu_xmm3      T__darwin_xmm_reg
	F__fpu_xmm4      T__darwin_xmm_reg
	F__fpu_xmm5      T__darwin_xmm_reg
	F__fpu_xmm6      T__darwin_xmm_reg
	F__fpu_xmm7      T__darwin_xmm_reg
	F__fpu_xmm8      T__darwin_xmm_reg
	F__fpu_xmm9      T__darwin_xmm_reg
	F__fpu_xmm10     T__darwin_xmm_reg
	F__fpu_xmm11     T__darwin_xmm_reg
	F__fpu_xmm12     T__darwin_xmm_reg
	F__fpu_xmm13     T__darwin_xmm_reg
	F__fpu_xmm14     T__darwin_xmm_reg
	F__fpu_xmm15     T__darwin_xmm_reg
	F__fpu_rsrv4     [96]int8
	F__fpu_reserved1 int32
	F__avx_reserved1 [64]int8
	F__fpu_ymmh0     T__darwin_xmm_reg
	F__fpu_ymmh1     T__darwin_xmm_reg
	F__fpu_ymmh2     T__darwin_xmm_reg
	F__fpu_ymmh3     T__darwin_xmm_reg
	F__fpu_ymmh4     T__darwin_xmm_reg
	F__fpu_ymmh5     T__darwin_xmm_reg
	F__fpu_ymmh6     T__darwin_xmm_reg
	F__fpu_ymmh7     T__darwin_xmm_reg
	F__fpu_ymmh8     T__darwin_xmm_reg
	F__fpu_ymmh9     T__darwin_xmm_reg
	F__fpu_ymmh10    T__darwin_xmm_reg
	F__fpu_ymmh11    T__darwin_xmm_reg
	F__fpu_ymmh12    T__darwin_xmm_reg
	F__fpu_ymmh13    T__darwin_xmm_reg
	F__fpu_ymmh14    T__darwin_xmm_reg
	F__fpu_ymmh15    T__darwin_xmm_reg
}

type T__darwin_x86_avx512_state64 = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       T__darwin_fp_control
	F__fpu_fsw       T__darwin_fp_status
	F__fpu_ftw       T__uint8_t
	F__fpu_rsrv1     T__uint8_t
	F__fpu_fop       T__uint16_t
	F__fpu_ip        T__uint32_t
	F__fpu_cs        T__uint16_t
	F__fpu_rsrv2     T__uint16_t
	F__fpu_dp        T__uint32_t
	F__fpu_ds        T__uint16_t
	F__fpu_rsrv3     T__uint16_t
	F__fpu_mxcsr     T__uint32_t
	F__fpu_mxcsrmask T__uint32_t
	F__fpu_stmm0     T__darwin_mmst_reg
	F__fpu_stmm1     T__darwin_mmst_reg
	F__fpu_stmm2     T__darwin_mmst_reg
	F__fpu_stmm3     T__darwin_mmst_reg
	F__fpu_stmm4     T__darwin_mmst_reg
	F__fpu_stmm5     T__darwin_mmst_reg
	F__fpu_stmm6     T__darwin_mmst_reg
	F__fpu_stmm7     T__darwin_mmst_reg
	F__fpu_xmm0      T__darwin_xmm_reg
	F__fpu_xmm1      T__darwin_xmm_reg
	F__fpu_xmm2      T__darwin_xmm_reg
	F__fpu_xmm3      T__darwin_xmm_reg
	F__fpu_xmm4      T__darwin_xmm_reg
	F__fpu_xmm5      T__darwin_xmm_reg
	F__fpu_xmm6      T__darwin_xmm_reg
	F__fpu_xmm7      T__darwin_xmm_reg
	F__fpu_xmm8      T__darwin_xmm_reg
	F__fpu_xmm9      T__darwin_xmm_reg
	F__fpu_xmm10     T__darwin_xmm_reg
	F__fpu_xmm11     T__darwin_xmm_reg
	F__fpu_xmm12     T__darwin_xmm_reg
	F__fpu_xmm13     T__darwin_xmm_reg
	F__fpu_xmm14     T__darwin_xmm_reg
	F__fpu_xmm15     T__darwin_xmm_reg
	F__fpu_rsrv4     [96]int8
	F__fpu_reserved1 int32
	F__avx_reserved1 [64]int8
	F__fpu_ymmh0     T__darwin_xmm_reg
	F__fpu_ymmh1     T__darwin_xmm_reg
	F__fpu_ymmh2     T__darwin_xmm_reg
	F__fpu_ymmh3     T__darwin_xmm_reg
	F__fpu_ymmh4     T__darwin_xmm_reg
	F__fpu_ymmh5     T__darwin_xmm_reg
	F__fpu_ymmh6     T__darwin_xmm_reg
	F__fpu_ymmh7     T__darwin_xmm_reg
	F__fpu_ymmh8     T__darwin_xmm_reg
	F__fpu_ymmh9     T__darwin_xmm_reg
	F__fpu_ymmh10    T__darwin_xmm_reg
	F__fpu_ymmh11    T__darwin_xmm_reg
	F__fpu_ymmh12    T__darwin_xmm_reg
	F__fpu_ymmh13    T__darwin_xmm_reg
	F__fpu_ymmh14    T__darwin_xmm_reg
	F__fpu_ymmh15    T__darwin_xmm_reg
	F__fpu_k0        T__darwin_opmask_reg
	F__fpu_k1        T__darwin_opmask_reg
	F__fpu_k2        T__darwin_opmask_reg
	F__fpu_k3        T__darwin_opmask_reg
	F__fpu_k4        T__darwin_opmask_reg
	F__fpu_k5        T__darwin_opmask_reg
	F__fpu_k6        T__darwin_opmask_reg
	F__fpu_k7        T__darwin_opmask_reg
	F__fpu_zmmh0     T__darwin_ymm_reg
	F__fpu_zmmh1     T__darwin_ymm_reg
	F__fpu_zmmh2     T__darwin_ymm_reg
	F__fpu_zmmh3     T__darwin_ymm_reg
	F__fpu_zmmh4     T__darwin_ymm_reg
	F__fpu_zmmh5     T__darwin_ymm_reg
	F__fpu_zmmh6     T__darwin_ymm_reg
	F__fpu_zmmh7     T__darwin_ymm_reg
	F__fpu_zmmh8     T__darwin_ymm_reg
	F__fpu_zmmh9     T__darwin_ymm_reg
	F__fpu_zmmh10    T__darwin_ymm_reg
	F__fpu_zmmh11    T__darwin_ymm_reg
	F__fpu_zmmh12    T__darwin_ymm_reg
	F__fpu_zmmh13    T__darwin_ymm_reg
	F__fpu_zmmh14    T__darwin_ymm_reg
	F__fpu_zmmh15    T__darwin_ymm_reg
	F__fpu_zmm16     T__darwin_zmm_reg
	F__fpu_zmm17     T__darwin_zmm_reg
	F__fpu_zmm18     T__darwin_zmm_reg
	F__fpu_zmm19     T__darwin_zmm_reg
	F__fpu_zmm20     T__darwin_zmm_reg
	F__fpu_zmm21     T__darwin_zmm_reg
	F__fpu_zmm22     T__darwin_zmm_reg
	F__fpu_zmm23     T__darwin_zmm_reg
	F__fpu_zmm24     T__darwin_zmm_reg
	F__fpu_zmm25     T__darwin_zmm_reg
	F__fpu_zmm26     T__darwin_zmm_reg
	F__fpu_zmm27     T__darwin_zmm_reg
	F__fpu_zmm28     T__darwin_zmm_reg
	F__fpu_zmm29     T__darwin_zmm_reg
	F__fpu_zmm30     T__darwin_zmm_reg
	F__fpu_zmm31     T__darwin_zmm_reg
}

type T__darwin_x86_exception_state64 = struct {
	F__trapno     T__uint16_t
	F__cpu        T__uint16_t
	F__err        T__uint32_t
	F__faultvaddr T__uint64_t
}

type T__darwin_x86_debug_state64 = struct {
	F__dr0 T__uint64_t
	F__dr1 T__uint64_t
	F__dr2 T__uint64_t
	F__dr3 T__uint64_t
	F__dr4 T__uint64_t
	F__dr5 T__uint64_t
	F__dr6 T__uint64_t
	F__dr7 T__uint64_t
}

type T__darwin_x86_cpmu_state64 = struct {
	F__ctrs [16]T__uint64_t
}

type T__darwin_mcontext32 = struct {
	F__es T__darwin_i386_exception_state
	F__ss T__darwin_i386_thread_state
	F__fs T__darwin_i386_float_state
}

type T__darwin_mcontext_avx32 = struct {
	F__es T__darwin_i386_exception_state
	F__ss T__darwin_i386_thread_state
	F__fs T__darwin_i386_avx_state
}

type T__darwin_mcontext_avx512_32 = struct {
	F__es T__darwin_i386_exception_state
	F__ss T__darwin_i386_thread_state
	F__fs T__darwin_i386_avx512_state
}

type T__darwin_mcontext64 = struct {
	F__es T__darwin_x86_exception_state64
	F__ss T__darwin_x86_thread_state64
	F__fs T__darwin_x86_float_state64
}

type T__darwin_mcontext64_full = struct {
	F__es T__darwin_x86_exception_state64
	F__ss T__darwin_x86_thread_full_state64
	F__fs T__darwin_x86_float_state64
}

type T__darwin_mcontext_avx64 = struct {
	F__es T__darwin_x86_exception_state64
	F__ss T__darwin_x86_thread_state64
	F__fs T__darwin_x86_avx_state64
}

type T__darwin_mcontext_avx64_full = struct {
	F__es T__darwin_x86_exception_state64
	F__ss T__darwin_x86_thread_full_state64
	F__fs T__darwin_x86_avx_state64
}

type T__darwin_mcontext_avx512_64 = struct {
	F__es T__darwin_x86_exception_state64
	F__ss T__darwin_x86_thread_state64
	F__fs T__darwin_x86_avx512_state64
}

type T__darwin_mcontext_avx512_64_full = struct {
	F__es T__darwin_x86_exception_state64
	F__ss T__darwin_x86_thread_full_state64
	F__fs T__darwin_x86_avx512_state64
}

type Tmcontext_t = uintptr

type Tpthread_attr_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type T__darwin_sigaltstack = struct {
	Fss_sp    uintptr
	Fss_size  T__darwin_size_t
	Fss_flags int32
}

type Tstack_t = struct {
	Fss_sp    uintptr
	Fss_size  T__darwin_size_t
	Fss_flags int32
}

type T__darwin_ucontext = struct {
	Fuc_onstack  int32
	Fuc_sigmask  T__darwin_sigset_t
	Fuc_stack    T__darwin_sigaltstack
	Fuc_link     uintptr
	Fuc_mcsize   T__darwin_size_t
	Fuc_mcontext uintptr
}

type Tucontext_t = struct {
	Fuc_onstack  int32
	Fuc_sigmask  T__darwin_sigset_t
	Fuc_stack    T__darwin_sigaltstack
	Fuc_link     uintptr
	Fuc_mcsize   T__darwin_size_t
	Fuc_mcontext uintptr
}

type Tsigset_t = uint32

type Tuid_t = uint32

type Tsigval = struct {
	Fsival_ptr   [0]uintptr
	Fsival_int   int32
	F__ccgo_pad2 [4]byte
}

type Tsigevent = struct {
	Fsigev_notify            int32
	Fsigev_signo             int32
	Fsigev_value             Tsigval
	Fsigev_notify_function   uintptr
	Fsigev_notify_attributes uintptr
}

type Tsiginfo_t = struct {
	Fsi_signo  int32
	Fsi_errno  int32
	Fsi_code   int32
	Fsi_pid    Tpid_t
	Fsi_uid    Tuid_t
	Fsi_status int32
	Fsi_addr   uintptr
	Fsi_value  Tsigval
	Fsi_band   int64
	F__pad     [7]uint64
}

type T__siginfo = Tsiginfo_t

type T__sigaction_u = struct {
	F__sa_sigaction [0]uintptr
	F__sa_handler   uintptr
}

type T__sigaction = struct {
	F__sigaction_u T__sigaction_u
	Fsa_tramp      uintptr
	Fsa_mask       Tsigset_t
	Fsa_flags      int32
}

type Tsigaction = struct {
	F__sigaction_u T__sigaction_u
	Fsa_mask       Tsigset_t
	Fsa_flags      int32
}

type Tsig_t = uintptr

type Tsigvec = struct {
	Fsv_handler uintptr
	Fsv_mask    int32
	Fsv_flags   int32
}

type Tsigstack = struct {
	Fss_sp      uintptr
	Fss_onstack int32
}

type Tuint64_t = uint64

type Tint_least64_t = int64

type Tuint_least64_t = uint64

type Tint_fast64_t = int64

type Tuint_fast64_t = uint64

type Tuint32_t = uint32

type Tint_least32_t = int32

type Tuint_least32_t = uint32

type Tint_fast32_t = int32

type Tuint_fast32_t = uint32

type Tuint16_t = uint16

type Tint_least16_t = int16

type Tuint_least16_t = uint16

type Tint_fast16_t = int16

type Tuint_fast16_t = uint16

type Tuint8_t = uint8

type Tint_least8_t = int8

type Tuint_least8_t = uint8

type Tint_fast8_t = int8

type Tuint_fast8_t = uint8

type Tintmax_t = int64

type Tuintmax_t = uint64

type Ttimeval = struct {
	Ftv_sec  T__darwin_time_t
	Ftv_usec T__darwin_suseconds_t
}

type Trlim_t = uint64

type Trusage = struct {
	Fru_utime    Ttimeval
	Fru_stime    Ttimeval
	Fru_maxrss   int64
	Fru_ixrss    int64
	Fru_idrss    int64
	Fru_isrss    int64
	Fru_minflt   int64
	Fru_majflt   int64
	Fru_nswap    int64
	Fru_inblock  int64
	Fru_oublock  int64
	Fru_msgsnd   int64
	Fru_msgrcv   int64
	Fru_nsignals int64
	Fru_nvcsw    int64
	Fru_nivcsw   int64
}

type Trusage_info_t = uintptr

type Trusage_info_v0 = struct {
	Fri_uuid               [16]Tuint8_t
	Fri_user_time          Tuint64_t
	Fri_system_time        Tuint64_t
	Fri_pkg_idle_wkups     Tuint64_t
	Fri_interrupt_wkups    Tuint64_t
	Fri_pageins            Tuint64_t
	Fri_wired_size         Tuint64_t
	Fri_resident_size      Tuint64_t
	Fri_phys_footprint     Tuint64_t
	Fri_proc_start_abstime Tuint64_t
	Fri_proc_exit_abstime  Tuint64_t
}

type Trusage_info_v1 = struct {
	Fri_uuid                  [16]Tuint8_t
	Fri_user_time             Tuint64_t
	Fri_system_time           Tuint64_t
	Fri_pkg_idle_wkups        Tuint64_t
	Fri_interrupt_wkups       Tuint64_t
	Fri_pageins               Tuint64_t
	Fri_wired_size            Tuint64_t
	Fri_resident_size         Tuint64_t
	Fri_phys_footprint        Tuint64_t
	Fri_proc_start_abstime    Tuint64_t
	Fri_proc_exit_abstime     Tuint64_t
	Fri_child_user_time       Tuint64_t
	Fri_child_system_time     Tuint64_t
	Fri_child_pkg_idle_wkups  Tuint64_t
	Fri_child_interrupt_wkups Tuint64_t
	Fri_child_pageins         Tuint64_t
	Fri_child_elapsed_abstime Tuint64_t
}

type Trusage_info_v2 = struct {
	Fri_uuid                  [16]Tuint8_t
	Fri_user_time             Tuint64_t
	Fri_system_time           Tuint64_t
	Fri_pkg_idle_wkups        Tuint64_t
	Fri_interrupt_wkups       Tuint64_t
	Fri_pageins               Tuint64_t
	Fri_wired_size            Tuint64_t
	Fri_resident_size         Tuint64_t
	Fri_phys_footprint        Tuint64_t
	Fri_proc_start_abstime    Tuint64_t
	Fri_proc_exit_abstime     Tuint64_t
	Fri_child_user_time       Tuint64_t
	Fri_child_system_time     Tuint64_t
	Fri_child_pkg_idle_wkups  Tuint64_t
	Fri_child_interrupt_wkups Tuint64_t
	Fri_child_pageins         Tuint64_t
	Fri_child_elapsed_abstime Tuint64_t
	Fri_diskio_bytesread      Tuint64_t
	Fri_diskio_byteswritten   Tuint64_t
}

type Trusage_info_v3 = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
}

type Trusage_info_v4 = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
	Fri_logical_writes                Tuint64_t
	Fri_lifetime_max_phys_footprint   Tuint64_t
	Fri_instructions                  Tuint64_t
	Fri_cycles                        Tuint64_t
	Fri_billed_energy                 Tuint64_t
	Fri_serviced_energy               Tuint64_t
	Fri_interval_max_phys_footprint   Tuint64_t
	Fri_runnable_time                 Tuint64_t
}

type Trusage_info_v5 = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
	Fri_logical_writes                Tuint64_t
	Fri_lifetime_max_phys_footprint   Tuint64_t
	Fri_instructions                  Tuint64_t
	Fri_cycles                        Tuint64_t
	Fri_billed_energy                 Tuint64_t
	Fri_serviced_energy               Tuint64_t
	Fri_interval_max_phys_footprint   Tuint64_t
	Fri_runnable_time                 Tuint64_t
	Fri_flags                         Tuint64_t
}

type Trusage_info_v6 = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
	Fri_logical_writes                Tuint64_t
	Fri_lifetime_max_phys_footprint   Tuint64_t
	Fri_instructions                  Tuint64_t
	Fri_cycles                        Tuint64_t
	Fri_billed_energy                 Tuint64_t
	Fri_serviced_energy               Tuint64_t
	Fri_interval_max_phys_footprint   Tuint64_t
	Fri_runnable_time                 Tuint64_t
	Fri_flags                         Tuint64_t
	Fri_user_ptime                    Tuint64_t
	Fri_system_ptime                  Tuint64_t
	Fri_pinstructions                 Tuint64_t
	Fri_pcycles                       Tuint64_t
	Fri_energy_nj                     Tuint64_t
	Fri_penergy_nj                    Tuint64_t
	Fri_reserved                      [14]Tuint64_t
}

type Trusage_info_current = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
	Fri_logical_writes                Tuint64_t
	Fri_lifetime_max_phys_footprint   Tuint64_t
	Fri_instructions                  Tuint64_t
	Fri_cycles                        Tuint64_t
	Fri_billed_energy                 Tuint64_t
	Fri_serviced_energy               Tuint64_t
	Fri_interval_max_phys_footprint   Tuint64_t
	Fri_runnable_time                 Tuint64_t
	Fri_flags                         Tuint64_t
	Fri_user_ptime                    Tuint64_t
	Fri_system_ptime                  Tuint64_t
	Fri_pinstructions                 Tuint64_t
	Fri_pcycles                       Tuint64_t
	Fri_energy_nj                     Tuint64_t
	Fri_penergy_nj                    Tuint64_t
	Fri_reserved                      [14]Tuint64_t
}

type Trlimit = struct {
	Frlim_cur Trlim_t
	Frlim_max Trlim_t
}

type Tproc_rlimit_control_wakeupmon = struct {
	Fwm_flags Tuint32_t
	Fwm_rate  Tint32_t
}

type Twait = struct {
	Fw_T [0]struct {
		F__ccgo0 uint32
	}
	Fw_S [0]struct {
		F__ccgo0 uint32
	}
	Fw_status int32
}

type Tct_rune_t = int32

type Trune_t = int32

type Twchar_t = int32

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tmalloc_type_id_t = uint64

type Tdev_t = int32

type Tmode_t = uint16

type Trsize_t = uint64

type Terrno_t = int32

type Tclock_t = uint64

type Ttime_t = int64

type Ttimespec = struct {
	Ftv_sec  T__darwin_time_t
	Ftv_nsec int64
}

type Ttm = struct {
	Ftm_sec    int32
	Ftm_min    int32
	Ftm_hour   int32
	Ftm_mday   int32
	Ftm_mon    int32
	Ftm_year   int32
	Ftm_wday   int32
	Ftm_yday   int32
	Ftm_isdst  int32
	Ftm_gmtoff int64
	Ftm_zone   uintptr
} //TODO "timezone" // time.h:103:13:

type Tclockid_t = int32

const _CLOCK_REALTIME = 0
const _CLOCK_MONOTONIC = 6
const _CLOCK_MONOTONIC_RAW = 4
const _CLOCK_MONOTONIC_RAW_APPROX = 5
const _CLOCK_UPTIME_RAW = 8
const _CLOCK_UPTIME_RAW_APPROX = 9
const _CLOCK_PROCESS_CPUTIME_ID = 12
const _CLOCK_THREAD_CPUTIME_ID = 16

type Taccessx_descriptor = struct {
	Fad_name_offset uint32
	Fad_flags       int32
	Fad_pad         [2]int32
}

type Tgid_t = uint32

type Tuseconds_t = uint32

type Tfd_set = struct {
	Ffds_bits [32]T__int32_t
}

type Tsuseconds_t = int32

type Tuuid_t = [16]uint8

type Tfloat_t = float32

type Tdouble_t = float64

type T__float2 = struct {
	F__sinval float32
	F__cosval float32
}

type T__double2 = struct {
	F__sinval float64
	F__cosval float64
}

type Texception = struct {
	Ftype1  int32
	Fname   uintptr
	Farg1   float64
	Farg2   float64
	Fretval float64
}

type TSRC_DATA = struct {
	Fdata_in           uintptr
	Fdata_out          uintptr
	Finput_frames      int64
	Foutput_frames     int64
	Finput_frames_used int64
	Foutput_frames_gen int64
	Fend_of_input      int32
	Fsrc_ratio         float64
}

type Tsrc_callback_t = uintptr

const SRC_SINC_BEST_QUALITY = 0
const SRC_SINC_MEDIUM_QUALITY = 1
const SRC_SINC_FASTEST = 2
const SRC_ZERO_ORDER_HOLD = 3
const SRC_LINEAR = 4

var _input [131072]float32

var _output [131072]float32

func _throughput_test(tls *libc.TLS, converter int32, channels int32, best_throughput uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var clock_time, start_time Tclock_t
	var duration float64
	var error1, v1 int32
	var throughput, total_frames, v2 int64
	var _ /* src_data at bp+0 */ TSRC_DATA
	_, _, _, _, _, _, _, _ = clock_time, duration, error1, start_time, throughput, total_frames, v1, v2
	total_frames = 0
	libc.Xprintf(tls, __ccgo_ts, libc.VaList(bp+72, libsamplerate.Xsrc_get_name(tls, converter), channels))
	libc.Xfflush(tls, libc.X__stdoutp)
	(*(*TSRC_DATA)(unsafe.Pointer(bp))).Fdata_in = uintptr(unsafe.Pointer(&_input))
	(*(*TSRC_DATA)(unsafe.Pointer(bp))).Finput_frames = int64(int32(libc.Uint64FromInt64(524288)/libc.Uint64FromInt64(4)) / channels)
	(*(*TSRC_DATA)(unsafe.Pointer(bp))).Fdata_out = uintptr(unsafe.Pointer(&_output))
	(*(*TSRC_DATA)(unsafe.Pointer(bp))).Foutput_frames = int64(int32(libc.Uint64FromInt64(524288)/libc.Uint64FromInt64(4)) / channels)
	(*(*TSRC_DATA)(unsafe.Pointer(bp))).Fsrc_ratio = float64(0.99)
	libc.Xsleep(tls, uint32(2))
	start_time = libc.Xclock(tls)
	for cond := true; cond; cond = duration < float64(5) {
		v1 = libsamplerate.Xsrc_simple(tls, bp, converter, channels)
		error1 = v1
		if v1 != 0 {
			libc.Xputs(tls, libsamplerate.Xsrc_strerror(tls, error1))
			libc.Xexit(tls, int32(1))
		}
		total_frames += (*(*TSRC_DATA)(unsafe.Pointer(bp))).Foutput_frames_gen
		clock_time = libc.Xclock(tls) - start_time
		duration = float64(1) * float64(clock_time) / float64(libc.Uint64FromInt32(1000000))
	}
	if (*(*TSRC_DATA)(unsafe.Pointer(bp))).Finput_frames_used != (*(*TSRC_DATA)(unsafe.Pointer(bp))).Finput_frames {
		libc.Xprintf(tls, __ccgo_ts+27, libc.VaList(bp+72, int32(83), (*(*TSRC_DATA)(unsafe.Pointer(bp))).Finput_frames_used, (*(*TSRC_DATA)(unsafe.Pointer(bp))).Finput_frames))
		libc.Xexit(tls, int32(1))
	}
	if libc.Xfabs(tls, (*(*TSRC_DATA)(unsafe.Pointer(bp))).Fsrc_ratio*float64((*(*TSRC_DATA)(unsafe.Pointer(bp))).Finput_frames_used)-float64((*(*TSRC_DATA)(unsafe.Pointer(bp))).Foutput_frames_gen)) > libc.Float64FromInt32(2) {
		libc.Xprintf(tls, __ccgo_ts+76, libc.VaList(bp+72, int32(88)))
		libc.Xprintf(tls, __ccgo_ts+122, libc.VaList(bp+72, int32(libc.Uint64FromInt64(524288)/libc.Uint64FromInt64(4))/channels))
		libc.Xprintf(tls, __ccgo_ts+143, libc.VaList(bp+72, (*(*TSRC_DATA)(unsafe.Pointer(bp))).Foutput_frames_gen, libc.Xfloor(tls, float64(0.5)+(*(*TSRC_DATA)(unsafe.Pointer(bp))).Fsrc_ratio*float64((*(*TSRC_DATA)(unsafe.Pointer(bp))).Finput_frames_used))))
		libc.Xexit(tls, int32(1))
	}
	throughput = libc.X__builtin_lrint(tls, libc.Xfloor(tls, float64(total_frames)/duration))
	if !(best_throughput != 0) {
		libc.Xprintf(tls, __ccgo_ts+187, libc.VaList(bp+72, duration, throughput))
	} else {
		if throughput > *(*int64)(unsafe.Pointer(best_throughput)) {
			v2 = throughput
		} else {
			v2 = *(*int64)(unsafe.Pointer(best_throughput))
		}
		*(*int64)(unsafe.Pointer(best_throughput)) = v2
		libc.Xprintf(tls, __ccgo_ts+205, libc.VaList(bp+72, duration, throughput, *(*int64)(unsafe.Pointer(best_throughput))))
	}
}

/* throughput_test */

func _single_run(tls *libc.TLS) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var k, max_channels int32
	_, _ = k, max_channels
	max_channels = int32(10)
	libc.Xprintf(tls, __ccgo_ts+235, libc.VaList(bp+8, x_get_cpu_name(tls)))
	libc.Xputs(tls, __ccgo_ts+255)
	k = int32(1)
	for {
		if !(k <= max_channels/int32(2)) {
			break
		}
		_throughput_test(tls, SRC_SINC_FASTEST, k, uintptr(0))
		goto _1
	_1:
		k++
	}
	libc.Xputs(tls, __ccgo_ts+404)
	k = int32(1)
	for {
		if !(k <= max_channels/int32(2)) {
			break
		}
		_throughput_test(tls, SRC_SINC_MEDIUM_QUALITY, k, uintptr(0))
		goto _2
	_2:
		k++
	}
	libc.Xputs(tls, __ccgo_ts+404)
	k = int32(1)
	for {
		if !(k <= max_channels) {
			break
		}
		_throughput_test(tls, SRC_SINC_BEST_QUALITY, k, uintptr(0))
		goto _3
	_3:
		k++
	}
	libc.Xputs(tls, __ccgo_ts+404)
	return
}

/* single_run */

func _multi_run(tls *libc.TLS, run_count int32) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var ch, i, k int32
	var channels [12]int32
	var _ /* sinc_best at bp+16 */ int64
	var _ /* sinc_fastest at bp+0 */ int64
	var _ /* sinc_medium at bp+8 */ int64
	_, _, _, _ = ch, channels, i, k
	channels = [12]int32{
		0:  int32(1),
		1:  int32(2),
		2:  int32(3),
		3:  int32(4),
		4:  int32(5),
		5:  int32(6),
		6:  int32(8),
		7:  int32(10),
		8:  int32(12),
		9:  int32(14),
		10: int32(16),
		11: int32(18),
	}
	libc.Xprintf(tls, __ccgo_ts+235, libc.VaList(bp+32, x_get_cpu_name(tls)))
	libc.Xputs(tls, __ccgo_ts+405)
	i = 0
	for {
		if !(i < int32(libc.Uint64FromInt64(48)/libc.Uint64FromInt64(4))) {
			break
		}
		*(*int64)(unsafe.Pointer(bp)) = 0
		*(*int64)(unsafe.Pointer(bp + 8)) = 0
		*(*int64)(unsafe.Pointer(bp + 16)) = 0
		ch = channels[i]
		k = 0
		for {
			if !(k < run_count) {
				break
			}
			_throughput_test(tls, SRC_SINC_FASTEST, ch, bp)
			_throughput_test(tls, SRC_SINC_MEDIUM_QUALITY, ch, bp+8)
			_throughput_test(tls, SRC_SINC_BEST_QUALITY, ch, bp+16)
			libc.Xputs(tls, __ccgo_ts+404)
			/* Let the CPU cool down. We might be running on a laptop. */
			libc.Xsleep(tls, uint32(10))
			goto _2
		_2:
			k++
		}
		libc.Xprintf(tls, __ccgo_ts+592, libc.VaList(bp+32, ch))
		libc.Xprintf(tls, __ccgo_ts+700, libc.VaList(bp+32, libsamplerate.Xsrc_get_name(tls, SRC_SINC_FASTEST), *(*int64)(unsafe.Pointer(bp))))
		libc.Xprintf(tls, __ccgo_ts+700, libc.VaList(bp+32, libsamplerate.Xsrc_get_name(tls, SRC_SINC_MEDIUM_QUALITY), *(*int64)(unsafe.Pointer(bp + 8))))
		libc.Xprintf(tls, __ccgo_ts+700, libc.VaList(bp+32, libsamplerate.Xsrc_get_name(tls, SRC_SINC_BEST_QUALITY), *(*int64)(unsafe.Pointer(bp + 16))))
		goto _1
	_1:
		i++
	}
	libc.Xputs(tls, __ccgo_ts+404)
}

/* multi_run */

func _usage_exit(tls *libc.TLS, argv0 uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var cptr, v1 uintptr
	_, _ = cptr, v1
	v1 = libc.Xstrrchr(tls, argv0, int32('/'))
	cptr = v1
	if v1 != libc.UintptrFromInt32(0) {
		argv0 = cptr
	}
	libc.Xprintf(tls, __ccgo_ts+720, libc.VaList(bp+8, argv0, argv0))
	libc.Xexit(tls, 0)
}

/* usage_exit */

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var run_count int32
	var _ /* freq at bp+0 */ float64
	_ = run_count
	libc.X__builtin___memset_chk(tls, uintptr(unsafe.Pointer(&_input)), 0, uint64(524288), libc.X__builtin_object_size(tls, uintptr(unsafe.Pointer(&_input)), 0))
	*(*float64)(unsafe.Pointer(bp)) = float64(0.01)
	x_gen_windowed_sines(tls, int32(1), bp, float64(1), uintptr(unsafe.Pointer(&_input)), libc.Int32FromInt32(1)<<libc.Int32FromInt32(17))
	if argc == int32(1) {
		_single_run(tls)
	} else {
		if argc == int32(3) && libc.Xstrcmp(tls, *(*uintptr)(unsafe.Pointer(argv + 1*8)), __ccgo_ts+854) == 0 {
			run_count = libc.Xatoi(tls, *(*uintptr)(unsafe.Pointer(argv + 2*8)))
			if run_count < int32(1) || run_count > int32(20) {
				libc.Xprintf(tls, __ccgo_ts+864, 0)
				libc.Xexit(tls, int32(1))
			}
			_multi_run(tls, run_count)
		} else {
			_usage_exit(tls, *(*uintptr)(unsafe.Pointer(argv)))
		}
	}
	libc.Xputs(tls, __ccgo_ts+923)
	return 0
}

/* main */

func main() {
	libc.Start(x_main)
}

const m_M_PI1 = 3.141592653589793
const m__CTYPE_A = 256
const m__CTYPE_B = 131072
const m__CTYPE_C = 512
const m__CTYPE_D = 1024
const m__CTYPE_G = 2048
const m__CTYPE_I = 524288
const m__CTYPE_L = 4096
const m__CTYPE_P = 8192
const m__CTYPE_Q = 2097152
const m__CTYPE_R = 262144
const m__CTYPE_S = 16384
const m__CTYPE_SW0 = 0x20000000
const m__CTYPE_SW1 = 0x40000000
const m__CTYPE_SW2 = 0x80000000
const m__CTYPE_SW3 = 0xc0000000
const m__CTYPE_SWM = 3758096384
const m__CTYPE_SWS = 30
const m__CTYPE_T = 1048576
const m__CTYPE_U = 32768
const m__CTYPE_X = 65536
const m__RUNE_MAGIC_A = "RuneMagA"
const m___DARWIN_CTYPE_TOP_inline = "__header_inline"
const m___DARWIN_CTYPE_inline = "__header_inline"

type Twint_t = int32

type T_RuneEntry = struct {
	F__min   T__darwin_rune_t
	F__max   T__darwin_rune_t
	F__map   T__darwin_rune_t
	F__types uintptr
}

type T_RuneRange = struct {
	F__nranges int32
	F__ranges  uintptr
}

type T_RuneCharClass = struct {
	F__name [14]int8
	F__mask T__uint32_t
}

type T_RuneLocale = struct {
	F__magic        [8]int8
	F__encoding     [32]int8
	F__sgetrune     uintptr
	F__sputrune     uintptr
	F__invalid_rune T__darwin_rune_t
	F__runetype     [256]T__uint32_t
	F__maplower     [256]T__darwin_rune_t
	F__mapupper     [256]T__darwin_rune_t
	F__runetype_ext T_RuneRange
	F__maplower_ext T_RuneRange
	F__mapupper_ext T_RuneRange
	F__variable     uintptr
	F__variable_len int32
	F__ncharclasses int32
	F__charclasses  uintptr
}

func x_gen_windowed_sines(tls *libc.TLS, freq_count int32, freqs uintptr, max float64, output uintptr, output_len int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var amplitude, phase float64
	var freq, k int32
	_, _, _, _ = amplitude, freq, k, phase
	amplitude = max / float64(freq_count)
	k = 0
	for {
		if !(k < output_len) {
			break
		}
		*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(0)
		goto _1
	_1:
		k++
	}
	freq = 0
	for {
		if !(freq < freq_count) {
			break
		}
		phase = libc.Float64FromFloat64(0.9) * libc.Float64FromFloat64(3.141592653589793) / float64(freq_count)
		if *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8)) <= float64(0) || *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8)) >= float64(0.5) {
			libc.Xprintf(tls, __ccgo_ts+1018, libc.VaList(bp+8, __ccgo_ts+1083, freq, *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8))))
			libc.Xexit(tls, int32(1))
		}
		k = 0
		for {
			if !(k < output_len) {
				break
			}
			*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4))) + amplitude*libc.Xsin(tls, *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8))*float64(libc.Int32FromInt32(2)*k)*float64(3.141592653589793)+phase))
			goto _3
		_3:
			k++
		}
		goto _2
	_2:
		freq++
	}
	/* Apply Hanning Window. */
	k = 0
	for {
		if !(k < output_len) {
			break
		}
		*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4))) * (libc.Float64FromFloat64(0.5) - libc.Float64FromFloat64(0.5)*libc.Xcos(tls, float64(libc.Int32FromInt32(2)*k)*float64(3.141592653589793)/float64(output_len-libc.Int32FromInt32(1)))))
		goto _4
	_4:
		k++
	}
	/*	data [k] *= 0.3635819 - 0.4891775 * cos ((2 * k) * M_PI / (output_len - 1))
		+ 0.1365995 * cos ((4 * k) * M_PI / (output_len - 1))
		- 0.0106411 * cos ((6 * k) * M_PI / (output_len - 1)) ;
	*/
	return
}

/* gen_windowed_sines */

func x_save_oct_float(tls *libc.TLS, filename uintptr, input uintptr, in_len int32, output uintptr, out_len int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var file, v1 uintptr
	var k int32
	_, _, _ = file, k, v1
	libc.Xprintf(tls, __ccgo_ts+1096, libc.VaList(bp+8, filename))
	v1 = libc.Xfopen(tls, filename, __ccgo_ts+1142)
	file = v1
	if !(v1 != 0) {
		return
	}
	libc.Xfprintf(tls, file, __ccgo_ts+1144, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1169, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1184, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1200, libc.VaList(bp+8, in_len))
	libc.Xfprintf(tls, file, __ccgo_ts+1212, 0)
	k = 0
	for {
		if !(k < in_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+1226, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(input + uintptr(k)*4)))))
		goto _2
	_2:
		k++
	}
	libc.Xfprintf(tls, file, __ccgo_ts+1231, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1184, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1200, libc.VaList(bp+8, out_len))
	libc.Xfprintf(tls, file, __ccgo_ts+1212, 0)
	k = 0
	for {
		if !(k < out_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+1226, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4)))))
		goto _3
	_3:
		k++
	}
	libc.Xfclose(tls, file)
	return
}

/* save_oct_float */

func x_save_oct_double(tls *libc.TLS, filename uintptr, input uintptr, in_len int32, output uintptr, out_len int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var file, v1 uintptr
	var k int32
	_, _, _ = file, k, v1
	libc.Xprintf(tls, __ccgo_ts+1096, libc.VaList(bp+8, filename))
	v1 = libc.Xfopen(tls, filename, __ccgo_ts+1142)
	file = v1
	if !(v1 != 0) {
		return
	}
	libc.Xfprintf(tls, file, __ccgo_ts+1144, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1169, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1184, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1200, libc.VaList(bp+8, in_len))
	libc.Xfprintf(tls, file, __ccgo_ts+1212, 0)
	k = 0
	for {
		if !(k < in_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+1226, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(input + uintptr(k)*8))))
		goto _2
	_2:
		k++
	}
	libc.Xfprintf(tls, file, __ccgo_ts+1231, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1184, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+1200, libc.VaList(bp+8, out_len))
	libc.Xfprintf(tls, file, __ccgo_ts+1212, 0)
	k = 0
	for {
		if !(k < out_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+1226, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(output + uintptr(k)*8))))
		goto _3
	_3:
		k++
	}
	libc.Xfclose(tls, file)
	return
}

/* save_oct_double */

func x_interleave_data(tls *libc.TLS, in uintptr, out uintptr, frames int32, channels int32) {
	var ch, fr int32
	_, _ = ch, fr
	fr = 0
	for {
		if !(fr < frames) {
			break
		}
		ch = 0
		for {
			if !(ch < channels) {
				break
			}
			*(*float32)(unsafe.Pointer(out + uintptr(ch+channels*fr)*4)) = *(*float32)(unsafe.Pointer(in + uintptr(fr+frames*ch)*4))
			goto _2
		_2:
			ch++
		}
		goto _1
	_1:
		fr++
	}
	return
}

/* interleave_data */

func x_deinterleave_data(tls *libc.TLS, in uintptr, out uintptr, frames int32, channels int32) {
	var ch, fr int32
	_, _ = ch, fr
	ch = 0
	for {
		if !(ch < channels) {
			break
		}
		fr = 0
		for {
			if !(fr < frames) {
				break
			}
			*(*float32)(unsafe.Pointer(out + uintptr(fr+frames*ch)*4)) = *(*float32)(unsafe.Pointer(in + uintptr(ch+channels*fr)*4))
			goto _2
		_2:
			fr++
		}
		goto _1
	_1:
		ch++
	}
	return
}

/* deinterleave_data */

func x_reverse_data(tls *libc.TLS, data uintptr, datalen int32) {
	var left, right int32
	var temp float32
	_, _, _ = left, right, temp
	left = 0
	right = datalen - int32(1)
	for left < right {
		temp = *(*float32)(unsafe.Pointer(data + uintptr(left)*4))
		*(*float32)(unsafe.Pointer(data + uintptr(left)*4)) = *(*float32)(unsafe.Pointer(data + uintptr(right)*4))
		*(*float32)(unsafe.Pointer(data + uintptr(right)*4)) = temp
		left++
		right--
	}
}

/* reverse_data */

func x_get_cpu_name(tls *libc.TLS) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var dest, file, name, search, src, v1 uintptr
	var is_pipe, v12, v16, v18, v19, v2, v21, v25, v27, v28, v6, v8, v9 int32
	var v14, v23, v4 T__darwin_ct_rune_t
	var v15, v24, v5 uint64
	var v30 bool
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = dest, file, is_pipe, name, search, src, v1, v12, v14, v15, v16, v18, v19, v2, v21, v23, v24, v25, v27, v28, v30, v4, v5, v6, v8, v9
	name = __ccgo_ts + 1247
	search = libc.UintptrFromInt32(0)
	file = libc.UintptrFromInt32(0)
	is_pipe = 0
	file = libc.Xpopen(tls, __ccgo_ts+1255, __ccgo_ts+1318)
	search = __ccgo_ts + 1320
	is_pipe = int32(1)
	if search == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+1335, libc.VaList(bp+8, uintptr(unsafe.Pointer(&___func__3))))
		return name
	}
	for libc.Xfgets(tls, uintptr(unsafe.Pointer(&_buffer)), int32(512), file) != libc.UintptrFromInt32(0) {
		if libc.Xstrstr(tls, uintptr(unsafe.Pointer(&_buffer)), search) != 0 {
			v1 = libc.Xstrchr(tls, uintptr(unsafe.Pointer(&_buffer)), int32(':'))
			src = v1
			if v1 != libc.UintptrFromInt32(0) {
				src++
				for {
					v4 = int32(*(*int8)(unsafe.Pointer(src)))
					v5 = uint64(0x00004000)
					v9 = libc.BoolInt32(v4 & ^libc.Int32FromInt32(0x7F) == 0)
					goto _10
				_10:
					if v9 != 0 {
						v8 = libc.BoolInt32(!!(uint64(*(*T__uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(&libc.X_DefaultRuneLocale)) + 60 + uintptr(v4)*4)))&v5 != 0))
					} else {
						v8 = libc.BoolInt32(!!(libc.X__maskrune(tls, v4, v5) != 0))
					}
					v6 = v8
					goto _7
				_7:
					v2 = v6
					goto _3
				_3:
					if !(v2 != 0) {
						break
					}
					src++
				}
				name = src
				/* Remove consecutive spaces. */
				src++
				dest = src
				for {
					if !(*(*int8)(unsafe.Pointer(src)) != 0) {
						break
					}
					v14 = int32(*(*int8)(unsafe.Pointer(src)))
					v15 = uint64(0x00004000)
					v19 = libc.BoolInt32(v14 & ^libc.Int32FromInt32(0x7F) == 0)
					goto _20
				_20:
					if v19 != 0 {
						v18 = libc.BoolInt32(!!(uint64(*(*T__uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(&libc.X_DefaultRuneLocale)) + 60 + uintptr(v14)*4)))&v15 != 0))
					} else {
						v18 = libc.BoolInt32(!!(libc.X__maskrune(tls, v14, v15) != 0))
					}
					v16 = v18
					goto _17
				_17:
					v12 = v16
					goto _13
				_13:
					if v30 = v12 != 0; v30 {
						v23 = int32(*(*int8)(unsafe.Pointer(dest + uintptr(-libc.Int32FromInt32(1)))))
						v24 = uint64(0x00004000)
						v28 = libc.BoolInt32(v23 & ^libc.Int32FromInt32(0x7F) == 0)
						goto _29
					_29:
						if v28 != 0 {
							v27 = libc.BoolInt32(!!(uint64(*(*T__uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(&libc.X_DefaultRuneLocale)) + 60 + uintptr(v23)*4)))&v24 != 0))
						} else {
							v27 = libc.BoolInt32(!!(libc.X__maskrune(tls, v23, v24) != 0))
						}
						v25 = v27
						goto _26
					_26:
						v21 = v25
						goto _22
					_22:
					}
					if v30 && v21 != 0 {
						goto _11
					}
					*(*int8)(unsafe.Pointer(dest)) = *(*int8)(unsafe.Pointer(src))
					dest++
					goto _11
				_11:
					src++
				}
				*(*int8)(unsafe.Pointer(dest)) = 0
				break
			}
		}
	}
	if is_pipe != 0 {
		libc.Xpclose(tls, file)
	} else {
		libc.Xfclose(tls, file)
	}
	return name
}

var ___func__3 = [13]int8{'g', 'e', 't', '_', 'c', 'p', 'u', '_', 'n', 'a', 'm', 'e'}

var _buffer [512]int8

func x_calculate_snr(tls *libc.TLS, data uintptr, len1 int32, expected_peaks int32) (r float64) {
	var snr float64
	_ = snr
	snr = float64(200)
	data = data
	len1 = len1
	expected_peaks = expected_peaks
	return snr
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "    %-30s     %2d         \x00\n\nLine %d : input frames used %ld should be %ld\n\x00\n\nLine %d : input / output length mismatch.\n\n\x00    input len  : %d\n\x00    output len : %ld (should be %g +/- 2)\n\n\x00%5.2f      %10ld\n\x00%5.2f      %10ld       %10ld\n\x00\n    CPU name : %s\n\x00\n    Converter                        Channels    Duration      Throughput\n    ---------------------------------------------------------------------\x00\x00\n    Converter                        Channels    Duration      Throughput    Best Throughput\n    ----------------------------------------------------------------------------------------\x00\n    Converter (channels: %d)         Best Throughput\n    ------------------------------------------------\n\x00    %-30s    %10ld\n\x00Usage :\n    %s                 - Single run of the throughput test.\n    %s --best-of N     - Do N runs of test a print bext result.\n\n\x00--best-of\x00Please be sensible. Run count should be in range (1, 10].\n\x00            Duration is in seconds.\n            Throughput is in frames/sec (more is better).\n\x00\n%s : Error : freq [%d] == %g is out of range. Should be < 0.5.\n\x00tests/util.c\x00Dumping input and output data to file : %s.\n\n\x00w\x00# Not created by Octave\n\x00# name: input\n\x00# type: matrix\n\x00# rows: %d\n\x00# columns: 1\n\x00% g\n\x00# name: output\n\x00Unknown\x00/usr/sbin/system_profiler -detailLevel full SPHardwareDataType\x00r\x00Processor Name\x00Error : search is NULL in function %s.\n\x00"
