// Code generated for freebsd/amd64 by 'gcc --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -w -mlong-double-64 -o tests/.libs/varispeed_test.go tests/varispeed_test-varispeed_test.o.go tests/varispeed_test-util.o.go tests/varispeed_test-calc_snr.o.go -lm -lsamplerate', DO NOT EDIT.

//go:build freebsd && amd64
// +build freebsd,amd64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libsamplerate"
)

var (
	_ reflect.Type
	_ unsafe.Pointer
)

const m_BUFSIZ = 1024
const m_CPU_CLIPS_NEGATIVE = 0
const m_CPU_CLIPS_POSITIVE = 0
const m_CPU_IS_BIG_ENDIAN = 0
const m_CPU_IS_LITTLE_ENDIAN = 1
const m_ENABLE_SINC_BEST_CONVERTER = "yes"
const m_ENABLE_SINC_FAST_CONVERTER = "yes"
const m_ENABLE_SINC_MEDIUM_CONVERTER = "yes"
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_FILENAME_MAX = 1024
const m_FOPEN_MAX = 20
const m_FP_FAST_FMAF = 1
const m_FP_ILOGBNAN = "__INT_MAX"
const m_FP_INFINITE = 0x01
const m_FP_NAN = 0x02
const m_FP_NORMAL = 0x04
const m_FP_SUBNORMAL = 0x08
const m_FP_ZERO = 0x10
const m_HAVE_ALARM = 1
const m_HAVE_CALLOC = 1
const m_HAVE_CEIL = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_FLOOR = 1
const m_HAVE_FMOD = 1
const m_HAVE_FREE = 1
const m_HAVE_IMMINTRIN_H = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_LRINT = 1
const m_HAVE_LRINTF = 1
const m_HAVE_MALLOC = 1
const m_HAVE_MEMCPY = 1
const m_HAVE_MEMMOVE = 1
const m_HAVE_SIGALRM = 1
const m_HAVE_SIGNAL = 1
const m_HAVE_STDBOOL_H = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TIMES_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_VISIBILITY = 1
const m_HUGE = "MAXFLOAT"
const m_LT_OBJDIR = ".libs/"
const m_L_ctermid = 1024
const m_L_cuserid = 17
const m_L_tmpnam = 1024
const m_MATH_ERREXCEPT = 2
const m_MATH_ERRNO = 1
const m_M_1_PI = 0.31830988618379067154
const m_M_2_PI = 0.63661977236758134308
const m_M_2_SQRTPI = 1.12837916709551257390
const m_M_E = 2.7182818284590452354
const m_M_LN10 = 2.30258509299404568402
const m_M_LN2 = 0.69314718055994530942
const m_M_LOG10E = 0.43429448190325182765
const m_M_LOG2E = 1.4426950408889634074
const m_M_PI = 3.14159265358979323846
const m_M_PI_2 = 1.57079632679489661923
const m_M_PI_4 = 0.78539816339744830962
const m_M_SQRT1_2 = 0.70710678118654752440
const m_M_SQRT2 = 1.41421356237309504880
const m_PACKAGE = "libsamplerate"
const m_PACKAGE_BUGREPORT = "erikd@mega-nerd.com"
const m_PACKAGE_NAME = "libsamplerate"
const m_PACKAGE_STRING = "libsamplerate 0.2.2"
const m_PACKAGE_TARNAME = "libsamplerate"
const m_PACKAGE_URL = "https://github.com/libsndfile/libsamplerate/"
const m_PACKAGE_VERSION = "0.2.2"
const m_P_tmpdir = "/tmp/"
const m_RAND_MAX = 0x7fffffff
const m_SEEK_CUR = 1
const m_SEEK_END = 2
const m_SEEK_SET = 0
const m_SIZEOF_DOUBLE = 8
const m_SIZEOF_FLOAT = 4
const m_SIZEOF_INT = 4
const m_SIZEOF_LONG = 8
const m_STDC_HEADERS = 1
const m_TMP_MAX = 308915776
const m_VERSION = "0.2.2"
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__LP64 = 1
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BITINT_MAXWIDTH__ = 8388608
const m___BOOL_WIDTH__ = 8
const m___BSD_VISIBLE = 1
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CC_SUPPORTS_DYNAMIC_ARRAY_INIT = 1
const m___CC_SUPPORTS_INLINE = 1
const m___CC_SUPPORTS_VARADIC_XXX = 1
const m___CC_SUPPORTS_WARNING = 1
const m___CC_SUPPORTS___FUNC__ = 1
const m___CC_SUPPORTS___INLINE = 1
const m___CC_SUPPORTS___INLINE__ = 1
const m___CHAR_BIT = 8
const m___CHAR_BIT__ = 8
const m___CLANG_ATOMIC_BOOL_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR_LOCK_FREE = 2
const m___CLANG_ATOMIC_INT_LOCK_FREE = 2
const m___CLANG_ATOMIC_LLONG_LOCK_FREE = 2
const m___CLANG_ATOMIC_LONG_LOCK_FREE = 2
const m___CLANG_ATOMIC_POINTER_LOCK_FREE = 2
const m___CLANG_ATOMIC_SHORT_LOCK_FREE = 2
const m___CLANG_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___CONSTANT_CFSTRINGS__ = 1
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DENORM_MIN__ = 4.9406564584124654e-324
const m___DBL_DIG__ = 15
const m___DBL_EPSILON__ = 2.2204460492503131e-16
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DBL_MAX__ = 1.7976931348623157e+308
const m___DBL_MIN__ = 2.2250738585072014e-308
const m___DECIMAL_DIG__ = "__LDBL_DECIMAL_DIG__"
const m___ELF__ = 1
const m___EXT1_VISIBLE = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLT16_DECIMAL_DIG__ = 5
const m___FLT16_DENORM_MIN__ = 5.9604644775390625e-8
const m___FLT16_DIG__ = 3
const m___FLT16_EPSILON__ = 9.765625e-4
const m___FLT16_HAS_DENORM__ = 1
const m___FLT16_HAS_INFINITY__ = 1
const m___FLT16_HAS_QUIET_NAN__ = 1
const m___FLT16_MANT_DIG__ = 11
const m___FLT16_MAX_10_EXP__ = 4
const m___FLT16_MAX_EXP__ = 16
const m___FLT16_MAX__ = 6.5504e+4
const m___FLT16_MIN__ = 6.103515625e-5
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209290e-7
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282347e+38
const m___FLT_MIN__ = 1.17549435e-38
const m___FLT_RADIX__ = 2
const m___FUNCTION__ = "__func__"
const m___FXSR__ = 1
const m___FreeBSD__ = 14
const m___FreeBSD_cc_version = 1400006
const m___GCC_ASM_FLAG_OUTPUTS__ = 1
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GNUCLIKE_ASM = 3
const m___GNUCLIKE_BUILTIN_CONSTANT_P = 1
const m___GNUCLIKE_BUILTIN_MEMCPY = 1
const m___GNUCLIKE_BUILTIN_NEXT_ARG = 1
const m___GNUCLIKE_BUILTIN_STDARG = 1
const m___GNUCLIKE_BUILTIN_VAALIST = 1
const m___GNUCLIKE_BUILTIN_VARARGS = 1
const m___GNUCLIKE_CTOR_SECTION_HANDLING = 1
const m___GNUCLIKE___SECTION = 1
const m___GNUCLIKE___TYPEOF = 1
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 1
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC_VA_LIST_COMPATIBILITY = 1
const m___GNUC__ = 4
const m___GXX_ABI_VERSION = 1002
const m___INT16_FMTd__ = "hd"
const m___INT16_FMTi__ = "hi"
const m___INT16_MAX__ = 32767
const m___INT16_TYPE__ = "short"
const m___INT32_FMTd__ = "d"
const m___INT32_FMTi__ = "i"
const m___INT32_MAX__ = 2147483647
const m___INT32_TYPE__ = "int"
const m___INT64_C_SUFFIX__ = "L"
const m___INT64_FMTd__ = "ld"
const m___INT64_FMTi__ = "li"
const m___INT64_MAX__ = 9223372036854775807
const m___INT8_FMTd__ = "hhd"
const m___INT8_FMTi__ = "hhi"
const m___INT8_MAX__ = 127
const m___INTMAX_C_SUFFIX__ = "L"
const m___INTMAX_FMTd__ = "ld"
const m___INTMAX_FMTi__ = "li"
const m___INTMAX_MAX__ = 9223372036854775807
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_FMTd__ = "ld"
const m___INTPTR_FMTi__ = "li"
const m___INTPTR_MAX__ = 9223372036854775807
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_FMTd__ = "hd"
const m___INT_FAST16_FMTi__ = "hi"
const m___INT_FAST16_MAX__ = 32767
const m___INT_FAST16_TYPE__ = "short"
const m___INT_FAST16_WIDTH__ = 16
const m___INT_FAST32_FMTd__ = "d"
const m___INT_FAST32_FMTi__ = "i"
const m___INT_FAST32_MAX__ = 2147483647
const m___INT_FAST32_TYPE__ = "int"
const m___INT_FAST32_WIDTH__ = 32
const m___INT_FAST64_FMTd__ = "ld"
const m___INT_FAST64_FMTi__ = "li"
const m___INT_FAST64_MAX__ = 9223372036854775807
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_FMTd__ = "hhd"
const m___INT_FAST8_FMTi__ = "hhi"
const m___INT_FAST8_MAX__ = 127
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_FMTd__ = "hd"
const m___INT_LEAST16_FMTi__ = "hi"
const m___INT_LEAST16_MAX__ = 32767
const m___INT_LEAST16_TYPE__ = "short"
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_FMTd__ = "d"
const m___INT_LEAST32_FMTi__ = "i"
const m___INT_LEAST32_MAX__ = 2147483647
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_FMTd__ = "ld"
const m___INT_LEAST64_FMTi__ = "li"
const m___INT_LEAST64_MAX__ = 9223372036854775807
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_FMTd__ = "hhd"
const m___INT_LEAST8_FMTi__ = "hhi"
const m___INT_LEAST8_MAX__ = 127
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX = 0x7fffffff
const m___INT_MAX__ = 2147483647
const m___INT_WIDTH__ = 32
const m___ISO_C_VISIBLE = 2011
const m___KPRINTF_ATTRIBUTE__ = 1
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.2204460492503131e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.7976931348623157e+308
const m___LDBL_MIN__ = 2.2250738585072014e-308
const m___LITTLE_ENDIAN__ = 1
const m___LLONG_MAX = 0x7fffffffffffffff
const m___LLONG_WIDTH__ = 64
const m___LONG_BIT = 64
const m___LONG_LONG_MAX__ = 9223372036854775807
const m___LONG_MAX = 0x7fffffffffffffff
const m___LONG_MAX__ = 9223372036854775807
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___MMX__ = 1
const m___NO_INLINE__ = 1
const m___NO_MATH_ERRNO__ = 1
const m___NO_MATH_INLINES = 1
const m___OBJC_BOOL_IS_BOOL = 0
const m___OFF_MAX = "__LONG_MAX"
const m___OFF_MIN = "__LONG_MIN"
const m___OPENCL_MEMORY_SCOPE_ALL_SVM_DEVICES = 3
const m___OPENCL_MEMORY_SCOPE_DEVICE = 2
const m___OPENCL_MEMORY_SCOPE_SUB_GROUP = 4
const m___OPENCL_MEMORY_SCOPE_WORK_GROUP = 1
const m___OPENCL_MEMORY_SCOPE_WORK_ITEM = 0
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___POINTER_WIDTH__ = 64
const m___POSIX_VISIBLE = 200809
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTRDIFF_FMTd__ = "ld"
const m___PTRDIFF_FMTi__ = "li"
const m___PTRDIFF_MAX__ = 9223372036854775807
const m___PTRDIFF_WIDTH__ = 64
const m___QUAD_MAX = "__LONG_MAX"
const m___QUAD_MIN = "__LONG_MIN"
const m___S2OAP = 0x0001
const m___SALC = 0x4000
const m___SAPP = 0x0100
const m___SCHAR_MAX = 0x7f
const m___SCHAR_MAX__ = 127
const m___SEG_FS = 1
const m___SEG_GS = 1
const m___SEOF = 0x0020
const m___SERR = 0x0040
const m___SHRT_MAX = 0x7fff
const m___SHRT_MAX__ = 32767
const m___SHRT_WIDTH__ = 16
const m___SIGN = 0x8000
const m___SIG_ATOMIC_MAX__ = 2147483647
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_FMTX__ = "lX"
const m___SIZE_FMTo__ = "lo"
const m___SIZE_FMTu__ = "lu"
const m___SIZE_FMTx__ = "lx"
const m___SIZE_MAX__ = 18446744073709551615
const m___SIZE_T_MAX = "__ULONG_MAX"
const m___SIZE_WIDTH__ = 64
const m___SLBF = 0x0001
const m___SMBF = 0x0080
const m___SMOD = 0x2000
const m___SNBF = 0x0002
const m___SNPT = 0x0800
const m___SOFF = 0x1000
const m___SOPT = 0x0400
const m___SRD = 0x0004
const m___SRW = 0x0010
const m___SSE2_MATH__ = 1
const m___SSE2__ = 1
const m___SSE_MATH__ = 1
const m___SSE__ = 1
const m___SSIZE_MAX = "__LONG_MAX"
const m___SSTR = 0x0200
const m___STDC_HOSTED__ = 1
const m___STDC_MB_MIGHT_NEQ_WC__ = 1
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC__ = 1
const m___SWR = 0x0008
const m___UCHAR_MAX = 0xff
const m___UINT16_FMTX__ = "hX"
const m___UINT16_FMTo__ = "ho"
const m___UINT16_FMTu__ = "hu"
const m___UINT16_FMTx__ = "hx"
const m___UINT16_MAX__ = 65535
const m___UINT32_C_SUFFIX__ = "U"
const m___UINT32_FMTX__ = "X"
const m___UINT32_FMTo__ = "o"
const m___UINT32_FMTu__ = "u"
const m___UINT32_FMTx__ = "x"
const m___UINT32_MAX__ = 4294967295
const m___UINT64_C_SUFFIX__ = "UL"
const m___UINT64_FMTX__ = "lX"
const m___UINT64_FMTo__ = "lo"
const m___UINT64_FMTu__ = "lu"
const m___UINT64_FMTx__ = "lx"
const m___UINT64_MAX__ = 18446744073709551615
const m___UINT8_FMTX__ = "hhX"
const m___UINT8_FMTo__ = "hho"
const m___UINT8_FMTu__ = "hhu"
const m___UINT8_FMTx__ = "hhx"
const m___UINT8_MAX__ = 255
const m___UINTMAX_C_SUFFIX__ = "UL"
const m___UINTMAX_FMTX__ = "lX"
const m___UINTMAX_FMTo__ = "lo"
const m___UINTMAX_FMTu__ = "lu"
const m___UINTMAX_FMTx__ = "lx"
const m___UINTMAX_MAX__ = 18446744073709551615
const m___UINTMAX_WIDTH__ = 64
const m___UINTPTR_FMTX__ = "lX"
const m___UINTPTR_FMTo__ = "lo"
const m___UINTPTR_FMTu__ = "lu"
const m___UINTPTR_FMTx__ = "lx"
const m___UINTPTR_MAX__ = 18446744073709551615
const m___UINTPTR_WIDTH__ = 64
const m___UINT_FAST16_FMTX__ = "hX"
const m___UINT_FAST16_FMTo__ = "ho"
const m___UINT_FAST16_FMTu__ = "hu"
const m___UINT_FAST16_FMTx__ = "hx"
const m___UINT_FAST16_MAX__ = 65535
const m___UINT_FAST32_FMTX__ = "X"
const m___UINT_FAST32_FMTo__ = "o"
const m___UINT_FAST32_FMTu__ = "u"
const m___UINT_FAST32_FMTx__ = "x"
const m___UINT_FAST32_MAX__ = 4294967295
const m___UINT_FAST64_FMTX__ = "lX"
const m___UINT_FAST64_FMTo__ = "lo"
const m___UINT_FAST64_FMTu__ = "lu"
const m___UINT_FAST64_FMTx__ = "lx"
const m___UINT_FAST64_MAX__ = 18446744073709551615
const m___UINT_FAST8_FMTX__ = "hhX"
const m___UINT_FAST8_FMTo__ = "hho"
const m___UINT_FAST8_FMTu__ = "hhu"
const m___UINT_FAST8_FMTx__ = "hhx"
const m___UINT_FAST8_MAX__ = 255
const m___UINT_LEAST16_FMTX__ = "hX"
const m___UINT_LEAST16_FMTo__ = "ho"
const m___UINT_LEAST16_FMTu__ = "hu"
const m___UINT_LEAST16_FMTx__ = "hx"
const m___UINT_LEAST16_MAX__ = 65535
const m___UINT_LEAST32_FMTX__ = "X"
const m___UINT_LEAST32_FMTo__ = "o"
const m___UINT_LEAST32_FMTu__ = "u"
const m___UINT_LEAST32_FMTx__ = "x"
const m___UINT_LEAST32_MAX__ = 4294967295
const m___UINT_LEAST64_FMTX__ = "lX"
const m___UINT_LEAST64_FMTo__ = "lo"
const m___UINT_LEAST64_FMTu__ = "lu"
const m___UINT_LEAST64_FMTx__ = "lx"
const m___UINT_LEAST64_MAX__ = 18446744073709551615
const m___UINT_LEAST8_FMTX__ = "hhX"
const m___UINT_LEAST8_FMTo__ = "hho"
const m___UINT_LEAST8_FMTu__ = "hhu"
const m___UINT_LEAST8_FMTx__ = "hhx"
const m___UINT_LEAST8_MAX__ = 255
const m___UINT_MAX = 0xffffffff
const m___ULLONG_MAX = "0xffffffffffffffffU"
const m___ULONG_MAX = 0xffffffffffffffff
const m___UQUAD_MAX = "__ULONG_MAX"
const m___USHRT_MAX = 0xffff
const m___VERSION__ = "FreeBSD Clang 16.0.6 (https://github.com/llvm/llvm-project.git llvmorg-16.0.6-0-g7cbf1a259152)"
const m___WCHAR_MAX = "__INT_MAX"
const m___WCHAR_MAX__ = 2147483647
const m___WCHAR_MIN = "__INT_MIN"
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 2147483647
const m___WINT_TYPE__ = "int"
const m___WINT_WIDTH__ = 32
const m___WORD_BIT = 32
const m___XSI_VISIBLE = 700
const m___amd64 = 1
const m___amd64__ = 1
const m___clang__ = 1
const m___clang_literal_encoding__ = "UTF-8"
const m___clang_major__ = 16
const m___clang_minor__ = 0
const m___clang_patchlevel__ = 6
const m___clang_version__ = "16.0.6 (https://github.com/llvm/llvm-project.git llvmorg-16.0.6-0-g7cbf1a259152)"
const m___clang_wide_literal_encoding__ = "UTF-32"
const m___code_model_small__ = 1
const m___const = "const"
const m___has_extension = "__has_feature"
const m___isnan = "__inline_isnan"
const m___isnanf = "__inline_isnanf"
const m___k8 = 1
const m___k8__ = 1
const m___llvm__ = 1
const m___restrict = "restrict"
const m___restrict_arr = "restrict"
const m___signed = "signed"
const m___tune_k8__ = 1
const m___unix = 1
const m___unix__ = 1
const m___volatile = "volatile"
const m___x86_64 = 1
const m___x86_64__ = 1
const m_math_errhandling = "MATH_ERREXCEPT"
const m_stderr = "__stderrp"
const m_stdin = "__stdinp"
const m_stdout = "__stdoutp"
const m_unix = 1

type T__builtin_va_list = uintptr

type T__predefined_size_t = uint64

type T__predefined_wchar_t = int32

type T__predefined_ptrdiff_t = int64

type T__int8_t = int8

type T__uint8_t = uint8

type T__int16_t = int16

type T__uint16_t = uint16

type T__int32_t = int32

type T__uint32_t = uint32

type T__int64_t = int64

type T__uint64_t = uint64

type T__int_least8_t = int8

type T__int_least16_t = int16

type T__int_least32_t = int32

type T__int_least64_t = int64

type T__intmax_t = int64

type T__uint_least8_t = uint8

type T__uint_least16_t = uint16

type T__uint_least32_t = uint32

type T__uint_least64_t = uint64

type T__uintmax_t = uint64

type T__intptr_t = int64

type T__intfptr_t = int64

type T__uintptr_t = uint64

type T__uintfptr_t = uint64

type T__vm_offset_t = uint64

type T__vm_size_t = uint64

type T__size_t = uint64

type T__ssize_t = int64

type T__ptrdiff_t = int64

type T__clock_t = int32

type T__critical_t = int64

type T__double_t = float64

type T__float_t = float32

type T__int_fast8_t = int32

type T__int_fast16_t = int32

type T__int_fast32_t = int32

type T__int_fast64_t = int64

type T__register_t = int64

type T__segsz_t = int64

type T__time_t = int64

type T__uint_fast8_t = uint32

type T__uint_fast16_t = uint32

type T__uint_fast32_t = uint32

type T__uint_fast64_t = uint64

type T__u_register_t = uint64

type T__vm_paddr_t = uint64

type T___wchar_t = int32

type T__blksize_t = int32

type T__blkcnt_t = int64

type T__clockid_t = int32

type T__fflags_t = uint32

type T__fsblkcnt_t = uint64

type T__fsfilcnt_t = uint64

type T__gid_t = uint32

type T__id_t = int64

type T__ino_t = uint64

type T__key_t = int64

type T__lwpid_t = int32

type T__mode_t = uint16

type T__accmode_t = int32

type T__nl_item = int32

type T__nlink_t = uint64

type T__off_t = int64

type T__off64_t = int64

type T__pid_t = int32

type T__sbintime_t = int64

type T__rlim_t = int64

type T__sa_family_t = uint8

type T__socklen_t = uint32

type T__suseconds_t = int64

type T__timer_t = uintptr

type T__mqd_t = uintptr

type T__uid_t = uint32

type T__useconds_t = uint32

type T__cpuwhich_t = int32

type T__cpulevel_t = int32

type T__cpusetid_t = int32

type T__daddr_t = int64

type T__ct_rune_t = int32

type T__rune_t = int32

type T__wint_t = int32

type T__char16_t = uint16

type T__char32_t = uint32

type T__max_align_t = struct {
	F__max_align1 int64
	F__max_align2 float64
}

type T__dev_t = uint64

type T__fixpt_t = uint32

type T__mbstate_t = struct {
	F_mbstateL  [0]T__int64_t
	F__mbstate8 [128]int8
}

type T__rman_res_t = uint64

type T__va_list = uintptr

type T__gnuc_va_list = uintptr

type Tfpos_t = int64

type Tsize_t = uint64

type Trsize_t = uint64

type Toff_t = int64

type Tssize_t = int64

type Toff64_t = int64

type Tva_list = uintptr

type T__sbuf = struct {
	F_base uintptr
	F_size int32
}

type T__sFILE = struct {
	F_p           uintptr
	F_r           int32
	F_w           int32
	F_flags       int16
	F_file        int16
	F_bf          T__sbuf
	F_lbfsize     int32
	F_cookie      uintptr
	F_close       uintptr
	F_read        uintptr
	F_seek        uintptr
	F_write       uintptr
	F_ub          T__sbuf
	F_up          uintptr
	F_ur          int32
	F_ubuf        [3]uint8
	F_nbuf        [1]uint8
	F_lb          T__sbuf
	F_blksize     int32
	F_offset      Tfpos_t
	F_fl_mutex    uintptr
	F_fl_owner    uintptr
	F_fl_count    int32
	F_orientation int32
	F_mbstate     T__mbstate_t
	F_flags2      int32
}

type TFILE = struct {
	F_p           uintptr
	F_r           int32
	F_w           int32
	F_flags       int16
	F_file        int16
	F_bf          T__sbuf
	F_lbfsize     int32
	F_cookie      uintptr
	F_close       uintptr
	F_read        uintptr
	F_seek        uintptr
	F_write       uintptr
	F_ub          T__sbuf
	F_up          uintptr
	F_ur          int32
	F_ubuf        [3]uint8
	F_nbuf        [1]uint8
	F_lb          T__sbuf
	F_blksize     int32
	F_offset      Tfpos_t
	F_fl_mutex    uintptr
	F_fl_owner    uintptr
	F_fl_count    int32
	F_orientation int32
	F_mbstate     T__mbstate_t
	F_flags2      int32
}

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type Trune_t = int32

type Twchar_t = int32

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Terrno_t = int32

type Tconstraint_handler_t = uintptr

type Tdouble_t = float64

type Tfloat_t = float32

type Tlocale_t = uintptr

type Tmode_t = uint16

type TSRC_DATA = struct {
	Fdata_in           uintptr
	Fdata_out          uintptr
	Finput_frames      int64
	Foutput_frames     int64
	Finput_frames_used int64
	Foutput_frames_gen int64
	Fend_of_input      int32
	Fsrc_ratio         float64
}

type Tsrc_callback_t = uintptr

const SRC_SINC_BEST_QUALITY = 0
const SRC_SINC_MEDIUM_QUALITY = 1
const SRC_SINC_FASTEST = 2
const SRC_ZERO_ORDER_HOLD = 3
const SRC_LINEAR = 4

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	libc.Xputs(tls, __ccgo_ts)
	libc.Xprintf(tls, __ccgo_ts+24, 0)
	libc.Xfflush(tls, libc.X__stdoutp)
	_varispeed_test(tls, SRC_ZERO_ORDER_HOLD, float64(10))
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xprintf(tls, __ccgo_ts+70, 0)
	libc.Xfflush(tls, libc.X__stdoutp)
	_varispeed_test(tls, SRC_LINEAR, float64(10))
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xprintf(tls, __ccgo_ts+113, 0)
	libc.Xfflush(tls, libc.X__stdoutp)
	_varispeed_test(tls, SRC_SINC_FASTEST, float64(115))
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xputs(tls, __ccgo_ts+156)
	libc.Xprintf(tls, __ccgo_ts+24, 0)
	libc.Xfflush(tls, libc.X__stdoutp)
	_varispeed_bounds_test(tls, SRC_ZERO_ORDER_HOLD)
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xprintf(tls, __ccgo_ts+70, 0)
	libc.Xfflush(tls, libc.X__stdoutp)
	_varispeed_bounds_test(tls, SRC_LINEAR)
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xprintf(tls, __ccgo_ts+113, 0)
	libc.Xfflush(tls, libc.X__stdoutp)
	_varispeed_bounds_test(tls, SRC_SINC_FASTEST)
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xputs(tls, __ccgo_ts+183)
	return 0
}

/* main */

func _varispeed_test(tls *libc.TLS, converter int32, target_snr float64) {
	bp := tls.Alloc(112)
	defer tls.Free(112)
	var input_len, v2, v3, v4, v6, v7 int32
	var snr float64
	var src_state, v1 uintptr
	var v5 int64
	var _ /* error at bp+72 */ int32
	var _ /* sine_freq at bp+0 */ float64
	var _ /* src_data at bp+8 */ TSRC_DATA
	_, _, _, _, _, _, _, _, _, _ = input_len, snr, src_state, v1, v2, v3, v4, v5, v6, v7
	libc.Xmemset(tls, uintptr(unsafe.Pointer(&_input)), 0, uint64(65536))
	input_len = int32(libc.Uint64FromInt64(65536)/libc.Uint64FromInt64(4)) / libc.Int32FromInt32(2)
	*(*float64)(unsafe.Pointer(bp)) = float64(0.0111)
	x_gen_windowed_sines(tls, int32(1), bp, float64(1), uintptr(unsafe.Pointer(&_input)), input_len)
	/* Perform sample rate conversion. */
	v1 = libsamplerate.Xsrc_new(tls, converter, int32(1), bp+72)
	src_state = v1
	if v1 == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+184, libc.VaList(bp+88, int32(98), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 72)))))
		libc.Xexit(tls, int32(1))
	}
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fend_of_input = int32(1)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fdata_in = uintptr(unsafe.Pointer(&_input))
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames = int64(input_len)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fsrc_ratio = float64(3)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fdata_out = uintptr(unsafe.Pointer(&_output))
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames = int64(int32(libc.Uint64FromInt64(65536) / libc.Uint64FromInt64(4)))
	v2 = libsamplerate.Xsrc_set_ratio(tls, src_state, float64(1)/(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fsrc_ratio)
	*(*int32)(unsafe.Pointer(bp + 72)) = v2
	if v2 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+88, int32(113), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 72)))))
		libc.Xexit(tls, int32(1))
	}
	v3 = libsamplerate.Xsrc_process(tls, src_state, bp+8)
	*(*int32)(unsafe.Pointer(bp + 72)) = v3
	if v3 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+88, int32(118), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 72)))))
		libc.Xprintf(tls, __ccgo_ts+238, libc.VaList(bp+88, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames))
		libc.Xprintf(tls, __ccgo_ts+270, libc.VaList(bp+88, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames))
		libc.Xexit(tls, int32(1))
	}
	if (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames_used != int64(input_len) {
		libc.Xprintf(tls, __ccgo_ts+303, libc.VaList(bp+88, int32(125)))
		libc.Xprintf(tls, __ccgo_ts+330, libc.VaList(bp+88, input_len))
		libc.Xprintf(tls, __ccgo_ts+355, libc.VaList(bp+88, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames_used))
		libc.Xexit(tls, int32(1))
	}
	/* Copy the last output to the input. */
	libc.Xmemcpy(tls, uintptr(unsafe.Pointer(&_input)), uintptr(unsafe.Pointer(&_output)), uint64(65536))
	x_reverse_data(tls, uintptr(unsafe.Pointer(&_input)), int32((*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames_gen))
	v4 = libsamplerate.Xsrc_reset(tls, src_state)
	*(*int32)(unsafe.Pointer(bp + 72)) = v4
	if v4 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+88, int32(136), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 72)))))
		libc.Xexit(tls, int32(1))
	}
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fend_of_input = int32(1)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fdata_in = uintptr(unsafe.Pointer(&_input))
	v5 = (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames_gen
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames = v5
	input_len = int32(v5)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fdata_out = uintptr(unsafe.Pointer(&_output))
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames = int64(int32(libc.Uint64FromInt64(65536) / libc.Uint64FromInt64(4)))
	v6 = libsamplerate.Xsrc_set_ratio(tls, src_state, float64(1)/(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fsrc_ratio)
	*(*int32)(unsafe.Pointer(bp + 72)) = v6
	if v6 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+88, int32(149), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 72)))))
		libc.Xexit(tls, int32(1))
	}
	v7 = libsamplerate.Xsrc_process(tls, src_state, bp+8)
	*(*int32)(unsafe.Pointer(bp + 72)) = v7
	if v7 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+88, int32(154), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 72)))))
		libc.Xprintf(tls, __ccgo_ts+238, libc.VaList(bp+88, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames))
		libc.Xprintf(tls, __ccgo_ts+270, libc.VaList(bp+88, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames))
		libc.Xexit(tls, int32(1))
	}
	if (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames_used != int64(input_len) {
		libc.Xprintf(tls, __ccgo_ts+303, libc.VaList(bp+88, int32(161)))
		libc.Xprintf(tls, __ccgo_ts+330, libc.VaList(bp+88, input_len))
		libc.Xprintf(tls, __ccgo_ts+355, libc.VaList(bp+88, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames_used))
		libc.Xexit(tls, int32(1))
	}
	src_state = libsamplerate.Xsrc_delete(tls, src_state)
	snr = x_calculate_snr(tls, uintptr(unsafe.Pointer(&_output)), int32((*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames_gen), int32(1))
	if target_snr > snr {
		libc.Xprintf(tls, __ccgo_ts+382, libc.VaList(bp+88, int32(172), snr, target_snr))
		x_save_oct_float(tls, __ccgo_ts+437, uintptr(unsafe.Pointer(&_input)), int32((*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames), uintptr(unsafe.Pointer(&_output)), int32((*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames_gen))
		libc.Xexit(tls, int32(1))
	}
	return
}

var _input [16384]float32

var _output [16384]float32

/* varispeed_test */

func _varispeed_bounds_test(tls *libc.TLS, converter int32) {
	var chan1, r1, r2 int32
	var ratios [3]float64
	_, _, _, _ = chan1, r1, r2, ratios
	ratios = [3]float64{
		0: float64(0.1),
		1: float64(0.01),
		2: libc.Float64FromInt32(20),
	}
	chan1 = int32(1)
	for {
		if !(chan1 <= int32(9)) {
			break
		}
		r1 = 0
		for {
			if !(r1 < int32(libc.Uint64FromInt64(24)/libc.Uint64FromInt64(8))) {
				break
			}
			r2 = 0
			for {
				if !(r2 < int32(libc.Uint64FromInt64(24)/libc.Uint64FromInt64(8))) {
					break
				}
				if r1 != r2 {
					_set_ratio_test(tls, converter, chan1, ratios[r1], ratios[r2])
				}
				goto _3
			_3:
				r2++
			}
			goto _2
		_2:
			r1++
		}
		goto _1
	_1:
		chan1++
	}
}

/* varispeed_bounds_test */

func _set_ratio_test(tls *libc.TLS, converter int32, channels int32, initial_ratio float64, second_ratio float64) {
	bp := tls.Alloc(240)
	defer tls.Free(240)
	var chunk_size, k, max_loop_count, total_frames_gen, total_frames_used, total_input_frames, total_output_frames, v3, v4, v5, v8 int32
	var input, output, src_state, v1 uintptr
	var v7 float32
	var _ /* details at bp+0 */ [128]int8
	var _ /* error at bp+192 */ int32
	var _ /* src_data at bp+128 */ TSRC_DATA
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = chunk_size, input, k, max_loop_count, output, src_state, total_frames_gen, total_frames_used, total_input_frames, total_output_frames, v1, v3, v4, v5, v7, v8
	total_input_frames = libc.Int32FromInt32(1) << libc.Int32FromInt32(14)
	/* Maximum upsample ratio is 20, use a value beigger. */
	total_output_frames = libc.Int32FromInt32(25) * (libc.Int32FromInt32(1) << libc.Int32FromInt32(14))
	/* Interested in array boundary conditions, so all zero data here is fine. */
	input = libc.Xcalloc(tls, uint64(total_input_frames*channels), uint64(4))
	output = libc.Xcalloc(tls, uint64(total_output_frames*channels), uint64(4))
	max_loop_count = int32(100000)
	chunk_size = int32(128)
	libc.X__builtin_snprintf(tls, bp, uint64(128), __ccgo_ts+451, libc.VaList(bp+208, channels, initial_ratio, second_ratio))
	v1 = libsamplerate.Xsrc_new(tls, converter, channels, bp+192)
	src_state = v1
	if v1 == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+184, libc.VaList(bp+208, int32(215), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 192)))))
		libc.Xexit(tls, int32(1))
	}
	total_frames_used = 0
	total_frames_gen = 0
	libc.Xmemset(tls, bp+128, 0, uint64(64))
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fend_of_input = 0
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fsrc_ratio = initial_ratio
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fdata_in = input
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fdata_out = output
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Finput_frames = int64(chunk_size)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames = int64(total_output_frames)
	/* Use a max_loop_count here to enable the detection of infinite loops
	** (due to end of input not being detected.
	 */
	k = 0
	for {
		if !(k < max_loop_count) {
			break
		}
		if k == int32(1) {
			/* Hard switch to second_ratio after processing one chunk. */
			(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fsrc_ratio = second_ratio
			v3 = libsamplerate.Xsrc_set_ratio(tls, src_state, second_ratio)
			*(*int32)(unsafe.Pointer(bp + 192)) = v3
			if v3 != 0 {
				libc.Xprintf(tls, __ccgo_ts+479, libc.VaList(bp+208, int32(238), bp, libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 192)))))
				libc.Xexit(tls, int32(1))
			}
		}
		v4 = libsamplerate.Xsrc_process(tls, src_state, bp+128)
		*(*int32)(unsafe.Pointer(bp + 192)) = v4
		if v4 != 0 {
			libc.Xprintf(tls, __ccgo_ts+479, libc.VaList(bp+208, int32(244), bp, libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 192)))))
			libc.Xexit(tls, int32(1))
		}
		if (*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fend_of_input != 0 && (*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames_gen == 0 {
			break
		}
		total_frames_used = int32(int64(total_frames_used) + (*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Finput_frames_used)
		total_frames_gen = int32(int64(total_frames_gen) + (*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames_gen)
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fdata_in += uintptr((*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Finput_frames_used*int64(channels)) * 4
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fdata_out += uintptr((*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames_gen*int64(channels)) * 4
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Finput_frames = int64(total_input_frames - total_frames_used)
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames = int64(total_output_frames - total_frames_gen)
		if total_frames_used >= total_input_frames {
			v5 = int32(1)
		} else {
			v5 = 0
		}
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fend_of_input = v5
		goto _2
	_2:
		k++
	}
	if !(k < max_loop_count) {
		libc.Xprintf(tls, __ccgo_ts+501, libc.VaList(bp+208, int32(263), __ccgo_ts+536))
		libc.Xexit(tls, int32(1))
	}
	if !(total_frames_gen > libc.Int32FromInt32(0)) {
		libc.Xprintf(tls, __ccgo_ts+501, libc.VaList(bp+208, int32(264), __ccgo_ts+555))
		libc.Xexit(tls, int32(1))
	}
	k = 0
	for {
		if !(k < total_frames_gen*channels) {
			break
		}
		v7 = *(*float32)(unsafe.Pointer(output + uintptr(k)*4))
		v8 = libc.BoolInt32(v7 != v7)
		goto _9
	_9:
		if !!(v8 != 0) {
			libc.Xprintf(tls, __ccgo_ts+501, libc.VaList(bp+208, int32(267), __ccgo_ts+576))
			libc.Xexit(tls, int32(1))
		}
		goto _6
	_6:
		k++
	}
	src_state = libsamplerate.Xsrc_delete(tls, src_state)
	libc.Xfree(tls, input)
	libc.Xfree(tls, output)
	return
}

/* set_ratio_test */

func main() {
	libc.Start(x_main)
}

const m_M_PI1 = 3.141592653589793
const m__CTYPE_A = 256
const m__CTYPE_B = 131072
const m__CTYPE_C = 512
const m__CTYPE_D = 1024
const m__CTYPE_G = 2048
const m__CTYPE_I = 524288
const m__CTYPE_L = 4096
const m__CTYPE_N = 4194304
const m__CTYPE_P = 8192
const m__CTYPE_Q = 2097152
const m__CTYPE_R = 262144
const m__CTYPE_S = 16384
const m__CTYPE_SW0 = 0x20000000
const m__CTYPE_SW1 = 0x40000000
const m__CTYPE_SW2 = 0x80000000
const m__CTYPE_SW3 = 0xc0000000
const m__CTYPE_SWM = 3758096384
const m__CTYPE_SWS = 30
const m__CTYPE_T = 1048576
const m__CTYPE_U = 32768
const m__CTYPE_X = 65536
const m__RUNE_MAGIC_1 = "RuneMagi"
const m__XLOCALE_INLINE = "inline"
const m__XLOCALE_RUN_FUNCTIONS_DEFINED = 1

type T_RuneEntry = struct {
	F__min   T__rune_t
	F__max   T__rune_t
	F__map   T__rune_t
	F__types uintptr
}

type T_RuneRange = struct {
	F__nranges int32
	F__ranges  uintptr
}

type T_RuneLocale = struct {
	F__magic        [8]int8
	F__encoding     [32]int8
	F__sgetrune     uintptr
	F__sputrune     uintptr
	F__invalid_rune T__rune_t
	F__runetype     [256]uint64
	F__maplower     [256]T__rune_t
	F__mapupper     [256]T__rune_t
	F__runetype_ext T_RuneRange
	F__maplower_ext T_RuneRange
	F__mapupper_ext T_RuneRange
	F__variable     uintptr
	F__variable_len int32
}

func x_gen_windowed_sines(tls *libc.TLS, freq_count int32, freqs uintptr, max float64, output uintptr, output_len int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var amplitude, phase float64
	var freq, k int32
	_, _, _, _ = amplitude, freq, k, phase
	amplitude = max / float64(freq_count)
	k = 0
	for {
		if !(k < output_len) {
			break
		}
		*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(0)
		goto _1
	_1:
		k++
	}
	freq = 0
	for {
		if !(freq < freq_count) {
			break
		}
		phase = libc.Float64FromFloat64(0.9) * libc.Float64FromFloat64(3.141592653589793) / float64(freq_count)
		if *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8)) <= float64(0) || *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8)) >= float64(0.5) {
			libc.Xprintf(tls, __ccgo_ts+597, libc.VaList(bp+8, __ccgo_ts+662, freq, *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8))))
			libc.Xexit(tls, int32(1))
		}
		k = 0
		for {
			if !(k < output_len) {
				break
			}
			*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4))) + amplitude*libc.Xsin(tls, *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8))*float64(libc.Int32FromInt32(2)*k)*float64(3.141592653589793)+phase))
			goto _3
		_3:
			k++
		}
		goto _2
	_2:
		freq++
	}
	/* Apply Hanning Window. */
	k = 0
	for {
		if !(k < output_len) {
			break
		}
		*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4))) * (libc.Float64FromFloat64(0.5) - libc.Float64FromFloat64(0.5)*libc.Xcos(tls, float64(libc.Int32FromInt32(2)*k)*float64(3.141592653589793)/float64(output_len-libc.Int32FromInt32(1)))))
		goto _4
	_4:
		k++
	}
	/*	data [k] *= 0.3635819 - 0.4891775 * cos ((2 * k) * M_PI / (output_len - 1))
		+ 0.1365995 * cos ((4 * k) * M_PI / (output_len - 1))
		- 0.0106411 * cos ((6 * k) * M_PI / (output_len - 1)) ;
	*/
	return
}

/* gen_windowed_sines */

func x_save_oct_float(tls *libc.TLS, filename uintptr, input uintptr, in_len int32, output uintptr, out_len int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var file, v1 uintptr
	var k int32
	_, _, _ = file, k, v1
	libc.Xprintf(tls, __ccgo_ts+675, libc.VaList(bp+8, filename))
	v1 = libc.Xfopen(tls, filename, __ccgo_ts+721)
	file = v1
	if !(v1 != 0) {
		return
	}
	libc.Xfprintf(tls, file, __ccgo_ts+723, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+748, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+763, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+779, libc.VaList(bp+8, in_len))
	libc.Xfprintf(tls, file, __ccgo_ts+791, 0)
	k = 0
	for {
		if !(k < in_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+805, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(input + uintptr(k)*4)))))
		goto _2
	_2:
		k++
	}
	libc.Xfprintf(tls, file, __ccgo_ts+810, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+763, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+779, libc.VaList(bp+8, out_len))
	libc.Xfprintf(tls, file, __ccgo_ts+791, 0)
	k = 0
	for {
		if !(k < out_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+805, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4)))))
		goto _3
	_3:
		k++
	}
	libc.Xfclose(tls, file)
	return
}

/* save_oct_float */

func x_save_oct_double(tls *libc.TLS, filename uintptr, input uintptr, in_len int32, output uintptr, out_len int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var file, v1 uintptr
	var k int32
	_, _, _ = file, k, v1
	libc.Xprintf(tls, __ccgo_ts+675, libc.VaList(bp+8, filename))
	v1 = libc.Xfopen(tls, filename, __ccgo_ts+721)
	file = v1
	if !(v1 != 0) {
		return
	}
	libc.Xfprintf(tls, file, __ccgo_ts+723, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+748, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+763, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+779, libc.VaList(bp+8, in_len))
	libc.Xfprintf(tls, file, __ccgo_ts+791, 0)
	k = 0
	for {
		if !(k < in_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+805, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(input + uintptr(k)*8))))
		goto _2
	_2:
		k++
	}
	libc.Xfprintf(tls, file, __ccgo_ts+810, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+763, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+779, libc.VaList(bp+8, out_len))
	libc.Xfprintf(tls, file, __ccgo_ts+791, 0)
	k = 0
	for {
		if !(k < out_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+805, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(output + uintptr(k)*8))))
		goto _3
	_3:
		k++
	}
	libc.Xfclose(tls, file)
	return
}

/* save_oct_double */

func x_interleave_data(tls *libc.TLS, in uintptr, out uintptr, frames int32, channels int32) {
	var ch, fr int32
	_, _ = ch, fr
	fr = 0
	for {
		if !(fr < frames) {
			break
		}
		ch = 0
		for {
			if !(ch < channels) {
				break
			}
			*(*float32)(unsafe.Pointer(out + uintptr(ch+channels*fr)*4)) = *(*float32)(unsafe.Pointer(in + uintptr(fr+frames*ch)*4))
			goto _2
		_2:
			ch++
		}
		goto _1
	_1:
		fr++
	}
	return
}

/* interleave_data */

func x_deinterleave_data(tls *libc.TLS, in uintptr, out uintptr, frames int32, channels int32) {
	var ch, fr int32
	_, _ = ch, fr
	ch = 0
	for {
		if !(ch < channels) {
			break
		}
		fr = 0
		for {
			if !(fr < frames) {
				break
			}
			*(*float32)(unsafe.Pointer(out + uintptr(fr+frames*ch)*4)) = *(*float32)(unsafe.Pointer(in + uintptr(ch+channels*fr)*4))
			goto _2
		_2:
			fr++
		}
		goto _1
	_1:
		ch++
	}
	return
}

/* deinterleave_data */

func x_reverse_data(tls *libc.TLS, data uintptr, datalen int32) {
	var left, right int32
	var temp float32
	_, _, _ = left, right, temp
	left = 0
	right = datalen - int32(1)
	for left < right {
		temp = *(*float32)(unsafe.Pointer(data + uintptr(left)*4))
		*(*float32)(unsafe.Pointer(data + uintptr(left)*4)) = *(*float32)(unsafe.Pointer(data + uintptr(right)*4))
		*(*float32)(unsafe.Pointer(data + uintptr(right)*4)) = temp
		left++
		right--
	}
}

/* reverse_data */

func x_get_cpu_name(tls *libc.TLS) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var dest, file, name, search, src, v1, v17, v25, v8 uintptr
	var is_pipe, v11, v14, v19, v2, v22, v5 int32
	var v13, v21, v4 T__ct_rune_t
	var v16, v24, v7 uint64
	var v27 bool
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = dest, file, is_pipe, name, search, src, v1, v11, v13, v14, v16, v17, v19, v2, v21, v22, v24, v25, v27, v4, v5, v7, v8
	name = __ccgo_ts + 826
	search = libc.UintptrFromInt32(0)
	file = libc.UintptrFromInt32(0)
	is_pipe = 0
	file = libc.Xpopen(tls, __ccgo_ts+834, __ccgo_ts+844)
	search = __ccgo_ts + 846
	is_pipe = int32(1)
	if search == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+855, libc.VaList(bp+8, uintptr(unsafe.Pointer(&___func__3))))
		return name
	}
	for libc.Xfgets(tls, uintptr(unsafe.Pointer(&_buffer)), int32(512), file) != libc.UintptrFromInt32(0) {
		if libc.Xstrstr(tls, uintptr(unsafe.Pointer(&_buffer)), search) != 0 {
			v1 = libc.Xstrchr(tls, uintptr(unsafe.Pointer(&_buffer)), int32(':'))
			src = v1
			if v1 != libc.UintptrFromInt32(0) {
				src++
				for {
					v4 = int32(*(*int8)(unsafe.Pointer(src)))
					if v4 < 0 || v4 >= libc.X__mb_sb_limit {
						v7 = uint64(0)
					} else {
						if libc.X_ThreadRuneLocale != 0 {
							v8 = libc.X_ThreadRuneLocale
							goto _9
						}
						v8 = libc.X_CurrentRuneLocale
						goto _9
					_9:
						v7 = *(*uint64)(unsafe.Pointer(v8 + 64 + uintptr(v4)*8)) & uint64(0x00004000)
					}
					v5 = int32(v7)
					goto _6
				_6:
					v2 = libc.BoolInt32(!!(v5 != 0))
					goto _3
				_3:
					if !(v2 != 0) {
						break
					}
					src++
				}
				name = src
				/* Remove consecutive spaces. */
				src++
				dest = src
				for {
					if !(*(*int8)(unsafe.Pointer(src)) != 0) {
						break
					}
					v13 = int32(*(*int8)(unsafe.Pointer(src)))
					if v13 < 0 || v13 >= libc.X__mb_sb_limit {
						v16 = uint64(0)
					} else {
						if libc.X_ThreadRuneLocale != 0 {
							v17 = libc.X_ThreadRuneLocale
							goto _18
						}
						v17 = libc.X_CurrentRuneLocale
						goto _18
					_18:
						v16 = *(*uint64)(unsafe.Pointer(v17 + 64 + uintptr(v13)*8)) & uint64(0x00004000)
					}
					v14 = int32(v16)
					goto _15
				_15:
					v11 = libc.BoolInt32(!!(v14 != 0))
					goto _12
				_12:
					if v27 = v11 != 0; v27 {
						v21 = int32(*(*int8)(unsafe.Pointer(dest + uintptr(-libc.Int32FromInt32(1)))))
						if v21 < 0 || v21 >= libc.X__mb_sb_limit {
							v24 = uint64(0)
						} else {
							if libc.X_ThreadRuneLocale != 0 {
								v25 = libc.X_ThreadRuneLocale
								goto _26
							}
							v25 = libc.X_CurrentRuneLocale
							goto _26
						_26:
							v24 = *(*uint64)(unsafe.Pointer(v25 + 64 + uintptr(v21)*8)) & uint64(0x00004000)
						}
						v22 = int32(v24)
						goto _23
					_23:
						v19 = libc.BoolInt32(!!(v22 != 0))
						goto _20
					_20:
					}
					if v27 && v19 != 0 {
						goto _10
					}
					*(*int8)(unsafe.Pointer(dest)) = *(*int8)(unsafe.Pointer(src))
					dest++
					goto _10
				_10:
					src++
				}
				*(*int8)(unsafe.Pointer(dest)) = 0
				break
			}
		}
	}
	if is_pipe != 0 {
		libc.Xpclose(tls, file)
	} else {
		libc.Xfclose(tls, file)
	}
	return name
}

var ___func__3 = [13]int8{'g', 'e', 't', '_', 'c', 'p', 'u', '_', 'n', 'a', 'm', 'e'}

var _buffer [512]int8

func x_calculate_snr(tls *libc.TLS, data uintptr, len1 int32, expected_peaks int32) (r float64) {
	var snr float64
	_ = snr
	snr = float64(200)
	data = data
	len1 = len1
	expected_peaks = expected_peaks
	return snr
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "\n    Varispeed SNR test\x00        Zero Order Hold interpolator    : \x00ok\x00        Linear interpolator             : \x00        Sinc interpolator               : \x00\n    Varispeed bounds test\x00\x00\n\nLine %d : src_new () failed : %s\n\n\x00\n\nLine %d : %s\n\n\x00  src_data.input_frames  : %ld\n\x00  src_data.output_frames : %ld\n\n\x00\n\nLine %d : unused input.\n\x00\tinput_len         : %d\n\x00\tinput_frames_used : %ld\n\n\x00\n\nLine %d : snr (%3.1f) does not meet target (%3.1f)\n\n\x00varispeed.mat\x00%d channels, ratio %g -> %g\x00\n\nLine %d : %s : %s\n\n\x00Condition failed on Line %d : %s\n\n\x00k < max_loop_count\x00total_frames_gen > 0\x00! isnan (output [k])\x00\n%s : Error : freq [%d] == %g is out of range. Should be < 0.5.\n\x00tests/util.c\x00Dumping input and output data to file : %s.\n\n\x00w\x00# Not created by Octave\n\x00# name: input\n\x00# type: matrix\n\x00# rows: %d\n\x00# columns: 1\n\x00% g\n\x00# name: output\n\x00Unknown\x00sysctl -a\x00r\x00hw.model\x00Error : search is NULL in function %s.\n\x00"
