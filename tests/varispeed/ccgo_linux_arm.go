// Code generated for linux/arm by 'gcc --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -w -o tests/.libs/varispeed_test.go tests/varispeed_test-varispeed_test.o.go tests/varispeed_test-util.o.go tests/varispeed_test-calc_snr.o.go -lm -lsamplerate', DO NOT EDIT.

//go:build linux && arm
// +build linux,arm

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libsamplerate"
)

var (
	_ reflect.Type
	_ unsafe.Pointer
)

const m_BIG_ENDIAN = "__BIG_ENDIAN"
const m_BUFSIZ = 8192
const m_BYTE_ORDER = "__BYTE_ORDER"
const m_CPU_CLIPS_NEGATIVE = 1
const m_CPU_CLIPS_POSITIVE = 1
const m_CPU_IS_BIG_ENDIAN = 0
const m_CPU_IS_LITTLE_ENDIAN = 1
const m_ENABLE_SINC_BEST_CONVERTER = "yes"
const m_ENABLE_SINC_FAST_CONVERTER = "yes"
const m_ENABLE_SINC_MEDIUM_CONVERTER = "yes"
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_FD_SETSIZE = "__FD_SETSIZE"
const m_FILENAME_MAX = 4096
const m_FOPEN_MAX = 16
const m_FP_ILOGBNAN = 2147483647
const m_FP_INFINITE = 1
const m_FP_NAN = 0
const m_FP_NORMAL = 4
const m_FP_SUBNORMAL = 3
const m_FP_ZERO = 2
const m_HAVE_ALARM = 1
const m_HAVE_CALLOC = 1
const m_HAVE_CEIL = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_FLOOR = 1
const m_HAVE_FMOD = 1
const m_HAVE_FREE = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_LRINT = 1
const m_HAVE_LRINTF = 1
const m_HAVE_MALLOC = 1
const m_HAVE_MEMCPY = 1
const m_HAVE_MEMMOVE = 1
const m_HAVE_SIGALRM = 1
const m_HAVE_SIGNAL = 1
const m_HAVE_STDBOOL_H = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TIMES_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_VISIBILITY = 1
const m_LITTLE_ENDIAN = "__LITTLE_ENDIAN"
const m_LT_OBJDIR = ".libs/"
const m_L_ctermid = 9
const m_L_tmpnam = 20
const m_MATH_ERREXCEPT = 2
const m_MATH_ERRNO = 1
const m_M_1_PI = 0.31830988618379067154
const m_M_2_PI = 0.63661977236758134308
const m_M_2_SQRTPI = 1.12837916709551257390
const m_M_E = 2.7182818284590452354
const m_M_LN10 = 2.30258509299404568402
const m_M_LN2 = 0.69314718055994530942
const m_M_LOG10E = 0.43429448190325182765
const m_M_LOG2E = 1.4426950408889634074
const m_M_PI = 3.14159265358979323846
const m_M_PI_2 = 1.57079632679489661923
const m_M_PI_4 = 0.78539816339744830962
const m_M_SQRT1_2 = 0.70710678118654752440
const m_M_SQRT2 = 1.41421356237309504880
const m_NFDBITS = "__NFDBITS"
const m_PACKAGE = "libsamplerate"
const m_PACKAGE_BUGREPORT = "erikd@mega-nerd.com"
const m_PACKAGE_NAME = "libsamplerate"
const m_PACKAGE_STRING = "libsamplerate 0.2.2"
const m_PACKAGE_TARNAME = "libsamplerate"
const m_PACKAGE_URL = "https://github.com/libsndfile/libsamplerate/"
const m_PACKAGE_VERSION = "0.2.2"
const m_PDP_ENDIAN = "__PDP_ENDIAN"
const m_P_tmpdir = "/tmp"
const m_RAND_MAX = 2147483647
const m_SEEK_CUR = 1
const m_SEEK_END = 2
const m_SEEK_SET = 0
const m_SIZEOF_DOUBLE = 8
const m_SIZEOF_FLOAT = 4
const m_SIZEOF_INT = 4
const m_SIZEOF_LONG = 4
const m_STDC_HEADERS = 1
const m_TMP_MAX = 238328
const m_VERSION = "0.2.2"
const m_WCONTINUED = 8
const m_WEXITED = 4
const m_WNOHANG = 1
const m_WNOWAIT = 0x01000000
const m_WSTOPPED = 2
const m_WUNTRACED = 2
const m__ALLOCA_H = 1
const m__ATFILE_SOURCE = 1
const m__BITS_BYTESWAP_H = 1
const m__BITS_ENDIANNESS_H = 1
const m__BITS_ENDIAN_H = 1
const m__BITS_LIBM_SIMD_DECL_STUBS_H = 1
const m__BITS_PTHREADTYPES_ARCH_H = 1
const m__BITS_PTHREADTYPES_COMMON_H = 1
const m__BITS_STDINT_INTN_H = 1
const m__BITS_STDIO_LIM_H = 1
const m__BITS_TIME64_H = 1
const m__BITS_TYPESIZES_H = 1
const m__BITS_TYPES_H = 1
const m__BITS_TYPES_LOCALE_T_H = 1
const m__BITS_TYPES___LOCALE_T_H = 1
const m__BITS_UINTN_IDENTITY_H = 1
const m__DEFAULT_SOURCE = 1
const m__ENDIAN_H = 1
const m__FEATURES_H = 1
const m__FILE_OFFSET_BITS = 64
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__IO_EOF_SEEN = 0x0010
const m__IO_ERR_SEEN = 0x0020
const m__IO_USER_LOCK = 0x8000
const m__MATH_H = 1
const m__POSIX_C_SOURCE = 200809
const m__POSIX_SOURCE = 1
const m__STDC_PREDEF_H = 1
const m__STDIO_H = 1
const m__STDLIB_H = 1
const m__STRINGS_H = 1
const m__STRING_H = 1
const m__STRUCT_TIMESPEC = 1
const m__SYS_CDEFS_H = 1
const m__SYS_SELECT_H = 1
const m__SYS_TYPES_H = 1
const m__THREAD_MUTEX_INTERNAL_H = 1
const m__THREAD_SHARED_TYPES_H = 1
const m___ACCUM_EPSILON__ = "0x1P-15K"
const m___ACCUM_FBIT__ = 15
const m___ACCUM_IBIT__ = 16
const m___ACCUM_MAX__ = "0X7FFFFFFFP-15K"
const m___APCS_32__ = 1
const m___ARMEL__ = 1
const m___ARM_32BIT_STATE = 1
const m___ARM_ARCH = 6
const m___ARM_ARCH_6__ = 1
const m___ARM_ARCH_ISA_ARM = 1
const m___ARM_ARCH_ISA_THUMB = 1
const m___ARM_EABI__ = 1
const m___ARM_FEATURE_CLZ = 1
const m___ARM_FEATURE_COPROC = 15
const m___ARM_FEATURE_DSP = 1
const m___ARM_FEATURE_LDREX = 4
const m___ARM_FEATURE_QBIT = 1
const m___ARM_FEATURE_SAT = 1
const m___ARM_FEATURE_SIMD32 = 1
const m___ARM_FEATURE_UNALIGNED = 1
const m___ARM_FP = 12
const m___ARM_PCS_VFP = 1
const m___ARM_SIZEOF_MINIMAL_ENUM = 4
const m___ARM_SIZEOF_WCHAR_T = 4
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 8
const m___BIG_ENDIAN = 4321
const m___BIT_TYPES_DEFINED__ = 1
const m___BLKCNT64_T_TYPE = "__SQUAD_TYPE"
const m___BLKCNT_T_TYPE = "__SLONGWORD_TYPE"
const m___BLKSIZE_T_TYPE = "__SLONGWORD_TYPE"
const m___BYTE_ORDER = "__LITTLE_ENDIAN"
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___CHAR_UNSIGNED__ = 1
const m___CLOCKID_T_TYPE = "__S32_TYPE"
const m___CLOCK_T_TYPE = "__SLONGWORD_TYPE"
const m___CPU_MASK_TYPE = "__ULONGWORD_TYPE"
const m___DADDR_T_TYPE = "__S32_TYPE"
const m___DA_FBIT__ = 31
const m___DA_IBIT__ = 32
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_IS_IEC_60559__ = 2
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DECIMAL_DIG__ = 17
const m___DEC_EVAL_METHOD__ = 2
const m___DEV_T_TYPE = "__UQUAD_TYPE"
const m___DQ_FBIT__ = 63
const m___DQ_IBIT__ = 0
const m___ELF__ = 1
const m___FD_SETSIZE = 1024
const m___FILE_defined = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT_WORD_ORDER = "__BYTE_ORDER"
const m___FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.2204460492503131e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_IS_IEC_60559__ = 2
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.7976931348623157e+308
const m___FLT32X_MIN__ = 2.2250738585072014e-308
const m___FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.1920928955078125e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_IS_IEC_60559__ = 2
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.4028234663852886e+38
const m___FLT32_MIN__ = 1.1754943508222875e-38
const m___FLT32_NORM_MAX__ = 3.4028234663852886e+38
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.2204460492503131e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_IS_IEC_60559__ = 2
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.7976931348623157e+308
const m___FLT64_MIN__ = 2.2250738585072014e-308
const m___FLT64_NORM_MAX__ = 1.7976931348623157e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.4012984643248171e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.1920928955078125e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_IS_IEC_60559__ = 2
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.4028234663852886e+38
const m___FLT_MIN__ = 1.1754943508222875e-38
const m___FLT_NORM_MAX__ = 3.4028234663852886e+38
const m___FLT_RADIX__ = 2
const m___FP_LOGB0_IS_MIN = 0
const m___FP_LOGBNAN_IS_MIN = 0
const m___FRACT_EPSILON__ = "0x1P-15R"
const m___FRACT_FBIT__ = 15
const m___FRACT_IBIT__ = 0
const m___FRACT_MAX__ = "0X7FFFP-15R"
const m___FSBLKCNT64_T_TYPE = "__UQUAD_TYPE"
const m___FSBLKCNT_T_TYPE = "__ULONGWORD_TYPE"
const m___FSFILCNT64_T_TYPE = "__UQUAD_TYPE"
const m___FSFILCNT_T_TYPE = "__ULONGWORD_TYPE"
const m___FSWORD_T_TYPE = "__SWORD_TYPE"
const m___FUNCTION__ = "__func__"
const m___GCC_ASM_FLAG_OUTPUTS__ = 1
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 1
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 1
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 1
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 1
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 1
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_CONSTRUCTIVE_SIZE = 64
const m___GCC_DESTRUCTIVE_SIZE = 64
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GID_T_TYPE = "__U32_TYPE"
const m___GLIBC_FLT_EVAL_METHOD = "__FLT_EVAL_METHOD__"
const m___GLIBC_MINOR__ = 36
const m___GLIBC_USE_DEPRECATED_GETS = 0
const m___GLIBC_USE_DEPRECATED_SCANF = 0
const m___GLIBC_USE_IEC_60559_BFP_EXT = 0
const m___GLIBC_USE_IEC_60559_BFP_EXT_C2X = 0
const m___GLIBC_USE_IEC_60559_EXT = 0
const m___GLIBC_USE_IEC_60559_FUNCS_EXT = 0
const m___GLIBC_USE_IEC_60559_FUNCS_EXT_C2X = 0
const m___GLIBC_USE_IEC_60559_TYPES_EXT = 0
const m___GLIBC_USE_ISOC2X = 0
const m___GLIBC_USE_LIB_EXT2 = 0
const m___GLIBC__ = 2
const m___GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 0
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const m___GNUC__ = 12
const m___GNU_LIBRARY__ = 6
const m___GXX_ABI_VERSION = 1017
const m___GXX_TYPEINFO_EQUALITY_INLINE = 0
const m___HAVE_DISTINCT_FLOAT128 = 0
const m___HAVE_DISTINCT_FLOAT128X = "__HAVE_FLOAT128X"
const m___HAVE_DISTINCT_FLOAT16 = "__HAVE_FLOAT16"
const m___HAVE_DISTINCT_FLOAT32 = 0
const m___HAVE_DISTINCT_FLOAT32X = 0
const m___HAVE_DISTINCT_FLOAT64 = 0
const m___HAVE_DISTINCT_FLOAT64X = 0
const m___HAVE_FLOAT128 = 0
const m___HAVE_FLOAT128X = 0
const m___HAVE_FLOAT16 = 0
const m___HAVE_FLOAT32 = 1
const m___HAVE_FLOAT32X = 1
const m___HAVE_FLOAT64 = 1
const m___HAVE_FLOAT64X = 0
const m___HAVE_FLOAT64X_LONG_DOUBLE = 0
const m___HAVE_FLOATN_NOT_TYPEDEF = 1
const m___HAVE_GENERIC_SELECTION = 1
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___HA_FBIT__ = 7
const m___HA_IBIT__ = 8
const m___HQ_FBIT__ = 15
const m___HQ_IBIT__ = 0
const m___ID_T_TYPE = "__U32_TYPE"
const m___INO64_T_TYPE = "__UQUAD_TYPE"
const m___INO_T_TYPE = "__ULONGWORD_TYPE"
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffff
const m___INTPTR_TYPE__ = "int"
const m___INTPTR_WIDTH__ = 32
const m___INT_FAST16_MAX__ = 0x7fffffff
const m___INT_FAST16_TYPE__ = "int"
const m___INT_FAST16_WIDTH__ = 32
const m___INT_FAST32_MAX__ = 0x7fffffff
const m___INT_FAST32_TYPE__ = "int"
const m___INT_FAST32_WIDTH__ = 32
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64 = 0
const m___KEY_T_TYPE = "__S32_TYPE"
const m___LACCUM_EPSILON__ = "0x1P-31LK"
const m___LACCUM_FBIT__ = 31
const m___LACCUM_IBIT__ = 32
const m___LACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LK"
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.2204460492503131e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_IS_IEC_60559__ = 2
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.7976931348623157e+308
const m___LDBL_MIN__ = 2.2250738585072014e-308
const m___LDBL_NORM_MAX__ = 1.7976931348623157e+308
const m___LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = 0
const m___LFRACT_EPSILON__ = "0x1P-31LR"
const m___LFRACT_FBIT__ = 31
const m___LFRACT_IBIT__ = 0
const m___LFRACT_MAX__ = "0X7FFFFFFFP-31LR"
const m___LITTLE_ENDIAN = 1234
const m___LLACCUM_EPSILON__ = "0x1P-31LLK"
const m___LLACCUM_FBIT__ = 31
const m___LLACCUM_IBIT__ = 32
const m___LLACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LLK"
const m___LLFRACT_EPSILON__ = "0x1P-63LLR"
const m___LLFRACT_FBIT__ = 63
const m___LLFRACT_IBIT__ = 0
const m___LLFRACT_MAX__ = "0X7FFFFFFFFFFFFFFFP-63LLR"
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX__ = 0x7fffffff
const m___LONG_WIDTH__ = 32
const m___MATH_DECLARE_LDOUBLE = 1
const m___MODE_T_TYPE = "__U32_TYPE"
const m___NLINK_T_TYPE = "__UWORD_TYPE"
const m___NO_INLINE__ = 1
const m___NO_LONG_DOUBLE_MATH = 1
const m___OFF64_T_TYPE = "__SQUAD_TYPE"
const m___OFF_T_TYPE = "__SLONGWORD_TYPE"
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PDP_ENDIAN = 3412
const m___PID_T_TYPE = "__S32_TYPE"
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTHREAD_MUTEX_HAVE_PREV = 0
const m___PTRDIFF_MAX__ = 0x7fffffff
const m___PTRDIFF_TYPE__ = "int"
const m___PTRDIFF_WIDTH__ = 32
const m___QQ_FBIT__ = 7
const m___QQ_IBIT__ = 0
const m___RLIM64_T_TYPE = "__UQUAD_TYPE"
const m___RLIM_T_MATCHES_RLIM64_T = 0
const m___RLIM_T_TYPE = "__ULONGWORD_TYPE"
const m___S32_TYPE = "int"
const m___S64_TYPE = "__int64_t"
const m___SACCUM_EPSILON__ = "0x1P-7HK"
const m___SACCUM_FBIT__ = 7
const m___SACCUM_IBIT__ = 8
const m___SACCUM_MAX__ = "0X7FFFP-7HK"
const m___SA_FBIT__ = 15
const m___SA_IBIT__ = 16
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SFRACT_EPSILON__ = "0x1P-7HR"
const m___SFRACT_FBIT__ = 7
const m___SFRACT_IBIT__ = 0
const m___SFRACT_MAX__ = "0X7FP-7HR"
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 4
const m___SIZEOF_POINTER__ = 4
const m___SIZEOF_PTHREAD_ATTR_T = 36
const m___SIZEOF_PTHREAD_BARRIERATTR_T = 4
const m___SIZEOF_PTHREAD_BARRIER_T = 20
const m___SIZEOF_PTHREAD_CONDATTR_T = 4
const m___SIZEOF_PTHREAD_COND_T = 48
const m___SIZEOF_PTHREAD_MUTEXATTR_T = 4
const m___SIZEOF_PTHREAD_MUTEX_T = 24
const m___SIZEOF_PTHREAD_RWLOCKATTR_T = 8
const m___SIZEOF_PTHREAD_RWLOCK_T = 32
const m___SIZEOF_PTRDIFF_T__ = 4
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 4
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffff
const m___SIZE_WIDTH__ = 32
const m___SQUAD_TYPE = "__int64_t"
const m___SQ_FBIT__ = 31
const m___SQ_IBIT__ = 0
const m___SSIZE_T_TYPE = "__SWORD_TYPE"
const m___STATFS_MATCHES_STATFS64 = 0
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_IEC_60559_BFP__ = 201404
const m___STDC_IEC_60559_COMPLEX__ = 201404
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC__ = 1
const m___SUSECONDS64_T_TYPE = "__SQUAD_TYPE"
const m___SUSECONDS_T_TYPE = "__SLONGWORD_TYPE"
const m___SWORD_TYPE = "int"
const m___SYSCALL_SLONG_TYPE = "__SLONGWORD_TYPE"
const m___SYSCALL_ULONG_TYPE = "__ULONGWORD_TYPE"
const m___TA_FBIT__ = 63
const m___TA_IBIT__ = 64
const m___THUMB_INTERWORK__ = 1
const m___TIME64_T_TYPE = "__SQUAD_TYPE"
const m___TIMESIZE = 32
const m___TIME_T_TYPE = "__SLONGWORD_TYPE"
const m___TQ_FBIT__ = 127
const m___TQ_IBIT__ = 0
const m___U64_TYPE = "__uint64_t"
const m___UACCUM_EPSILON__ = "0x1P-16UK"
const m___UACCUM_FBIT__ = 16
const m___UACCUM_IBIT__ = 16
const m___UACCUM_MAX__ = "0XFFFFFFFFP-16UK"
const m___UACCUM_MIN__ = "0.0UK"
const m___UDA_FBIT__ = 32
const m___UDA_IBIT__ = 32
const m___UDQ_FBIT__ = 64
const m___UDQ_IBIT__ = 0
const m___UFRACT_EPSILON__ = "0x1P-16UR"
const m___UFRACT_FBIT__ = 16
const m___UFRACT_IBIT__ = 0
const m___UFRACT_MAX__ = "0XFFFFP-16UR"
const m___UFRACT_MIN__ = "0.0UR"
const m___UHA_FBIT__ = 8
const m___UHA_IBIT__ = 8
const m___UHQ_FBIT__ = 16
const m___UHQ_IBIT__ = 0
const m___UID_T_TYPE = "__U32_TYPE"
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = "0xffffffffffffffffU"
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = "0xffffffffffffffffU"
const m___UINTPTR_MAX__ = 0xffffffff
const m___UINT_FAST16_MAX__ = 0xffffffff
const m___UINT_FAST32_MAX__ = 0xffffffff
const m___UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const m___UINT_LEAST8_MAX__ = 0xff
const m___ULACCUM_EPSILON__ = "0x1P-32ULK"
const m___ULACCUM_FBIT__ = 32
const m___ULACCUM_IBIT__ = 32
const m___ULACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULK"
const m___ULACCUM_MIN__ = "0.0ULK"
const m___ULFRACT_EPSILON__ = "0x1P-32ULR"
const m___ULFRACT_FBIT__ = 32
const m___ULFRACT_IBIT__ = 0
const m___ULFRACT_MAX__ = "0XFFFFFFFFP-32ULR"
const m___ULFRACT_MIN__ = "0.0ULR"
const m___ULLACCUM_EPSILON__ = "0x1P-32ULLK"
const m___ULLACCUM_FBIT__ = 32
const m___ULLACCUM_IBIT__ = 32
const m___ULLACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULLK"
const m___ULLACCUM_MIN__ = "0.0ULLK"
const m___ULLFRACT_EPSILON__ = "0x1P-64ULLR"
const m___ULLFRACT_FBIT__ = 64
const m___ULLFRACT_IBIT__ = 0
const m___ULLFRACT_MAX__ = "0XFFFFFFFFFFFFFFFFP-64ULLR"
const m___ULLFRACT_MIN__ = "0.0ULLR"
const m___UQQ_FBIT__ = 8
const m___UQQ_IBIT__ = 0
const m___UQUAD_TYPE = "__uint64_t"
const m___USACCUM_EPSILON__ = "0x1P-8UHK"
const m___USACCUM_FBIT__ = 8
const m___USACCUM_IBIT__ = 8
const m___USACCUM_MAX__ = "0XFFFFP-8UHK"
const m___USACCUM_MIN__ = "0.0UHK"
const m___USA_FBIT__ = 16
const m___USA_IBIT__ = 16
const m___USECONDS_T_TYPE = "__U32_TYPE"
const m___USE_ATFILE = 1
const m___USE_FILE_OFFSET64 = 1
const m___USE_FORTIFY_LEVEL = 0
const m___USE_ISOC11 = 1
const m___USE_ISOC95 = 1
const m___USE_ISOC99 = 1
const m___USE_MISC = 1
const m___USE_POSIX = 1
const m___USE_POSIX199309 = 1
const m___USE_POSIX199506 = 1
const m___USE_POSIX2 = 1
const m___USE_POSIX_IMPLICITLY = 1
const m___USE_XOPEN2K = 1
const m___USE_XOPEN2K8 = 1
const m___USFRACT_EPSILON__ = "0x1P-8UHR"
const m___USFRACT_FBIT__ = 8
const m___USFRACT_IBIT__ = 0
const m___USFRACT_MAX__ = "0XFFP-8UHR"
const m___USFRACT_MIN__ = "0.0UHR"
const m___USQ_FBIT__ = 32
const m___USQ_IBIT__ = 0
const m___UTA_FBIT__ = 64
const m___UTA_IBIT__ = 64
const m___UTQ_FBIT__ = 128
const m___UTQ_IBIT__ = 0
const m___VERSION__ = "12.2.0"
const m___VFP_FP__ = 1
const m___WALL = 0x40000000
const m___WCHAR_MAX__ = 0xffffffff
const m___WCHAR_MIN__ = 0
const m___WCHAR_WIDTH__ = 32
const m___WCLONE = 0x80000000
const m___WCOREFLAG = 0x80
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___WNOTHREAD = 0x20000000
const m___WORDSIZE = 32
const m___WORDSIZE32_PTRDIFF_LONG = 0
const m___WORDSIZE32_SIZE_ULONG = 0
const m___WORDSIZE_TIME64_COMPAT32 = 0
const m___W_CONTINUED = 0xffff
const m_____FILE_defined = 1
const m______fpos64_t_defined = 1
const m______fpos_t_defined = 1
const m_____mbstate_t_defined = 1
const m___arm__ = 1
const m___clock_t_defined = 1
const m___clockid_t_defined = 1
const m___glibc_c99_flexarr_available = 1
const m___gnu_linux__ = 1
const m___have_pthread_attr_t = 1
const m___ldiv_t_defined = 1
const m___linux = 1
const m___linux__ = 1
const m___lldiv_t_defined = 1
const m___sigset_t_defined = 1
const m___struct_FILE_defined = 1
const m___time_t_defined = 1
const m___timer_t_defined = 1
const m___timeval_defined = 1
const m___unix = 1
const m___unix__ = 1
const m_linux = 1
const m_unix = 1

type T__builtin_va_list = uintptr

type T__predefined_size_t = uint32

type T__predefined_wchar_t = uint32

type T__predefined_ptrdiff_t = int32

type Tsize_t = uint32

type T__gnuc_va_list = uintptr

type T__u_char = uint8

type T__u_short = uint16

type T__u_int = uint32

type T__u_long = uint32

type T__int8_t = int8

type T__uint8_t = uint8

type T__int16_t = int16

type T__uint16_t = uint16

type T__int32_t = int32

type T__uint32_t = uint32

type T__int64_t = int64

type T__uint64_t = uint64

type T__int_least8_t = int8

type T__uint_least8_t = uint8

type T__int_least16_t = int16

type T__uint_least16_t = uint16

type T__int_least32_t = int32

type T__uint_least32_t = uint32

type T__int_least64_t = int64

type T__uint_least64_t = uint64

type T__quad_t = int64

type T__u_quad_t = uint64

type T__intmax_t = int64

type T__uintmax_t = uint64

type T__dev_t = uint64

type T__uid_t = uint32

type T__gid_t = uint32

type T__ino_t = uint32

type T__ino64_t = uint64

type T__mode_t = uint32

type T__nlink_t = uint32

type T__off_t = int32

type T__off64_t = int64

type T__pid_t = int32

type T__fsid_t = struct {
	F__val [2]int32
}

type T__clock_t = int32

type T__rlim_t = uint32

type T__rlim64_t = uint64

type T__id_t = uint32

type T__time_t = int32

type T__useconds_t = uint32

type T__suseconds_t = int32

type T__suseconds64_t = int64

type T__daddr_t = int32

type T__key_t = int32

type T__clockid_t = int32

type T__timer_t = uintptr

type T__blksize_t = int32

type T__blkcnt_t = int32

type T__blkcnt64_t = int64

type T__fsblkcnt_t = uint32

type T__fsblkcnt64_t = uint64

type T__fsfilcnt_t = uint32

type T__fsfilcnt64_t = uint64

type T__fsword_t = int32

type T__ssize_t = int32

type T__syscall_slong_t = int32

type T__syscall_ulong_t = uint32

type T__loff_t = int64

type T__caddr_t = uintptr

type T__intptr_t = int32

type T__socklen_t = uint32

type T__sig_atomic_t = int32

type T__time64_t = int64

type T__mbstate_t = struct {
	F__count int32
	F__value struct {
		F__wchb [0][4]uint8
		F__wch  uint32
	}
}

type T__fpos_t = struct {
	F__pos   T__off_t
	F__state T__mbstate_t
}

type T_G_fpos_t = T__fpos_t

type T__fpos64_t = struct {
	F__ccgo_align [0]uint32
	F__pos        T__off64_t
	F__state      T__mbstate_t
}

type T_G_fpos64_t = T__fpos64_t

type T_IO_FILE = struct {
	F__ccgo_align    [0]uint32
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     T__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]uint8
	F_lock           uintptr
	F__ccgo_align21  [4]byte
	F_offset         T__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [40]uint8
}

type T__FILE = struct {
	F__ccgo_align    [0]uint32
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     T__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]uint8
	F_lock           uintptr
	F__ccgo_align21  [4]byte
	F_offset         T__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [40]uint8
}

type TFILE = struct {
	F__ccgo_align    [0]uint32
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     T__off_t
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]uint8
	F_lock           uintptr
	F__ccgo_align21  [4]byte
	F_offset         T__off64_t
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Tsize_t
	F_mode           int32
	F_unused2        [40]uint8
}

type T_IO_lock_t = struct{}

type Tva_list = uintptr

type Toff_t = int64

type Tssize_t = int32

type Tfpos_t = struct {
	F__ccgo_align [0]uint32
	F__pos        T__off64_t
	F__state      T__mbstate_t
}

type Twchar_t = uint32

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tlldiv_t = struct {
	F__ccgo_align [0]uint32
	Fquot         int64
	Frem          int64
}

type Tu_char = uint8

type Tu_short = uint16

type Tu_int = uint32

type Tu_long = uint32

type Tquad_t = int64

type Tu_quad_t = uint64

type Tfsid_t = struct {
	F__val [2]int32
}

type Tloff_t = int64

type Tino_t = uint64

type Tdev_t = uint64

type Tgid_t = uint32

type Tmode_t = uint32

type Tnlink_t = uint32

type Tuid_t = uint32

type Tpid_t = int32

type Tid_t = uint32

type Tdaddr_t = int32

type Tcaddr_t = uintptr

type Tkey_t = int32

type Tclock_t = int32

type Tclockid_t = int32

type Ttime_t = int32

type Ttimer_t = uintptr

type Tulong = uint32

type Tushort = uint16

type Tuint = uint32

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tu_int64_t = uint64

type Tregister_t = int32

type T__sigset_t = struct {
	F__val [32]uint32
}

type Tsigset_t = struct {
	F__val [32]uint32
}

type Ttimeval = struct {
	Ftv_sec  T__time_t
	Ftv_usec T__suseconds_t
}

type Ttimespec = struct {
	Ftv_sec  T__time_t
	Ftv_nsec T__syscall_slong_t
}

type Tsuseconds_t = int32

type T__fd_mask = int32

type Tfd_set = struct {
	F__fds_bits [32]T__fd_mask
}

type Tfd_mask = int32

type Tblksize_t = int32

type Tblkcnt_t = int64

type Tfsblkcnt_t = uint64

type Tfsfilcnt_t = uint64

type T__atomic_wide_counter = struct {
	F__ccgo_align [0]uint32
	F__value32    [0]struct {
		F__low  uint32
		F__high uint32
	}
	F__value64 uint64
}

type T__pthread_list_t = struct {
	F__prev uintptr
	F__next uintptr
}

type T__pthread_internal_list = T__pthread_list_t

type T__pthread_slist_t = struct {
	F__next uintptr
}

type T__pthread_internal_slist = T__pthread_slist_t

type T__pthread_mutex_s = struct {
	F__lock     int32
	F__count    uint32
	F__owner    int32
	F__kind     int32
	F__nusers   uint32
	F__ccgo5_20 struct {
		F__list  [0]T__pthread_slist_t
		F__spins int32
	}
}

type T__pthread_rwlock_arch_t = struct {
	F__readers       uint32
	F__writers       uint32
	F__wrphase_futex uint32
	F__writers_futex uint32
	F__pad3          uint32
	F__pad4          uint32
	F__flags         uint8
	F__shared        uint8
	F__pad1          uint8
	F__pad2          uint8
	F__cur_writer    int32
}

type T__pthread_cond_s = struct {
	F__ccgo_align   [0]uint32
	F__wseq         T__atomic_wide_counter
	F__g1_start     T__atomic_wide_counter
	F__g_refs       [2]uint32
	F__g_size       [2]uint32
	F__g1_orig_size uint32
	F__wrefs        uint32
	F__g_signals    [2]uint32
}

type T__tss_t = uint32

type T__thrd_t = uint32

type T__once_flag = struct {
	F__data int32
}

type Tpthread_t = uint32

type Tpthread_mutexattr_t = struct {
	F__align [0]int32
	F__size  [4]uint8
}

type Tpthread_condattr_t = struct {
	F__align [0]int32
	F__size  [4]uint8
}

type Tpthread_key_t = uint32

type Tpthread_once_t = int32

type Tpthread_attr_t1 = struct {
	F__align [0]int32
	F__size  [36]uint8
}

type Tpthread_attr_t = struct {
	F__align [0]int32
	F__size  [36]uint8
}

type Tpthread_mutex_t = struct {
	F__size  [0][24]uint8
	F__align [0]int32
	F__data  T__pthread_mutex_s
}

type Tpthread_cond_t = struct {
	F__ccgo_align [0]uint32
	F__size       [0][48]uint8
	F__align      [0]int64
	F__data       T__pthread_cond_s
}

type Tpthread_rwlock_t = struct {
	F__size  [0][32]uint8
	F__align [0]int32
	F__data  T__pthread_rwlock_arch_t
}

type Tpthread_rwlockattr_t = struct {
	F__align [0]int32
	F__size  [8]uint8
}

type Tpthread_spinlock_t = int32

type Tpthread_barrier_t = struct {
	F__align [0]int32
	F__size  [20]uint8
}

type Tpthread_barrierattr_t = struct {
	F__align [0]int32
	F__size  [4]uint8
}

type Trandom_data = struct {
	Ffptr      uintptr
	Frptr      uintptr
	Fstate     uintptr
	Frand_type int32
	Frand_deg  int32
	Frand_sep  int32
	Fend_ptr   uintptr
}

type Tdrand48_data = struct {
	F__ccgo_align [0]uint32
	F__x          [3]uint16
	F__old_x      [3]uint16
	F__c          uint16
	F__init       uint16
	F__a          uint64
}

type T__compar_fn_t = uintptr

type Tfloat_t = float32

type Tdouble_t = float64

const FP_NAN = 0
const FP_INFINITE = 1
const FP_ZERO = 2
const FP_SUBNORMAL = 3
const FP_NORMAL = 4

type T__locale_struct = struct {
	F__locales       [13]uintptr
	F__ctype_b       uintptr
	F__ctype_tolower uintptr
	F__ctype_toupper uintptr
	F__names         [13]uintptr
}

type T__locale_t = uintptr

type Tlocale_t = uintptr

type TSRC_DATA = struct {
	F__ccgo_align      [0]uint32
	Fdata_in           uintptr
	Fdata_out          uintptr
	Finput_frames      int32
	Foutput_frames     int32
	Finput_frames_used int32
	Foutput_frames_gen int32
	Fend_of_input      int32
	F__ccgo_align7     [4]byte
	Fsrc_ratio         float64
}

type Tsrc_callback_t = uintptr

const SRC_SINC_BEST_QUALITY = 0
const SRC_SINC_MEDIUM_QUALITY = 1
const SRC_SINC_FASTEST = 2
const SRC_ZERO_ORDER_HOLD = 3
const SRC_LINEAR = 4

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	libc.Xputs(tls, __ccgo_ts)
	libc.Xprintf(tls, __ccgo_ts+24, 0)
	libc.Xfflush(tls, libc.Xstdout)
	_varispeed_test(tls, SRC_ZERO_ORDER_HOLD, float64(10))
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xprintf(tls, __ccgo_ts+70, 0)
	libc.Xfflush(tls, libc.Xstdout)
	_varispeed_test(tls, SRC_LINEAR, float64(10))
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xprintf(tls, __ccgo_ts+113, 0)
	libc.Xfflush(tls, libc.Xstdout)
	_varispeed_test(tls, SRC_SINC_FASTEST, float64(115))
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xputs(tls, __ccgo_ts+156)
	libc.Xprintf(tls, __ccgo_ts+24, 0)
	libc.Xfflush(tls, libc.Xstdout)
	_varispeed_bounds_test(tls, SRC_ZERO_ORDER_HOLD)
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xprintf(tls, __ccgo_ts+70, 0)
	libc.Xfflush(tls, libc.Xstdout)
	_varispeed_bounds_test(tls, SRC_LINEAR)
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xprintf(tls, __ccgo_ts+113, 0)
	libc.Xfflush(tls, libc.Xstdout)
	_varispeed_bounds_test(tls, SRC_SINC_FASTEST)
	libc.Xputs(tls, __ccgo_ts+67)
	libc.Xputs(tls, __ccgo_ts+183)
	return 0
}

/* main */

func _varispeed_test(tls *libc.TLS, converter int32, target_snr float64) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var input_len, v2, v3, v4, v5, v6, v7 int32
	var snr float64
	var src_state, v1 uintptr
	var _ /* error at bp+48 */ int32
	var _ /* sine_freq at bp+0 */ float64
	var _ /* src_data at bp+8 */ TSRC_DATA
	_, _, _, _, _, _, _, _, _, _ = input_len, snr, src_state, v1, v2, v3, v4, v5, v6, v7
	libc.Xmemset(tls, uintptr(unsafe.Pointer(&_input)), 0, uint32(65536))
	input_len = int32(libc.Uint32FromInt64(65536)/libc.Uint32FromInt64(4)) / libc.Int32FromInt32(2)
	*(*float64)(unsafe.Pointer(bp)) = float64(0.0111)
	x_gen_windowed_sines(tls, int32(1), bp, float64(1), uintptr(unsafe.Pointer(&_input)), input_len)
	/* Perform sample rate conversion. */
	v1 = libsamplerate.Xsrc_new(tls, converter, int32(1), bp+48)
	src_state = v1
	if v1 == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+184, libc.VaList(bp+64, int32(98), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 48)))))
		libc.Xexit(tls, int32(1))
	}
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fend_of_input = int32(1)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fdata_in = uintptr(unsafe.Pointer(&_input))
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames = input_len
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fsrc_ratio = float64(3)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fdata_out = uintptr(unsafe.Pointer(&_output))
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames = int32(libc.Uint32FromInt64(65536) / libc.Uint32FromInt64(4))
	v2 = libsamplerate.Xsrc_set_ratio(tls, src_state, float64(1)/(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fsrc_ratio)
	*(*int32)(unsafe.Pointer(bp + 48)) = v2
	if v2 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+64, int32(113), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 48)))))
		libc.Xexit(tls, int32(1))
	}
	v3 = libsamplerate.Xsrc_process(tls, src_state, bp+8)
	*(*int32)(unsafe.Pointer(bp + 48)) = v3
	if v3 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+64, int32(118), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 48)))))
		libc.Xprintf(tls, __ccgo_ts+238, libc.VaList(bp+64, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames))
		libc.Xprintf(tls, __ccgo_ts+270, libc.VaList(bp+64, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames))
		libc.Xexit(tls, int32(1))
	}
	if (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames_used != input_len {
		libc.Xprintf(tls, __ccgo_ts+303, libc.VaList(bp+64, int32(125)))
		libc.Xprintf(tls, __ccgo_ts+330, libc.VaList(bp+64, input_len))
		libc.Xprintf(tls, __ccgo_ts+355, libc.VaList(bp+64, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames_used))
		libc.Xexit(tls, int32(1))
	}
	/* Copy the last output to the input. */
	libc.Xmemcpy(tls, uintptr(unsafe.Pointer(&_input)), uintptr(unsafe.Pointer(&_output)), uint32(65536))
	x_reverse_data(tls, uintptr(unsafe.Pointer(&_input)), (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames_gen)
	v4 = libsamplerate.Xsrc_reset(tls, src_state)
	*(*int32)(unsafe.Pointer(bp + 48)) = v4
	if v4 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+64, int32(136), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 48)))))
		libc.Xexit(tls, int32(1))
	}
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fend_of_input = int32(1)
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fdata_in = uintptr(unsafe.Pointer(&_input))
	v5 = (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames_gen
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames = v5
	input_len = v5
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fdata_out = uintptr(unsafe.Pointer(&_output))
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames = int32(libc.Uint32FromInt64(65536) / libc.Uint32FromInt64(4))
	v6 = libsamplerate.Xsrc_set_ratio(tls, src_state, float64(1)/(*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Fsrc_ratio)
	*(*int32)(unsafe.Pointer(bp + 48)) = v6
	if v6 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+64, int32(149), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 48)))))
		libc.Xexit(tls, int32(1))
	}
	v7 = libsamplerate.Xsrc_process(tls, src_state, bp+8)
	*(*int32)(unsafe.Pointer(bp + 48)) = v7
	if v7 != 0 {
		libc.Xprintf(tls, __ccgo_ts+221, libc.VaList(bp+64, int32(154), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 48)))))
		libc.Xprintf(tls, __ccgo_ts+238, libc.VaList(bp+64, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames))
		libc.Xprintf(tls, __ccgo_ts+270, libc.VaList(bp+64, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames))
		libc.Xexit(tls, int32(1))
	}
	if (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames_used != input_len {
		libc.Xprintf(tls, __ccgo_ts+303, libc.VaList(bp+64, int32(161)))
		libc.Xprintf(tls, __ccgo_ts+330, libc.VaList(bp+64, input_len))
		libc.Xprintf(tls, __ccgo_ts+355, libc.VaList(bp+64, (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames_used))
		libc.Xexit(tls, int32(1))
	}
	src_state = libsamplerate.Xsrc_delete(tls, src_state)
	snr = x_calculate_snr(tls, uintptr(unsafe.Pointer(&_output)), (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames_gen, int32(1))
	if target_snr > snr {
		libc.Xprintf(tls, __ccgo_ts+382, libc.VaList(bp+64, int32(172), snr, target_snr))
		x_save_oct_float(tls, __ccgo_ts+437, uintptr(unsafe.Pointer(&_input)), (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Finput_frames, uintptr(unsafe.Pointer(&_output)), (*(*TSRC_DATA)(unsafe.Pointer(bp + 8))).Foutput_frames_gen)
		libc.Xexit(tls, int32(1))
	}
	return
}

var _input [16384]float32

var _output [16384]float32

/* varispeed_test */

func _varispeed_bounds_test(tls *libc.TLS, converter int32) {
	var chan1, r1, r2 int32
	var ratios [3]float64
	_, _, _, _ = chan1, r1, r2, ratios
	ratios = [3]float64{
		0: float64(0.1),
		1: float64(0.01),
		2: libc.Float64FromInt32(20),
	}
	chan1 = int32(1)
	for {
		if !(chan1 <= int32(9)) {
			break
		}
		r1 = 0
		for {
			if !(r1 < int32(libc.Uint32FromInt64(24)/libc.Uint32FromInt64(8))) {
				break
			}
			r2 = 0
			for {
				if !(r2 < int32(libc.Uint32FromInt64(24)/libc.Uint32FromInt64(8))) {
					break
				}
				if r1 != r2 {
					_set_ratio_test(tls, converter, chan1, ratios[r1], ratios[r2])
				}
				goto _3
			_3:
				r2++
			}
			goto _2
		_2:
			r1++
		}
		goto _1
	_1:
		chan1++
	}
}

/* varispeed_bounds_test */

func _set_ratio_test(tls *libc.TLS, converter int32, channels int32, initial_ratio float64, second_ratio float64) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var chunk_size, k, max_loop_count, total_frames_gen, total_frames_used, total_input_frames, total_output_frames, v3, v4, v5 int32
	var input, output, src_state, v1 uintptr
	var _ /* details at bp+0 */ [128]uint8
	var _ /* error at bp+168 */ int32
	var _ /* src_data at bp+128 */ TSRC_DATA
	_, _, _, _, _, _, _, _, _, _, _, _, _, _ = chunk_size, input, k, max_loop_count, output, src_state, total_frames_gen, total_frames_used, total_input_frames, total_output_frames, v1, v3, v4, v5
	total_input_frames = libc.Int32FromInt32(1) << libc.Int32FromInt32(14)
	/* Maximum upsample ratio is 20, use a value beigger. */
	total_output_frames = libc.Int32FromInt32(25) * (libc.Int32FromInt32(1) << libc.Int32FromInt32(14))
	/* Interested in array boundary conditions, so all zero data here is fine. */
	input = libc.Xcalloc(tls, uint32(total_input_frames*channels), uint32(4))
	output = libc.Xcalloc(tls, uint32(total_output_frames*channels), uint32(4))
	max_loop_count = int32(100000)
	chunk_size = int32(128)
	libc.X__builtin_snprintf(tls, bp, uint32(128), __ccgo_ts+451, libc.VaList(bp+184, channels, initial_ratio, second_ratio))
	v1 = libsamplerate.Xsrc_new(tls, converter, channels, bp+168)
	src_state = v1
	if v1 == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+184, libc.VaList(bp+184, int32(215), libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 168)))))
		libc.Xexit(tls, int32(1))
	}
	total_frames_used = 0
	total_frames_gen = 0
	libc.Xmemset(tls, bp+128, 0, uint32(40))
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fend_of_input = 0
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fsrc_ratio = initial_ratio
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fdata_in = input
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fdata_out = output
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Finput_frames = chunk_size
	(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames = total_output_frames
	/* Use a max_loop_count here to enable the detection of infinite loops
	** (due to end of input not being detected.
	 */
	k = 0
	for {
		if !(k < max_loop_count) {
			break
		}
		if k == int32(1) {
			/* Hard switch to second_ratio after processing one chunk. */
			(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fsrc_ratio = second_ratio
			v3 = libsamplerate.Xsrc_set_ratio(tls, src_state, second_ratio)
			*(*int32)(unsafe.Pointer(bp + 168)) = v3
			if v3 != 0 {
				libc.Xprintf(tls, __ccgo_ts+479, libc.VaList(bp+184, int32(238), bp, libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 168)))))
				libc.Xexit(tls, int32(1))
			}
		}
		v4 = libsamplerate.Xsrc_process(tls, src_state, bp+128)
		*(*int32)(unsafe.Pointer(bp + 168)) = v4
		if v4 != 0 {
			libc.Xprintf(tls, __ccgo_ts+479, libc.VaList(bp+184, int32(244), bp, libsamplerate.Xsrc_strerror(tls, *(*int32)(unsafe.Pointer(bp + 168)))))
			libc.Xexit(tls, int32(1))
		}
		if (*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fend_of_input != 0 && (*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames_gen == 0 {
			break
		}
		total_frames_used = total_frames_used + (*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Finput_frames_used
		total_frames_gen = total_frames_gen + (*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames_gen
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fdata_in += uintptr((*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Finput_frames_used*channels) * 4
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fdata_out += uintptr((*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames_gen*channels) * 4
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Finput_frames = total_input_frames - total_frames_used
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Foutput_frames = total_output_frames - total_frames_gen
		if total_frames_used >= total_input_frames {
			v5 = int32(1)
		} else {
			v5 = 0
		}
		(*(*TSRC_DATA)(unsafe.Pointer(bp + 128))).Fend_of_input = v5
		goto _2
	_2:
		k++
	}
	if !(k < max_loop_count) {
		libc.Xprintf(tls, __ccgo_ts+501, libc.VaList(bp+184, int32(263), __ccgo_ts+536))
		libc.Xexit(tls, int32(1))
	}
	if !(total_frames_gen > libc.Int32FromInt32(0)) {
		libc.Xprintf(tls, __ccgo_ts+501, libc.VaList(bp+184, int32(264), __ccgo_ts+555))
		libc.Xexit(tls, int32(1))
	}
	k = 0
	for {
		if !(k < total_frames_gen*channels) {
			break
		}
		if !!(libc.X__builtin_isnan(tls, float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4)))) != 0) {
			libc.Xprintf(tls, __ccgo_ts+501, libc.VaList(bp+184, int32(267), __ccgo_ts+576))
			libc.Xexit(tls, int32(1))
		}
		goto _6
	_6:
		k++
	}
	src_state = libsamplerate.Xsrc_delete(tls, src_state)
	libc.Xfree(tls, input)
	libc.Xfree(tls, output)
	return
}

/* set_ratio_test */

func main() {
	libc.Start(x_main)
}

const m_M_PI1 = 3.141592653589793
const m__CTYPE_H = 1

const _ISupper = 256
const _ISlower = 512
const _ISalpha = 1024
const _ISdigit = 2048
const _ISxdigit = 4096
const _ISspace = 8192
const _ISprint = 16384
const _ISgraph = 32768
const _ISblank = 1
const _IScntrl = 2
const _ISpunct = 4
const _ISalnum = 8

func x_gen_windowed_sines(tls *libc.TLS, freq_count int32, freqs uintptr, max float64, output uintptr, output_len int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var amplitude, phase float64
	var freq, k int32
	_, _, _, _ = amplitude, freq, k, phase
	amplitude = max / float64(freq_count)
	k = 0
	for {
		if !(k < output_len) {
			break
		}
		*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(0)
		goto _1
	_1:
		k++
	}
	freq = 0
	for {
		if !(freq < freq_count) {
			break
		}
		phase = libc.Float64FromFloat64(0.9) * libc.Float64FromFloat64(3.141592653589793) / float64(freq_count)
		if *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8)) <= float64(0) || *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8)) >= float64(0.5) {
			libc.Xprintf(tls, __ccgo_ts+597, libc.VaList(bp+8, __ccgo_ts+662, freq, *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8))))
			libc.Xexit(tls, int32(1))
		}
		k = 0
		for {
			if !(k < output_len) {
				break
			}
			*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4))) + amplitude*libc.Xsin(tls, *(*float64)(unsafe.Pointer(freqs + uintptr(freq)*8))*float64(libc.Int32FromInt32(2)*k)*float64(3.141592653589793)+phase))
			goto _3
		_3:
			k++
		}
		goto _2
	_2:
		freq++
	}
	/* Apply Hanning Window. */
	k = 0
	for {
		if !(k < output_len) {
			break
		}
		*(*float32)(unsafe.Pointer(output + uintptr(k)*4)) = float32(float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4))) * (libc.Float64FromFloat64(0.5) - libc.Float64FromFloat64(0.5)*libc.Xcos(tls, float64(libc.Int32FromInt32(2)*k)*float64(3.141592653589793)/float64(output_len-libc.Int32FromInt32(1)))))
		goto _4
	_4:
		k++
	}
	/*	data [k] *= 0.3635819 - 0.4891775 * cos ((2 * k) * M_PI / (output_len - 1))
		+ 0.1365995 * cos ((4 * k) * M_PI / (output_len - 1))
		- 0.0106411 * cos ((6 * k) * M_PI / (output_len - 1)) ;
	*/
	return
}

/* gen_windowed_sines */

func x_save_oct_float(tls *libc.TLS, filename uintptr, input uintptr, in_len int32, output uintptr, out_len int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var file, v1 uintptr
	var k int32
	_, _, _ = file, k, v1
	libc.Xprintf(tls, __ccgo_ts+675, libc.VaList(bp+8, filename))
	v1 = libc.Xfopen(tls, filename, __ccgo_ts+721)
	file = v1
	if !(v1 != 0) {
		return
	}
	libc.Xfprintf(tls, file, __ccgo_ts+723, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+748, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+763, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+779, libc.VaList(bp+8, in_len))
	libc.Xfprintf(tls, file, __ccgo_ts+791, 0)
	k = 0
	for {
		if !(k < in_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+805, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(input + uintptr(k)*4)))))
		goto _2
	_2:
		k++
	}
	libc.Xfprintf(tls, file, __ccgo_ts+810, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+763, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+779, libc.VaList(bp+8, out_len))
	libc.Xfprintf(tls, file, __ccgo_ts+791, 0)
	k = 0
	for {
		if !(k < out_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+805, libc.VaList(bp+8, float64(*(*float32)(unsafe.Pointer(output + uintptr(k)*4)))))
		goto _3
	_3:
		k++
	}
	libc.Xfclose(tls, file)
	return
}

/* save_oct_float */

func x_save_oct_double(tls *libc.TLS, filename uintptr, input uintptr, in_len int32, output uintptr, out_len int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var file, v1 uintptr
	var k int32
	_, _, _ = file, k, v1
	libc.Xprintf(tls, __ccgo_ts+675, libc.VaList(bp+8, filename))
	v1 = libc.Xfopen(tls, filename, __ccgo_ts+721)
	file = v1
	if !(v1 != 0) {
		return
	}
	libc.Xfprintf(tls, file, __ccgo_ts+723, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+748, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+763, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+779, libc.VaList(bp+8, in_len))
	libc.Xfprintf(tls, file, __ccgo_ts+791, 0)
	k = 0
	for {
		if !(k < in_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+805, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(input + uintptr(k)*8))))
		goto _2
	_2:
		k++
	}
	libc.Xfprintf(tls, file, __ccgo_ts+810, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+763, 0)
	libc.Xfprintf(tls, file, __ccgo_ts+779, libc.VaList(bp+8, out_len))
	libc.Xfprintf(tls, file, __ccgo_ts+791, 0)
	k = 0
	for {
		if !(k < out_len) {
			break
		}
		libc.Xfprintf(tls, file, __ccgo_ts+805, libc.VaList(bp+8, *(*float64)(unsafe.Pointer(output + uintptr(k)*8))))
		goto _3
	_3:
		k++
	}
	libc.Xfclose(tls, file)
	return
}

/* save_oct_double */

func x_interleave_data(tls *libc.TLS, in uintptr, out uintptr, frames int32, channels int32) {
	var ch, fr int32
	_, _ = ch, fr
	fr = 0
	for {
		if !(fr < frames) {
			break
		}
		ch = 0
		for {
			if !(ch < channels) {
				break
			}
			*(*float32)(unsafe.Pointer(out + uintptr(ch+channels*fr)*4)) = *(*float32)(unsafe.Pointer(in + uintptr(fr+frames*ch)*4))
			goto _2
		_2:
			ch++
		}
		goto _1
	_1:
		fr++
	}
	return
}

/* interleave_data */

func x_deinterleave_data(tls *libc.TLS, in uintptr, out uintptr, frames int32, channels int32) {
	var ch, fr int32
	_, _ = ch, fr
	ch = 0
	for {
		if !(ch < channels) {
			break
		}
		fr = 0
		for {
			if !(fr < frames) {
				break
			}
			*(*float32)(unsafe.Pointer(out + uintptr(fr+frames*ch)*4)) = *(*float32)(unsafe.Pointer(in + uintptr(ch+channels*fr)*4))
			goto _2
		_2:
			fr++
		}
		goto _1
	_1:
		ch++
	}
	return
}

/* deinterleave_data */

func x_reverse_data(tls *libc.TLS, data uintptr, datalen int32) {
	var left, right int32
	var temp float32
	_, _, _ = left, right, temp
	left = 0
	right = datalen - int32(1)
	for left < right {
		temp = *(*float32)(unsafe.Pointer(data + uintptr(left)*4))
		*(*float32)(unsafe.Pointer(data + uintptr(left)*4)) = *(*float32)(unsafe.Pointer(data + uintptr(right)*4))
		*(*float32)(unsafe.Pointer(data + uintptr(right)*4)) = temp
		left++
		right--
	}
}

/* reverse_data */

func x_get_cpu_name(tls *libc.TLS) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var dest, file, name, search, src, v1 uintptr
	var is_pipe int32
	_, _, _, _, _, _, _ = dest, file, is_pipe, name, search, src, v1
	name = __ccgo_ts + 826
	search = libc.UintptrFromInt32(0)
	file = libc.UintptrFromInt32(0)
	is_pipe = 0
	file = libc.Xfopen(tls, __ccgo_ts+834, __ccgo_ts+848)
	search = __ccgo_ts + 850
	if search == libc.UintptrFromInt32(0) {
		libc.Xprintf(tls, __ccgo_ts+861, libc.VaList(bp+8, uintptr(unsafe.Pointer(&___func__))))
		return name
	}
	for libc.Xfgets(tls, uintptr(unsafe.Pointer(&_buffer)), int32(512), file) != libc.UintptrFromInt32(0) {
		if libc.Xstrstr(tls, uintptr(unsafe.Pointer(&_buffer)), search) != 0 {
			v1 = libc.Xstrchr(tls, uintptr(unsafe.Pointer(&_buffer)), int32(':'))
			src = v1
			if v1 != libc.UintptrFromInt32(0) {
				src++
				for int32(*(*uint16)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(libc.X__ctype_b_loc(tls))) + uintptr(int32(*(*uint8)(unsafe.Pointer(src))))*2)))&int32(uint16(_ISspace)) != 0 {
					src++
				}
				name = src
				/* Remove consecutive spaces. */
				src++
				dest = src
				for {
					if !(*(*uint8)(unsafe.Pointer(src)) != 0) {
						break
					}
					if int32(*(*uint16)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(libc.X__ctype_b_loc(tls))) + uintptr(int32(*(*uint8)(unsafe.Pointer(src))))*2)))&int32(uint16(_ISspace)) != 0 && int32(*(*uint16)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(libc.X__ctype_b_loc(tls))) + uintptr(int32(*(*uint8)(unsafe.Pointer(dest + uintptr(-libc.Int32FromInt32(1))))))*2)))&int32(uint16(_ISspace)) != 0 {
						goto _2
					}
					*(*uint8)(unsafe.Pointer(dest)) = *(*uint8)(unsafe.Pointer(src))
					dest++
					goto _2
				_2:
					src++
				}
				*(*uint8)(unsafe.Pointer(dest)) = uint8(0)
				break
			}
		}
	}
	if is_pipe != 0 {
		libc.Xpclose(tls, file)
	} else {
		libc.Xfclose(tls, file)
	}
	return name
}

var ___func__ = [13]uint8{'g', 'e', 't', '_', 'c', 'p', 'u', '_', 'n', 'a', 'm', 'e'}

var _buffer [512]uint8

const m___restrict_arr = "restrict"

func x_calculate_snr(tls *libc.TLS, data uintptr, len1 int32, expected_peaks int32) (r float64) {
	var snr float64
	_ = snr
	snr = float64(200)
	data = data
	len1 = len1
	expected_peaks = expected_peaks
	return snr
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "\n    Varispeed SNR test\x00        Zero Order Hold interpolator    : \x00ok\x00        Linear interpolator             : \x00        Sinc interpolator               : \x00\n    Varispeed bounds test\x00\x00\n\nLine %d : src_new () failed : %s\n\n\x00\n\nLine %d : %s\n\n\x00  src_data.input_frames  : %ld\n\x00  src_data.output_frames : %ld\n\n\x00\n\nLine %d : unused input.\n\x00\tinput_len         : %d\n\x00\tinput_frames_used : %ld\n\n\x00\n\nLine %d : snr (%3.1f) does not meet target (%3.1f)\n\n\x00varispeed.mat\x00%d channels, ratio %g -> %g\x00\n\nLine %d : %s : %s\n\n\x00Condition failed on Line %d : %s\n\n\x00k < max_loop_count\x00total_frames_gen > 0\x00! isnan (output [k])\x00\n%s : Error : freq [%d] == %g is out of range. Should be < 0.5.\n\x00tests/util.c\x00Dumping input and output data to file : %s.\n\n\x00w\x00# Not created by Octave\n\x00# name: input\n\x00# type: matrix\n\x00# rows: %d\n\x00# columns: 1\n\x00% g\n\x00# name: output\n\x00Unknown\x00/proc/cpuinfo\x00r\x00model name\x00Error : search is NULL in function %s.\n\x00"
